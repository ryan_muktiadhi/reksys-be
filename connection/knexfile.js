// Update with your config settings.

module.exports = {
  // development: {
  //   client: "oracledb",
  //   connection: {
  //     user: "KKTADMDB",
  //     password: "kktadmdb", // mypw contains the hr schema password
  //     connectString: "localhost:1521/XE",
  //     database: "KKTADMDB",
  //   },
  //   migrations: {
  //     tableName: "kktadmmigra",
  //   },
  // },
  // development: {
  //   client: "pg",
  //   connection: {
  //     host: "10.11.100.116",
  //     database: "kktkomiportal",
  //     user: "postgres",
  //     password: "postgres",
  //   },
  //   pool: {
  //     min: 2,
  //     max: 10,
  //   },
  // },
  development: {
    client: "oracledb",
    connection: {
      user: "kktkomidb",
      password: "kktadmdb", // mypw contains the hr schema password
      connectString: "localhost:1521/XE",
      database: "kktkomidb",
    },
    migrations: {
      tableName: "kktd",
    },
  },
};
