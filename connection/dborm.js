const nodemailer = require("nodemailer");
var conf = require("../config.json");

function postgre() {
  return require("knex")({
    client: "pg",
    connection: {
      host: conf.dbConfig.pg.host,
      database: conf.dbConfig.pg.database,
      user: conf.dbConfig.pg.user,
      password: conf.dbConfig.pg.password,
    },
    pool: {
      min: 2,
      max: 10,
    },
  });
}

function postgreKKT() {
  return require("knex")({
    client: "pg",
    version: "7.2",
    connection: {
      host: conf.dbConfig.pgkkt.host,
      database: conf.dbConfig.pgkkt.database,
      user: conf.dbConfig.pgkkt.user,
      password: conf.dbConfig.pgkkt.password,
    },
  });
}

function mssql() {
  return require("knex")({
    client: "mssql",
    connection: {
      host: "127.0.0.1",
      port: 1433,
      user: "s1sql",
      password: "P@ssw0rd.1",
      database: "ormKKT",
    },
  });
}

function conn() {
  if (conf.db == "oracle") {
    return oracle();
  } else if (conf.db == "postgres") {
    return postgre();
  }
}

function connKKT() {
  if (conf.db == "oracle") {
    return oracleKKTORA();
  } else if (conf.db == "postgres") {
    return postgreKKT();
  }
}

module.exports = {
  conn,
  connKKT,
};
