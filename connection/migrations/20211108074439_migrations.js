exports.up = function (knex) {
  return (
    knex.schema
      .createTable("m_accounttype", (table) => {
        table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
        table.bigInteger("iduser").defaultTo(0);
        table.string("cd_busparam").notNullable();
        table.bigInteger("idtenant").defaultTo(0);
        table.bigInteger("idapp").defaultTo(0);
        table.bigInteger("created_byid").defaultTo(0);
        table
          .timestamp("created_at", { useTz: false })
          .defaultTo(knex.fn.now());
        table
          .timestamp("updated_at", { useTz: false })
          .defaultTo(knex.fn.now());
      })
      .createTable("cdid_dbcorellation", (table) => {
        table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
        table.string("description").notNullable();
        table
          .timestamp("created_at", { useTz: false })
          .defaultTo(knex.fn.now());
        table
          .timestamp("updated_at", { useTz: false })
          .defaultTo(knex.fn.now());
      })
      .createTable("cdid_liscenses", (table) => {
        table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
        table
          .string("licensetokens")
          .defaultTo("", (options = { constraintName: (string = undefined) }));
        table
          .timestamp("expiredate", { useTz: false })
          .defaultTo(knex.fn.now());
      })
      .createTable("m_bic", (table) => {
        table.string("bic_code");
        table.string("bank_name");
        table.timestamp("last_update_date", { useTz: false });
        table.string("change_who");
        table.bigInteger("idtenant").defaultTo(0);
        table
          .timestamp("created_date", { useTz: false })
          .defaultTo(knex.fn.now());
        table.string("bank_code");
        table.integer("status").defaultTo(0);
      })
      .createTable("m_branch", (table) => {
        table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
        table.string("citycode");
        table.string("cityname");
        table.string("branchcode");
        table.string("branchname");
        table.integer("status").defaultTo(0);
        table.timestamp("last_update_date", { useTz: false });
        table.string("change_who");
        table
          .timestamp("created_date", { useTz: false })
          .defaultTo(knex.fn.now());
        table.string("bank_code");
        table.bigInteger("idtenant").defaultTo(0);
      })
      .createTable("m_channeltype", (table) => {
        table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
        table.string("channeltype");
        table.integer("status").defaultTo(0);
        table
          .timestamp("created_date", { useTz: false })
          .defaultTo(knex.fn.now());
        table.string("change_who");
        table.timestamp("last_update_date", { useTz: false });
        table.bigInteger("idtenant").defaultTo(0);
        table.string("channelcode");
      })
      .createTable("m_customertype", (table) => {
        table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
        table.string("code");
        table.string("name");
        table.string("type_cb");
        table.string("type_bf");
        table.integer("status").defaultTo(0);
        table.bigInteger("id_tenant").defaultTo(0);
        table
          .timestamp("created_date", { useTz: false })
          .defaultTo(knex.fn.now());
      })
      .createTable("m_datareport", (table) => {
        table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
        table.string("headercol");
        table.string("datacol");
        table
          .timestamp("created_date", { useTz: false })
          .defaultTo(knex.fn.now());
        table.string("srcpayload");
        table.string("filename");
      })
      .createTable("m_idtype", (table) => {
        table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
        table.string("code");
        table.string("name");
        table.string("type_cb");
        table.string("type_bf");
        table.integer("status").defaultTo(0);
        table.bigInteger("idtenant").defaultTo(0);
        table
          .timestamp("created_date", { useTz: false })
          .defaultTo(knex.fn.now());
      })
      .createTable("m_limit", (table) => {
        table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
        table.string("limit_name");
        table.string("tran_type");
        table.string("min_tran");
        table.string("max_limit");
        table.integer("status").defaultTo(0);
        table.bigInteger("idtenant").defaultTo(0);
        table
          .timestamp("created_date", { useTz: false })
          .defaultTo(knex.fn.now());
      })
      .createTable("m_prefund", (table) => {
        table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
        table.bigInteger("idparticipant");
        table.integer("amount");
        table.timestamp("last_update_date", { useTz: false });
        table.string("change_who");
        table.bigInteger("idtenant").defaultTo(0);
        table
          .timestamp("created_date", { useTz: false })
          .defaultTo(knex.fn.now());
      })
      .createTable("m_proxy", (table) => {
        table.string("registration_id");
        table.string("proxy_type");
        table.string("proxy_value");
        table.string("registrar_bic");
        table.string("account_number");
        table.string("account_type");
        table.string("account_name");
        table.string("identification_number_type");
        table.string("identification_number_value");
        table.string("proxy_status");
        table.timestamp("last_update_date", { useTz: false });
        table
          .timestamp("created_date", { useTz: false })
          .defaultTo(knex.fn.now());
        table.string("change_who");
        table.bigInteger("idtenant").defaultTo(0);
      })
      .createTable("m_proxyalias", (table) => {
        table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
        table.string("name");
        table.string("accnum");
        table.string("prxaddress");
        table.string("branch");
        table.string("channel");
        table.string("proxytype");
        table.string("secondid");
        table.string("custresident");
        table
          .timestamp("created_date", { useTz: false })
          .defaultTo(knex.fn.now());
        table.string("change_who");
        table.timestamp("last_update_date", { useTz: false });
        table.bigInteger("idtenant").defaultTo(0);
        table.integer("stsprxadd");
        table.string("secondtype");
        table.string("proxystat");
      })
      .createTable("m_proxytype", (table) => {
        table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
        table.string("proxycode").notNullable();
        table.string("proxyname").notNullable();
        table.integer("status").defaultTo(0);
        table
          .timestamp("created_date", { useTz: false })
          .defaultTo(knex.fn.now());
        table.integer("change_who");
        table
          .timestamp("last_update_date", { useTz: false })
          .defaultTo(knex.fn.now());
        table.integer("idtenant").defaultTo(0);
      })
      .createTable("m_resident", (table) => {
        table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
        table.string("code").notNullable();
        table.string("name").notNullable();
        table.string("status_cb").notNullable();
        table.string("status_bf").notNullable();
        table.integer("status");
        table.bigInteger("id_tenant");
        table
          .timestamp("created_date", { useTz: false })
          .defaultTo(knex.fn.now());
      })
      .createTable("m_systemparam", (table) => {
        table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
        table.string("paramname").notNullable();
        table.string("paramvalua").notNullable();
        table.integer("status").defaultTo(0);
        table
          .timestamp("created_date", { useTz: false })
          .defaultTo(knex.fn.now());
        table.bigInteger("change_who");
        table.timestamp("last_update_date", { useTz: false });
        table.bigInteger("id_tenant").defaultTo(0);
      })
      .createTable("m_trxcost", (table) => {
        table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
        table.string("fee");
        table.integer("values");
        table.integer("status").defaultTo(0);
        table.timestamp("last_update_date", { useTz: false });
        table.string("change_who");
        table.bigInteger("id_tenant").defaultTo(0);
        table
          .timestamp("created_date", { useTz: false })
          .defaultTo(knex.fn.now());
      })
      .createTable("outbound_message", (table) => {
        table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
        table.string("bizmsgid");
        table.string("channel");
        table.timestamp("chnl_req_time", { useTz: false });
        table.timestamp("chnl_resp_time", { useTz: false });
        table.timestamp("cihub_req_time", { useTz: false });
        table.timestamp("cihub_resp_time", { useTz: false });
        table.text("error_msg");
        table.text("full_request_msg");
        table.text("full_response_msg");
        table.string("intrn_ref_id");
        table.string("message_name");
        table.string("recpt_bank");
        table.string("resp_bizmsgid");
        table.string("resp_status");
        table.string("rejct_msg");
        table.integer("saf_counter");
      })
      .createTable("prefund_dashboard", (table) => {
        table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
        table.bigInteger("min");
        table.bigInteger("max");
        table.bigInteger("current");
      })
      .createTable("prefund_history", (table) => {
        table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
        table.string("refNum");
        table.integer("transactionType");
        table.string("prefundAmount");
        table.timestamp("datetime", { useTz: false });
      })
      //ganti ke sys_param ke db baru
      .createTable("system_param", (table) => {
        table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
        table.string("param_name").notNullable;
        table.string("param_value").notNullable;
        table.string("category");
        table.string("description");
      })
      .createTable("trx_log", (table) => {
        table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
        table.string("status");
        table.string("trxnumber");
        table.string("refnumbifast");
        table.string("channel");
        table.string("destbank");
        table.string("srcbank");
        table.string("destaccnum");
        table.string("srcaccnum");
        table.string("destaccname");
        table.string("srcaccname");
        table
          .timestamp("created_date", { useTz: false })
          .defaultTo(knex.fn.now());
        table.string("change_who");
        table.timestamp("last_updated_date", { useTz: false });
        table.decimal("amount", 12, 1).defaultTo(0.0);
        table.integer("transacttype").defaultTo(0);
        table.integer("idtenant");
        table.string("branchcode");
        table.string("feecode");
        table.string("feeamount");
      })
      .createTable("trx_reportlog", (table) => {
        table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
        table.integer("rpttype").defaultTo(0);
        table.integer("status").defaultTo(0);
        table
          .timestamp("created_at", { useTz: false })
          .defaultTo(knex.fn.now());
        table
          .timestamp("updated_at", { useTz: false })
          .defaultTo(knex.fn.now());
      })
  );
};

exports.down = function (knex) {
  return knex.schema
    .dropTable("cdid_adminnotification")
    .dropTable("cdid_dbcorellation")
    .dropTable("cdid_liscenses")
    .dropTable("cdid_orgapplications")
    .dropTable("cdid_orguserassign")
    .dropTable("cdid_owner")
    .dropTable("cdid_tenant_user_groupacl")
    .dropTable("cdid_tenant_user")
    .dropTable("cdid_tenant")
    .dropTable("cicd_menuuseracl")
    .dropTable("event_log")
    .dropTable("mst_application")
    .dropTable("mst_biodata_corell")
    .dropTable("mst_bussines_param")
    .dropTable("mst_menu")
    .dropTable("mst_module")
    .dropTable("mst_moduleby_tenant")
    .dropTable("mst_organizations")
    .dropTable("mst_tenantgroup")
    .dropTable("mst_usergroup")
    .dropTable("mst_usergroupacl")
    .dropTable("pass_hist")
    .dropTable("system_param")
    .dropTable("trx_eventlog")
    .dropTable("trx_sessionlog");
};
