exports.up = function (knex) {
    return knex.schema.createTable("t_transaction", (table) => {
      //table.bigincrements("id", (options = { primaryKey: (boolean = true) }));
      table.string("komi_unique_id");
      table.string("bifast_trx_no");
      table.string("komi_trx_no");
      table.string("channel_type");
      table.string("branch_code");
      table.string("recipient_bank");
      table.string("sender_bank");
      table.string("recipient_account_no");
      table.string("recipient_proxy_type");
      table.text("recipient_proxy_alias");
      table.string("recipient_account_name");
      table.string("sender_account_no");
      table.string("sender_account_name");
      table.string("charge_type");
      table.string("trx_type");
      table.integer("trx_amount");
      table.integer("trx_fee");
      table.timestamp("trx_date", { useTz: false }).defaultTo(knex.fn.now());
      table.string("trx_status_code");
      table.string("trx_status_message");
    });
  };
  
  exports.down = function (knex) {
    return knex.schema.dropTable("t_transaction");
  };
  