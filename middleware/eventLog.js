const knex = require("../connection/dborm");
const logs = require("../routes/services/utils/logger");

const fileLog = logs.logging();
const pool = knex.connKKT();

module.exports = async (req, res, next) => {
  try {
    const oldWrite = res.write;
    const oldEnd = res.end;

    const chunks = [];

    res.write = (...restArgs) => {
      chunks.push(Buffer.from(restArgs[0]));
      oldWrite.apply(res, restArgs);
    };

    res.end = async (...restArgs) => {
      if (restArgs[0]) {
        chunks.push(Buffer.from(restArgs[0]));
      }
      const body = Buffer.concat(chunks).toString("utf8");
      if (req.url.includes("sign")) {
        req.body.secret = "**************";
      }

      if (req.url != "/who") {
        await pool("event_log")
          .returning(["*"])
          .insert({
            request_ip:
              req.headers["x-forwarded-for"] || req.connection.remoteAddress,
            request_url: req.originalUrl,
            request_method: req.method,
            request_service: req.url,
            request_body: JSON.stringify(req.body),
            request_header: JSON.stringify(req.headers),
            request_date: req.requestDate,
            response_data: JSON.stringify(body) || {},
            response_date: pool.fn.now(),
            request_token: req.tokenDate,
            app_id: req.userData ? req.userData.apps[0].idapp : null,
          });
        await insertIntoLogEvent(req);
      }

      oldEnd.apply(res, restArgs);
    };
  } catch (err) {
    console.log(err);
  }
  next();
};

const insertIntoLogEvent = async (req) => {
  try {
    let fullPath = req.baseUrl + req.path;
    let routes = req.baseUrl.split("/");

    console.log(fullPath);

    let eventVal =
      fullPath.includes("getAll") || fullPath.includes("retrive")
        ? `${routes[2]} View all data `
        : fullPath.includes("get") ||
          fullPath.includes("byid") ||
          fullPath.includes("ById")
        ? `${routes[2]} View data with filter`
        : fullPath.includes("insert")
        ? `${routes[2]} Insert new data `
        : fullPath.includes("update")
        ? `${routes[2]} Update data `
        : fullPath.includes("delete")
        ? `${routes[2]} Delete data`
        : "";

    let idapp = req.userData.apps[0].idapp;

    let description =
      fullPath.includes("getAll") || fullPath.includes("retrive")
        ? `${routes[2]} View all ${routes[2]} data `
        : fullPath.includes("get") ||
          fullPath.includes("byid") ||
          fullPath.includes("ById")
        ? `${routes[2]} View ${routes[2]} data with filter`
        : fullPath.includes("insert")
        ? `${routes[2]} Insert ${routes[2]} new data `
        : fullPath.includes("update")
        ? `${routes[2]} Update ${routes[2]} data `
        : fullPath.includes("delete")
        ? `${routes[2]} Delete ${routes[2]} data`
        : "";
    let cdaction = fullPath.includes("getAll")
      ? 4
      : fullPath.includes("get")
      ? 4
      : fullPath.includes("insert")
      ? 5
      : fullPath.includes("update")
      ? 6
      : fullPath.includes("delete")
      ? 7
      : 4;
    console.log(eventVal, idapp, 0);

    await pool("trx_eventlog").insert({
      created_by: req.userData.id ? req.userData.id : 0,
      value: eventVal ? eventVal : 0,
      idapp: idapp ? idapp : 0,
      idmenu: 0,
      description: description ? description : 0,
      cdaction: cdaction ? cdaction : 0,
      refevent: req.tokenID ? req.tokenID : 0,
      valuejson: req.body ? JSON.stringify(req.body) : null,
    });
    fileLog.info(`Saving event to eventlog `);
  } catch (err) {
    console.log(err);
    // next();
  }
};
