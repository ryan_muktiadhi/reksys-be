const conf = require("./config.json");

const express = require("express"),
  app = express(),
  port = process.env.port || conf.port;
const cors = require("cors");
// var authRoute = require("./routes/services/serv_authentication");
// var organizationRoute = require("./routes/services/serv_organizations");
// var applicationRoute = require("./routes/services/serv_application");
// var userManagerRoute = require("./routes/services/serv_usermanagement");
// var ownerRoute = require("./routes/services/serv_owner");
// var modulesRoute = require("./routes/services/serv_modules");
// var menusRoute = require("./routes/services/serv_menus");
// var tasksRoute = require("./routes/services/serv_task");

// var profileRoute = require("./routes/services/serv_profile");
// var channelTypeRoute = require("./routes/services/komi_serv_channeltype");
// var bicRoute = require("./routes/services/komi_serv_bic");
// var proxyRoute = require("./routes/services/komi_serv_proxy");
// var proxyTypeRoute = require("./routes/services/komi_serv_proxytype");
// var monTrxRoute = require("./routes/services/komi_serv_montrx");
// var sysparamRoute = require("./routes/services/komi_serv_sysparam");
// var prefundRoute = require("./routes/services/komi_serv_prefund");
// var trxcostRoute = require("./routes/services/komi_serv_trxcost");
// var branchRoute = require("./routes/services/komi_serv_branch");
// var cityRoute = require("./routes/services/komi_serv_city");
// var integratApiRoute = require("./routes/services/serv_outapi");

// const limitRoute = require("./routes/services/komi_serv_limit");
// const idTypeRoute = require("./routes/services/komi_serv_idtype");
// const acctTypeRoute = require("./routes/services/komi_serv_acctype");
// const residentRoute = require("./routes/services/komi_serv_resident");
// const customerTypeRoute = require("./routes/services/komi_serv_custtype");
// const prefundDashboardRoute = require("./routes/services/komi_prefund_dashboard");
// const reportsRoute = require("./routes/services/komi_serv_trxreport");
// const logRoute = require("./routes/services/serv_log");
// const testOrm = require("./routes/services/komi_orm");
// const transactionRoute = require("./routes/services/komi_serv_transaction");
// const dashboardRoute = require("./routes/services/komi_serv_dashboard");
// const proxyManagementRoute = require("./routes/services/komi_serv_proxymgmt");
// const ConnectionChannelRoute = require("./routes/services/komi_serv_connchannel");
// const ApiLoggingExternalRoute = require("./routes/services/serv_log_external");
// const EmailNotificationlRoute = require("./routes/services/komi_serv_integrasi");
// const RekonsiliasiRoute = require("./routes/services/komi_serv_rekonsiliasi");

// var integratApiRoute = require("./routes/services/serv_outapi");

// komi channel route
// const channelProxyList = require("./routes/services/komi_channel_serv_proxylist");
// const channelAccountEnquiry = require("./routes/services/komi_channel_serv_accenquiry");
// const channelCreditTransfer = require("./routes/services/komi_channel_serv_credittransfer");
// const channelProxyManagement = require("./routes/services/komi_channel_serv_proxymanagement");
var rekonRoute = require("./routes/services/serv_rekon");
var path = require("path");

// const session = require("express-session");
app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);
app.use(cors());
//-==================== PATH SETTING =================
//app.use(logger('dev'));
app.use(express.static(path.join(__dirname, "public")));
app.use("/adm/rekon", rekonRoute);
// app.use("/komi/bic", bicRoute);
// app.use("/komi/proxy", proxyRoute);
// app.use("/komi/sysparam", sysparamRoute);
// app.use("/komi/channeltype", channelTypeRoute);
// app.use("/komi/prefund", prefundRoute);
// app.use("/komi/trxcost", trxcostRoute);
// app.use("/komi/branch", branchRoute);
// app.use("/komi/city", cityRoute);

// app.use("/komi/limit", limitRoute);
// app.use("/komi/idtype", idTypeRoute);
// app.use("/komi/accounttype", acctTypeRoute);
// app.use("/komi/resident", residentRoute);
// app.use("/komi/custtype", customerTypeRoute);
// app.use("/komi/prefunddashboard", prefundDashboardRoute);
// app.use("/komi/transaction", transactionRoute);
// app.use("/komi/proxymgmt", proxyManagementRoute);
// app.use("/komi/conchannel", ConnectionChannelRoute);
// app.use("/komi/v1/external/logging", ApiLoggingExternalRoute);
// app.use("/komi/v1/external/logging", ApiLoggingExternalRoute);
// app.use("/komi/integrate/", EmailNotificationlRoute);

// app.use("/komi/proxytype", proxyTypeRoute);
// app.use("/komi/montrx", monTrxRoute);
// app.use("/komi/reports", reportsRoute);
// app.use("/komi/log", logRoute);
// app.use("/komi/testOrm", testOrm);
// app.use("/komi/dashboard", dashboardRoute);
// app.use("/komi/rekonsiliasi", RekonsiliasiRoute);

// app.use("/komi/channel/proxylist", channelProxyList);
// app.use("/komi/channel/accountenquiry", channelAccountEnquiry);
// app.use("/komi/channel/credittransfer", channelCreditTransfer);
// app.use("/komi/channel/proxymanagement", channelProxyManagement);

var server = app.listen(port, function () {
  let host = server.address().address;
  let portname = server.address().port;
  console.log("Example server is running in http://%s:%s", host, portname);
});
