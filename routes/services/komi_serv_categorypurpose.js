const express = require("express"),
  app = express();
const router = express.Router();
const jwttools = require("./utils/encryptdecryptjwt");
const groupUtils = require("./utils/groupUtils");
const pool = require("../../connection/db");
const checkAuth = require("../../middleware/check-auth");
const moment = require("moment");
const knex = require("../../connection/dborm");
const msg = require("./utils/responseMessage");
router.use(checkAuth);

var conf = require("../../config.json");
// const conn
const conn = knex.conn();

//getAllGroup
router.get("/getAll", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  var apps = dcodeInfo.apps[0];
  try {
    console.log(
      ">>>>>>>>>>>> Tenanr, Level " + JSON.stringify(dcodeInfo.leveltenant)
    );

    const proxyType = await conn
      .select(
        "ID",
        "PROXY_CODE",
        "PROXY_NAME",
        "STATUS",
        "CREATED_DATE",
        "CREATED_BY",
        "UPDATED_DATE",
        "UPDATED_BY"
      )
      .from("M_PROXYTYPE");

    if (proxyType.length > 0) {
      setTimeout(function () {
        res.status(200).json({ status: 200, data: proxyType });
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({ status: 202, data: {} });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Internal server error" });
  }
});

router.get("/getByStatusActive", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  var apps = dcodeInfo.apps[0];

  try {
    console.log(
      ">>>>>>>>>>>> Tenanr, Level " + JSON.stringify(dcodeInfo.leveltenant)
    );

    const proxyType = await conn
      .select(
        "ID",
        "PROXY_CODE",
        "PROXY_NAME",
        "STATUS",
        "CREATED_DATE",
        "CREATED_BY",
        "UPDATED_DATE",
        "UPDATED_BY"
      )
      .from("M_PROXYTYPE")
      .where("STATUS", "1");

    if (proxyType.length > 0) {
      console.log("Success get Proxy Status data By Status active");
      setTimeout(function () {
        res.status(200).json({ status: 200, data: proxyType });
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({ status: 202, data: {} });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Internal server error" });
  }
});

router.get("/getById/:id", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  // var apps = dcodeInfo.apps[0];

  try {
    let id = req.params.id;

    const proxyType = await conn
      .select(
        "ID",
        "PROXY_CODE",
        "PROXY_NAME",
        "STATUS",
        "CREATED_DATE",
        "CREATED_BY",
        "UPDATED_DATE",
        "UPDATED_BY"
      )
      .from("M_PROXYTYPE")
      .where("ID", id);

    if (proxyType.length > 0) {
      setTimeout(function () {
        res.status(200).json({ status: 200, data: proxyType[0] });
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({ status: 202, data: {} });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Internal server error" });
  }
});

router.post("/insert", async (req, res, next) => {
  var dcodeInfo = req.userData;
  var apps = dcodeInfo.apps[0];

  try {
    const { proxyCode, proxyName, status } = req.body;

    // Check Already exsist
    const proxyTypeExsist = await conn
      .select("PROXY_CODE")
      .where("PROXY_CODE", proxyCode.trim())
      .from("M_PROXYTYPE");

    if (proxyTypeExsist.length > 0) {
      res.status(200).json({ status: 409, data: msg.ADD_DATA_ALREADY_EXSIST });
    }

    const resp = await conn("M_PROXYTYPE")
      .returning([
        "PROXY_CODE",
        "PROXY_NAME",
        "STATUS",
        "CREATED_BY",
        "CREATED_DATE",
      ])
      .insert({
        PROXY_CODE: proxyCode,
        PROXY_NAME: proxyName,
        STATUS: status,
        CREATED_BY: dcodeInfo.id,
        CREATED_DATE: conn.fn.now(),
      });

    if (resp.length > 0) {
      console.log(resp);
      res.status(200).json({ status: 200, data: msg.ADD_DATA_SUCCESS });
    } else {
      res.status(500).json({
        status: 500,
        data: msg.ADD_DATA_FAILED,
      });
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Internal server error" });
  }
});

router.post("/update", async (req, res, next) => {
  var dcodeInfo = req.userData;
  var apps = dcodeInfo.apps[0];

  try {
    const { id, proxyCode, proxyName, status } = req.body;
    const today = moment().format("YYYY-MM-DD HH:mm:ss");

    // Check eksist data
    const proxyTypeExsist = await conn
      .select("*")
      .where("ID", id)
      .from("M_PROXYTYPE");

    if (proxyTypeExsist.length < 0) {
      console.log("Failed update Account Type data");
      res
        .status(200)
        .json({ status: 204, data: msg.UPDATE_DATA_DOSENT_EXSIST });
    }

    const resp = await conn("M_PROXYTYPE")
      .returning(["ID", "PROXY_CODE", "PROXY_NAME", "STATUS", "UPDATED_DATE"])
      .update({
        PROXY_CODE: proxyCode,
        PROXY_NAME: proxyName,
        STATUS: status,
        CREATED_BY: proxyTypeExsist.CREATED_BY,
        CREATED_DATE: proxyTypeExsist.CREATED_DATE,
        UPDATED_BY: dcodeInfo.id,
        UPDATED_DATE: conn.fn.now(),
      })
      .where("ID", id);

    if (resp.length > 0) {
      res.status(200).json({ status: 200, data: msg.UPDATA_DATA_SUCCESS });
    } else {
      res.status(500).json({
        status: 500,
        data: msg.UPDATE_DATA_FAILED,
      });
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Internal server error" });
  }
});

router.get("/delete/:id", async (req, res, next) => {
  var dcodeInfo = req.userData;
  var apps = dcodeInfo.apps[0];
  let id = req.params.id;

  try {
    let resp = await conn("M_PROXYTYPE").where("ID", id).del();
    res.status(200).json({ status: 200, data: msg.DELETE_DATA_SUCCESS });
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error retrive Users" });
  }
});

module.exports = router;
