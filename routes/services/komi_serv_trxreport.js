const express = require("express"),
  app = express();
const router = express.Router();
/*
const jwttools = require("./utils/encryptdecryptjwt");
const groupUtils = require("./utils/groupUtils");
const pool = require("../../connection/db");const {
  fork
} = require("child_process");
const knex = require("../../connection/dborm");

var conf = require("../../config.json");
const conn = knex.conn();
// const conn
const conn = knex.conn();
*/

const moment = require("moment");

const checkAuth = require("../../middleware/check-auth");
router.use(checkAuth);

const axios = require("axios");

//getAllGroup
router.post("/getAllGenerated", async (req, res, next) => {
  const today = moment().format("YYYY-MM-DD HH:mm:ss");
  try {
    const {
      start_date,
      end_date,
      // firstRow,
      // maxRow,
      filters,
      // fieldOrders
    } = req.body;

    const usernamePasswordBuffer = Buffer.from(
      "mii_hafidz" + ":" + "metrodata.1"
    );
    const base64data = usernamePasswordBuffer.toString("base64");
    const headers = {
      Authorization: "Basic " + base64data,
    };

    var url =
      "http://192.168.226.221:5555/invoke/bjb.bifast.business.services:getTransactionReportByDateInterval";

    const resCore = await axios.post(
      url,
      {
        getTransactionReportByCurrentDateRequest: {
          esbHeader: {
            externalId: " ",
            timestamp: today,
          },
          esbBody: {
            trxDate1: start_date,
            trxDate2: end_date,
            filters: filters,
          },
        },
      },
      {
        headers,
      }
    );

    console.log(JSON.stringify(resCore.data));

    if (
      resCore.data.getTransactionReportByCurrentDateResponse.esbBody.results
        .length > 0
    ) {
      setTimeout(function () {
        res.status(200).json({
          status: 200,
          data: resCore.data.getTransactionReportByCurrentDateResponse.esbBody,
        });
      }, 1500);
    } else {
      setTimeout(function () {
        res.status(200).json({
          status: 202,
          data: resCore.data.getTransactionReportByCurrentDateResponse.esbBody,
        });
      }, 1500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({
      status: 500,
      data: "Internal Server Error",
    });
  }
});

// //getAllGroup
// router.get("/getAllGenerated", async (req, res, next) => {
//   //   const authHeader = req.headers.authorization;
//   var dcodeInfo = req.userData;
//   var apps = dcodeInfo.apps[0];
//   try {
//     console.log(
//       ">>>>>>>>>>>> Tenanr, Level " + JSON.stringify(dcodeInfo.leveltenant)
//     );
//     const today = moment().format("YYYY-MM-DD");

//     const bicAdmins = await conn
//       .where("idtenant", dcodeInfo.idtenant)
//       .select(
//         "id",
//         "rpttype",

//         "status",
//         "description",
//         "pathfile",
//         "created_date",
//         "idtenant"
//       )
//       .from("trx_reportlog");

//     if (bicAdmins.length > 0) {
//       setTimeout(function () {
//         res.status(200).json({ status: 200, data: bicAdmins });
//       }, 1500);
//     } else {
//       setTimeout(function () {
//         res.status(200).json({ status: 202, data: {} });
//       }, 1500);
//     }
//   } catch (err) {
//     console.log(err);
//     res.status(500).json({ status: 500, data: "internal error" });
//   }
// });
// router.post("/generateReportBy", async (req, res, next) => {
//   //   const authHeader = req.headers.authorization;
//   var dcodeInfo = req.userData;
//   // var apps = dcodeInfo.apps[0];
//   try {
//     console.log(
//       ">>>>>>>>>>>> Generate report, Level " +
//         JSON.stringify(dcodeInfo.leveltenant)
//     );
//     const today = moment().format("YYYY-MM-DD");

//     const { start_date, end_date, trxtype, desc } = req.body;

//     const bicAdmins = await conn("trx_reportlog")
//       .returning([
//         "id",
//         "rpttype",
//         "change_who",
//         "status",
//         "description",
//         "idtenant",
//       ])
//       .insert({
//         rpttype: trxtype,
//         change_who: dcodeInfo.fullname,
//         status: 0,
//         description: desc,
//         idtenant: dcodeInfo.idtenant,
//       });

//     if (bicAdmins.length > 0) {
//       let resurning = bicAdmins[0];
//       console.log("callingchild");
//       console.log(resurning);
//       const childProcess = fork("./routes/services/workers/reportworker.js");

//       childProcess.send({
//         start_date: start_date,
//         end_date: end_date,
//         trxtype: trxtype,
//         desc: desc,
//         iddata: resurning.id,
//         idtenant: dcodeInfo.idtenant,
//       });

//       setTimeout(function () {
//         res.status(200).json({ status: 200, data: bicAdmins });
//       }, 500);
//     } else {
//       setTimeout(function () {
//         res.status(200).json({ status: 202, data: {} });
//       }, 500);
//     }
//   } catch (err) {
//     console.log(err);
//     res.status(500).json({ status: 500, data: "internal error" });
//   }
// });
// router.get("/downloadByid/:id", async (req, res, next) => {
//   //   const authHeader = req.headers.authorization;
//   var dcodeInfo = req.userData;
//   var apps = dcodeInfo.apps[0];
//   try {
//     let rptcode = req.params.id;
//     console.log(
//       ">>>>>>>>>>>> Tenanr, Level " + JSON.stringify(dcodeInfo.leveltenant)
//     );
//     const today = moment().format("YYYY-MM-DD");

//     const bicAdmins = await conn
//       .where("id", rptcode)
//       .select("*")
//       .from("m_datareport");

//     if (bicAdmins.length > 0) {
//       setTimeout(function () {
//         res.status(200).json({ status: 200, data: bicAdmins });
//       }, 500);
//     } else {
//       setTimeout(function () {
//         res.status(200).json({ status: 202, data: {} });
//       }, 500);
//     }
//   } catch (err) {
//     console.log(err);
//     res.status(500).json({ status: 500, data: "internal error" });
//   }
// });

module.exports = router;
