const express = require("express"),
app = express();
const router = express.Router();
const pool = require("../../connection/db");
const jwttools = require("../../routes/services/utils/encryptdecryptjwt");
const logger = require("../../routes/services/utils/logservices");
const moment = require("moment");
const checkAuth = require("../../middleware/check-auth");
router.use(checkAuth);

router.post("/signviakkt", async (req, res, next) => {
  try {
    var idtenant = "0";
    console.log("INI REQUEST");
    const { credential, secret } = req.body;
    const resuserexist = await pool.query(
      "SELECT tu.id, tu.fullname, tu.userid, tu.idtenant, tu.idsubtenant, tu.leveltenant, tn.tnname, tn.tnstatus, tn.tnflag, tn.tnparentid, tn.cdtenant, bio.bioemailactive, bio.biophoneactive, bio.bioaddress, bio.bionik, bio.bionpwp, bio.bioidtipenik, bio.bioidcorel FROM public.cdid_tenant_user tu INNER JOIN public.cdid_tenant tn ON tu.idtenant=tn.id INNER JOIN (select * from public.mst_biodata_corell where bioidcorel=3) bio ON tu.id = bio.biocorelobjid WHERE tu.userid=$1 and tu.pwd=crypt($2, pwd)",
      [credential, secret]
    );
    if (resuserexist.rows.length > 0) {
      var jsonbody = resuserexist.rows[0];
      let appObject = [];
      let countapp = 0;
      idtenant = jsonbody.idtenant;
      let orgdefault = {"orgid" : 0};
      if(jsonbody.leveltenant > 0){
        idtenant = jsonbody.leveltenant > 1 ? jsonbody.tnparentid : jsonbody.idtenant;
      }
      jsonbody = {...jsonbody, ...orgdefault};
      var application = {};
      var appsModuleObj = {};
      
      if(jsonbody.leveltenant < 1){
        const liscenseapp = await pool.query(
          "SELECT clis.id_application id, TO_CHAR(clis.expiredate,'yyyy-MM-dd') expiredate, clis.id_application, clis.paidstatus, clis.active, clis.defaultactive, mapp.appname, mapp.applabel,mapp.description,mapp.routelink FROM public.cdid_liscenses clis INNER JOIN public.mst_application mapp ON clis.id_application = mapp.id where clis.cdid_tenant=$1 and clis.active=1 order by clis.defaultactive desc;",
          [idtenant]
        );
        if (liscenseapp.rows) {
          countapp = liscenseapp.rows.length;
          // appObject = liscenseapp.rows;
          // if (jsonbody.leveltenant > 1) {
            appObject = liscenseapp.rows;
          // } else {
          //   var itemApp = liscenseapp.rows[0];
          //   appObject.push(itemApp);
          // }
          var menusModuleObj = [];
          var appsModuleList = [];
          appsModuleObj = { sidemenus: appsModuleList };
          application = { appscount: countapp, apps: appObject };
        }
      } else {
        // CHeck Organization of Users.
//         SELECT ou.id, ou.userid, ou.orgid, ou.grpid, ou.created_at, oa.idapp, ma.appname,ma.applabel,ma.apptype,ma.routelink FROM public.cdid_orguserassign ou INNER JOIN cdid_orgapplications oa ON
// ou.orgid = oa.idorg INNER JOIN mst_application ma ON oa.idapp=ma.id WHERE ou.userid =4
        const liscenseapp = await pool.query("SELECT ma.id_application id, ma.expiredate, ma.id_application, ma.paidstatus, ma.active, ma.defaultactive, ma.appname,ma.applabel,ma.routelink FROM public.cdid_orguserassign ou INNER JOIN cdid_orgapplications oa ON ou.orgid = oa.idorg INNER JOIN (SELECT clis.id_application id, TO_CHAR(clis.expiredate,'yyyy-MM-dd') expiredate, clis.id_application, clis.paidstatus, clis.active, clis.defaultactive, mapp.appname, mapp.applabel,mapp.description,mapp.routelink FROM public.cdid_liscenses clis INNER JOIN public.mst_application mapp ON clis.id_application = mapp.id where clis.cdid_tenant=$1 and clis.active=1 order by clis.defaultactive desc) ma ON oa.idapp=ma.id WHERE ou.userid =$2",[idtenant, jsonbody.id]);
        if (liscenseapp.rows) {
          countapp = liscenseapp.rows.length;
          appObject = liscenseapp.rows;
          var menusModuleObj = [];
          var appsModuleList = [];
          appsModuleObj = { sidemenus: appsModuleList };
          application = { appscount: countapp, apps: appObject };
        }
      }
      jsonbody = { ...jsonbody, ...application, ...appsModuleObj };

      //#################### CHECK Session if exist ##########################
      let sessionid = "";
      const checkTodayLog = await pool.query("SELECT * FROM public.trx_sessionlog where created_by=$1 order by created_date desc limit 1", [jsonbody.id]);
      if(checkTodayLog.rows.length > 0) {
        sessionid = checkTodayLog.rows[0].id;
        var token = jwttools.encryptdata(sessionid);
        setTimeout(function() {
          res.status(200).json({ status: 200, data: token });
        }, 1000);
      } else {
        const today = moment().format('YYYY-MM-DD HH:mm:ss');
        const insertTodayLog = await pool.query("INSERT INTO public.trx_sessionlog(created_by, userinfo, expire_date, status) VALUES ($1, $2, $3, $4) RETURNING *",[jsonbody.id,jsonbody,today, 1]);
        if(insertTodayLog.rows.length > 0) {
          sessionid = insertTodayLog.rows[0].id;
          var token = jwttools.encryptdata(sessionid);
          setTimeout(function() {
            res.status(200).json({ status: 200, data: token });
          }, 1000);
        } else {
          res.status(500).json({ status: 500, data: "No User found" });
        }
      }
    } else {
      // if resuserexist < 1
      res.status(500).json({ status: 500, data: "No User found" });
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "No User found" });
  }
});

router.post("/changeapp", async (req, res, next) => {
  try {
    const { appid } = req.body;
    let systemdate = new Date();
    const authHeader = req.headers.authorization;
    var dcodeInfo = req.userData;
    var tokenID = req.tokenID;
      var idTenant =
        dcodeInfo.leveltenant > 1 ? dcodeInfo.idsubtenant : dcodeInfo.idtenant;
      var query =
        "SELECT lapp.id,lapp.expiredate,lapp.cdid_tenant,lapp.paidstatus, lapp.active,1 defaultactive,lapp.appname,lapp.applabel, oau.userid, oau.orgid, oau.grpid, mo.orgname, oapp.idapp,lapp.routelink FROM public.cdid_orguserassign oau INNER JOIN mst_organizations mo ON oau.orgid=mo.id INNER JOIN cdid_orgapplications oapp ON mo.id=oapp.idorg LEFT JOIN (SELECT clis.id, clis.licensetokens, clis.licensejson, TO_CHAR(clis.expiredate,'yyyy-MM-dd') expiredate, cdid_tenant, clis.id_application, clis.paidstatus, clis.active, clis.defaultactive, mapp.appname, mapp.applabel,mapp.description,mapp.routelink FROM public.cdid_liscenses clis INNER JOIN public.mst_application mapp ON clis.id_application = mapp.id where clis.cdid_tenant=$1 and clis.active=1 order by clis.defaultactive desc) lapp ON oapp.idapp = lapp.id_application where oau.userid = $2 and oapp.idapp=$3;";

      // var query = "SELECT clis.id, clis.licensetokens, TO_CHAR(clis.expiredate,'yyyy-MM-dd') expiredate, cdid_tenant, clis.id_application, clis.paidstatus, clis.active, 1 defaultactive, mapp.appname, mapp.applabel FROM public.cdid_liscenses clis INNER JOIN public.mst_application mapp ON clis.id_application = mapp.id where clis.cdid_tenant=$1 and clis.active=1 and clis.id_application=$2"
      // console.log(">>> JUMLAH Liscense APP "+query);
      const liscenseapp = await pool.query(query, [
        idTenant,
        dcodeInfo.id,
        appid,
      ]);
      console.log(">>> PARAM " + idTenant + ", "+dcodeInfo.id+", "+appid);
      console.log(">>> JUMLAH Liscense APP " + liscenseapp.rows.length);
      if (liscenseapp.rows.length > 0) {
        countapp = liscenseapp.rows.length;
        appObject = liscenseapp.rows;

        console.log(">>> APP Active :" + JSON.stringify(appObject));

        var menusModuleObj = [];
        var appsModuleList = [];
        var appsModuleObj = {};
        var application = { appscount: countapp, apps: appObject };
        let modulename = "";
        let modulenameEnd = "";
        var queryMenu;
        var ps;
        /// Select side menu
        console.log(">>> Level tenant " + dcodeInfo.leveltenant);
        switch (parseInt(dcodeInfo.leveltenant)) {
          case 0:
            queryMenu =
              "SELECT mm.id, mm.modulename,mm.status, mm.idapplication, mm.modulecode, mm.moduletype, mnu.name, mnu.title, mnu.routelink FROM public.mst_module mm INNER JOIN mst_menu mnu ON mm.id = mnu.idmodule WHERE mm.idapplication=$1 and mm.moduletype=1";
            ps = [appid];
            break;
          case 1:
            let checkUser = await pool.query(
              "SELECT * FROM public.cdid_tenant_user_groupacl where iduser = $1 ",
              [dcodeInfo.id]
            );
            let userAssignData = checkUser.rowCount;
            //console.log("line 508 " + dcodeInfo.id);
            console.log("line 508 " + userAssignData);

            queryMenu =
              userAssignData > 0
                ? "SELECT distinct on (mug.idmenu) mug.idmenu, tug.iduser, tug.idgroupuseracl,mmdl.idapplication,mmdl.modulename, mmnu.name, mmnu.title, mmnu.routelink, mmnu.routepath,mug.fcreate,mug.fread,mug.fupdate,mug.fdelete,mug.fview,mug.fapproval FROM public.cdid_tenant_user_groupacl tug INNER JOIN public.mst_usergroupacl mug ON tug.idgroupuseracl = mug.idgroup INNER JOIN mst_module mmdl ON mug.idmodule = mmdl.id INNER JOIN mst_menu mmnu ON mug.idmenu = mmnu.id WHERE tug.iduser= $1 order by mug.idmenu, mmdl.idapplication"
                : "SELECT mm.id, mm.modulename,mm.status, mm.idapplication, mm.modulecode, mm.moduletype, mnu.name, mnu.title, mnu.routelink FROM public.mst_module mm INNER JOIN mst_menu mnu ON mm.id = mnu.idmodule WHERE mm.idapplication=$1 and mm.moduletype=1";
            ps = userAssignData > 0 ? [dcodeInfo.id] : [appid];
            // queryMenu = "SELECT mm.id, mm.modulename,mm.status, mm.idapplication, mm.modulecode, mm.moduletype, mnu.name, mnu.title, mnu.routelink FROM public.mst_module mm INNER JOIN mst_menu mnu ON mm.id = mnu.idmodule WHERE mm.idapplication=$1 and mm.moduletype=1";
            // ps = [dcodeInfo.id];

            break;
          case 2:
            let checkUserACL = await pool.query(
              "SELECT * FROM public.cdid_tenant_user_groupacl where iduser = $1 ",
              [dcodeInfo.id]
            );
            let userAssignDataACL = checkUserACL.rowCount;
            queryMenu =
              userAssignDataACL > 0
                ? "SELECT distinct on (mug.idmenu) mug.idmenu, tug.iduser, tug.idgroupuseracl,mmdl.idapplication,mmdl.modulename, mmnu.name, mmnu.title, mmnu.routelink, mmnu.routepath,mug.fcreate,mug.fread,mug.fupdate,mug.fdelete,mug.fview,mug.fapproval FROM public.cdid_tenant_user_groupacl tug INNER JOIN public.mst_usergroupacl mug ON tug.idgroupuseracl = mug.idgroup INNER JOIN mst_module mmdl ON mug.idmodule = mmdl.id INNER JOIN mst_menu mmnu ON mug.idmenu = mmnu.id WHERE tug.iduser= $1 order by mug.idmenu, mmdl.idapplication"
                : "SELECT mm.id, mm.modulename,mm.status, mm.idapplication, mm.modulecode, mm.moduletype, mnu.name, mnu.title, mnu.routelink FROM public.mst_module mm INNER JOIN mst_menu mnu ON mm.id = mnu.idmodule WHERE mm.idapplication=$1 and mm.moduletype=1";
            ps = userAssignDataACL > 0 ? [dcodeInfo.id] : [appid];
            break;
          default:
            var queryMenu =
              "SELECT mm.id, mm.modulename,mm.status, mm.idapplication, mm.modulecode, mm.moduletype, mnu.name, mnu.title, mnu.routelink FROM public.mst_module mm INNER JOIN mst_menu mnu ON mm.id = mnu.idmodule WHERE mm.idapplication=$1 and mm.moduletype=1";
            var ps = [appid];
            break;
        }
       
        const getmoduleadminapps = await pool.query(queryMenu, ps);
        console.log(">>> JUMLAH Module APP " + getmoduleadminapps.rows.length);

        if (getmoduleadminapps.rows.length > 0) {
          let modelesTmp = getmoduleadminapps.rows;
          modelesTmp.forEach((element) => {
            if (element.modulename != modulename) {
              if (modulename != "") {
                let modulenmenus = {
                  label: modulename,
                  expanded: false,
                  items: menusModuleObj,
                };
                appsModuleList.push(modulenmenus);
                modulename = element.modulename;
                modulenameEnd = element.modulename;
                appsid = element.idapplication;
                menusModuleObj = [];
              } else {
                modulename = element.modulename;
                modulenameEnd = element.modulename;
                appsid = element.idapplication;
                console.log(">>>>>>> modulename awal >>>> " + modulename);
                menusModuleObj = [];
              }
            }
            menusModuleObj.push({
              label: element.title,
              routerLink: element.routelink,
            });
          });
          /*
          if (modulename === modulenameEnd) {
            console.log(
              ">>>>>>> Modul akhir " + (modulename === modulenameEnd)
            );
            let modulenmenus = {
              label: modulename,
              expanded: false,
              items: menusModuleObj,
            };
            console.log("asldn");
            console.log(menusModuleObj);
            appsModuleList.map((mdl) => {
              if (mdl.label === modulename) {
                console.log("asldn");
                console.log(menusModuleObj);
                menusModuleObj.map((dt) => {
                  mdl.items.push(dt);
                });

                appsModuleObj = { sidemenus: appsModuleList };
              } else {
                appsModuleList.push(modulenmenus);
                appsModuleObj = { sidemenus: appsModuleList };
              }
            });
          }*/

          if (modulename === modulenameEnd) {
            console.log(
              ">>>>>>> Modul akhir " + (modulename === modulenameEnd)
            );
            let modulenmenus = {
              label: modulename,
              expanded: false,
              items: menusModuleObj,
            };
            appsModuleList.push(modulenmenus);
            appsModuleObj = { sidemenus: appsModuleList };
          }
          dcodeInfo = { ...dcodeInfo, ...application, ...appsModuleObj };
          // console.log(">>>>>>> Kirim Balik " + JSON.stringify(dcodeInfo));

          //Update session log Side menu
          const sidemenuUpdateResp = await pool.query("UPDATE public.trx_sessionlog SET userinfo = $1 where id=$2 RETURNING *", [dcodeInfo,tokenID]);

          if(sidemenuUpdateResp.rows.length > 0) {
            var token = jwttools.encryptdata(tokenID);
            // setTimeout(function() {
              res.status(200).json({ status: 200, data: token });
            // }, 1000);
          } else {
            res.status(500).json({ status: 500, data: "Error Change App Inside" });
          }



          // const signoutResp = await pool.query("UPDATE public.trx_sessionlog SET status = $1 where id=$2 RETURNING *", [
          //   2,
          //   tokenId
          // ]);




          // var token = jwttools.encryptdata(dcodeInfo);
          // res.status(200).json({ status: 200, data: token });
        }
      }
   
    // res.status(200).json({ status: 200, data: dcodeInfo });
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error Change App Catch" });
  }
});

router.post("/signout", async (req, res, next) => {
  try {
    // const { appid } = req.body;
    const tokenId = req.tokenID;
    const signoutResp = await pool.query("UPDATE public.trx_sessionlog SET status = $1 where id=$2 RETURNING *", [
      2,
      tokenId
    ]);
    if(signoutResp.rows.length > 0) {
      setTimeout(function() {
        res.status(200).json({ status: 200, data: signoutResp.rows[0] });
      }, 1000);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error Change App Catch" });
  }
});


/*POST END*/

router.get("/who", async (req, res, next) => {
  try {
    let systemdate = new Date();
    const dcodeInfo = req.userData;
    const idtrx = req.tokenID;
    res.status(200).json({ status: 200, data: dcodeInfo, tokenId: idtrx });
  } catch (err) {
    res.status(500).json({ status: 500, data: "Error insert log Catch" });
  }
});

module.exports = router;
// router.post("/signviakkt", async (req, res, next) => {
//   try {
//     const { credential, secret } = req.body;
//     const resp = await pool.query(
//       "SELECT tu.id, tu.fullname, tu.userid, tu.pwd, tu.idtenant, tu.idsubtenant, tu.leveltenant, tn.tnname, tn.tnstatus, tn.tnflag, tn.tnparentid, tn.cdtenant, bio.bioname, bio.bioemailactive, bio.biophoneactive, bio.bioaddress, bio.bionik, bio.bionpwp, bio.bioidtipenik FROM public.cdid_tenant_user tu INNER JOIN public.cdid_tenant tn ON tu.idtenant=tn.id INNER JOIN (select * from public.mst_biodata_corell where bioidcorel=3) bio ON tu.id = bio.biocorelobjid WHERE tu.userid=$1 and tu.pwd=crypt($2, pwd)",
//       [credential, secret]
//     );
//     if (resp.rows.length > 0) {
//       var jsonbody = resp.rows[0];
//       let appObject = [];
//       let countapp = 0;
//       console.log(">>>>>>> Level Tenant " + jsonbody.leveltenant);
//       if (jsonbody.leveltenant > 0) {
//         var menusModuleObj = [];
//         var appsModuleList = [];
//         var appsModuleObj = { sidemenus: appsModuleList };
//         var application = {};
//         var idtenant =
//           jsonbody.leveltenant > 1 ? jsonbody.tnparentid : jsonbody.idtenant;

//         const liscenseapp = await pool.query(
//           "SELECT lapp.id,lapp.expiredate,lapp.cdid_tenant,lapp.paidstatus, lapp.active,1 defaultactive,lapp.appname,lapp.applabel, oau.userid, oau.orgid, oau.grpid, mo.orgname, oapp.idapp, lapp.routelink FROM public.cdid_orguserassign oau INNER JOIN mst_organizations mo ON oau.orgid=mo.id INNER JOIN cdid_orgapplications oapp ON mo.id=oapp.idorg LEFT JOIN (SELECT clis.id, clis.licensetokens, clis.licensejson, TO_CHAR(clis.expiredate,'yyyy-MM-dd') expiredate, cdid_tenant, clis.id_application, clis.paidstatus, clis.active, clis.defaultactive, mapp.appname, mapp.applabel,mapp.description, mapp.routelink FROM public.cdid_liscenses clis INNER JOIN public.mst_application mapp ON clis.id_application = mapp.id where clis.cdid_tenant=$1 and clis.active=1 order by clis.defaultactive desc) lapp ON oapp.idapp = lapp.id_application where oau.userid = $2;",
//           [idtenant, jsonbody.id]
//         );
//         if (liscenseapp.rows) {
//           countapp = liscenseapp.rows.length;
//           // appObject = liscenseapp.rows;
//           if (jsonbody.leveltenant > 0) {
//             appObject = liscenseapp.rows;
//           } else {
//             var itemApp = liscenseapp.rows[0];
//             appObject.push(itemApp);
//           }
//           // var menusModuleObj = [];
//           // var appsModuleList = [];
//           // var appsModuleObj = {"sidemenus" : appsModuleList};
//           application = { appscount: countapp, apps: appObject };
//           // jsonbody = { ...jsonbody, ...application, ...appsModuleObj };
//           // var token = jwttools.encryptdata(jsonbody);
//           // res.status(200).json({ status: 200, data: token });
//         }

//         if (jsonbody.leveltenant > 1) {
//           // "SELECT tug.iduser, tug.idgroupuseracl, mug.idmenu,mmdl.idapplication, mmdl.modulename, mmnu.name, mmnu.title, mmnu.routelink, mmnu.routepath FROM public.cdid_tenant_user_groupacl tug INNER JOIN public.mst_usergroupacl mug ON tug.idgroupuseracl = mug.idgroup INNER JOIN mst_module mmdl ON mug.idmodule = mmdl.id INNER JOIN mst_menu mmnu ON mug.idmenu = mmnu.id WHERE tug.iduser=$1 order by mmdl.idapplication, mug.idmenu",
//           const getaclgroup = await pool.query(
//             /*"SELECT tug.iduser, tug.idgroupuseracl, mug.idmenu,mmdl.idapplication, mmdl.modulename, mmnu.name, mmnu.title, mmnu.routelink, mmnu.routepath,mug.fcreate,mug.fread,mug.fupdate,mug.fdelete,mug.fview,mug.fapproval FROM public.cdid_tenant_user_groupacl tug INNER JOIN public.mst_usergroupacl mug ON tug.idgroupuseracl = mug.idgroup INNER JOIN mst_module mmdl ON mug.idmodule = mmdl.id INNER JOIN mst_menu mmnu ON mug.idmenu = mmnu.id WHERE tug.iduser=$1 order by mmdl.idapplication, mug.idmenu",*/
//             "SELECT distinct on (mug.idmenu) mug.idmenu, tug.iduser, tug.idgroupuseracl,mmdl.idapplication,mmdl.modulename, mmnu.name, mmnu.title, mmnu.routelink, mmnu.routepath,mug.fcreate,mug.fread,mug.fupdate,mug.fdelete,mug.fview,mug.fapproval FROM public.cdid_tenant_user_groupacl tug INNER JOIN public.mst_usergroupacl mug ON tug.idgroupuseracl = mug.idgroup INNER JOIN mst_module mmdl ON mug.idmodule = mmdl.id INNER JOIN mst_menu mmnu ON mug.idmenu = mmnu.id WHERE tug.iduser=$1 order by mug.idmenu, mmdl.idapplication",
//             [jsonbody.id]
//           );
//           console.log(">>>>>>> Menus Count " + getaclgroup.rows.length);
//           if (getaclgroup.rows.length > 0 && countapp > 0) {
//             let appsid = 0;

//             let modulename = "";
//             let modulenameEnd = "";
//             getaclgroup.rows.forEach((element) => {
//               // console.log(">>>>>>> Object menu "+JSON.stringify(element));
//               if (element.modulename != modulename) {
//                 if (modulename != "") {
//                   let modulenmenus = {
//                     label: modulename,
//                     expanded: false,
//                     items: menusModuleObj,
//                   };
//                   appsModuleList.push(modulenmenus);
//                   modulename = element.modulename;
//                   modulenameEnd = element.modulename;
//                   appsid = element.idapplication;
//                   menusModuleObj = [];
//                 } else {
//                   modulename = element.modulename;
//                   modulenameEnd = element.modulename;
//                   appsid = element.idapplication;
//                   console.log(">>>>>>> modulename awal >>>> " + modulename);
//                   menusModuleObj = [];
//                 }
//               }
//               menusModuleObj.push({
//                 label: element.title,
//                 routerLink: element.routelink,
//                 title: {
//                   c: +element.fcreate,
//                   r: element.fread,
//                   u: element.fupdate,
//                   d: element.fdelete,
//                   v: element.fview,
//                 },
//               });
//             });

//             if (modulename === modulenameEnd) {
//               console.log(
//                 ">>>>>>> Modul akhir " + (modulename === modulenameEnd)
//               );
//               let modulenmenus = {
//                 label: modulename,
//                 expanded: false,
//                 items: menusModuleObj,
//               };
//               // appsModuleList.push(modulenmenus);
//               appsModuleList = [];
//               appsModuleObj = { sidemenus: appsModuleList };
//             }
//             console.log(
//               ">>>>>>> Kirim Balik " + JSON.stringify(appsModuleList)
//             );
//             jsonbody = { ...jsonbody, ...application, ...appsModuleObj };
//             var token = jwttools.encryptdata(jsonbody);
//             res.status(200).json({ status: 200, data: token });
//           }
//         } else {
//           // jsonbody = { ...jsonbody, ...application };
//           if (jsonbody.leveltenant == 1) {
//             let modulename = "";
//             let modulenameEnd = "";
//             const getmoduleadminapps = await pool.query(
//               "SELECT mm.id, mm.modulename,mm.status, mm.idapplication, mm.modulecode, mm.moduletype, mnu.name, mnu.title, mnu.routelink FROM public.mst_module mm INNER JOIN mst_menu mnu ON mm.id = mnu.idmodule WHERE mm.idapplication=$1",
//               [0]
//             );
//             if (getmoduleadminapps.rows.length > 0) {
//               let modelesTmp = getmoduleadminapps.rows;
//               modelesTmp.forEach((element) => {
//                 if (element.modulename != modulename) {
//                   if (modulename != "") {
//                     let modulenmenus = {
//                       label: modulename,
//                       expanded: false,
//                       items: menusModuleObj,
//                     };
//                     appsModuleList.push(modulenmenus);
//                     modulename = element.modulename;
//                     modulenameEnd = element.modulename;
//                     appsid = element.idapplication;
//                     menusModuleObj = [];
//                   } else {
//                     modulename = element.modulename;
//                     modulenameEnd = element.modulename;
//                     appsid = element.idapplication;
//                     console.log(">>>>>>> modulename awal >>>> " + modulename);
//                     menusModuleObj = [];
//                   }
//                 }
//                 menusModuleObj.push({
//                   label: element.title,
//                   routerLink: element.routelink,
//                 });
//               });
//               if (modulename === modulenameEnd) {
//                 console.log(
//                   ">>>>>>> Modul akhir " + (modulename === modulenameEnd)
//                 );
//                 let modulenmenus = {
//                   label: modulename,
//                   expanded: false,
//                   items: menusModuleObj,
//                 };
//                 // appsModuleList.push(modulenmenus);
//                 appsModuleList = [];
//                 appsModuleObj = { sidemenus: appsModuleList };
//               }
//               console.log(
//                 ">>>>>>> Kirim Balik " + JSON.stringify(appsModuleList)
//               );
//               jsonbody = { ...jsonbody, ...application, ...appsModuleObj };
//               var token = jwttools.encryptdata(jsonbody);
//               res.status(200).json({ status: 200, data: token });
//             }
//           }

//           jsonbody = { ...jsonbody, ...application, ...appsModuleObj };
//           var token = jwttools.encryptdata(jsonbody);
//           res.status(200).json({ status: 200, data: token });
//         }
//       } else {
//         //########### CHECK LEVEL TENANT
//         const liscenseapp = await pool.query(
//           "SELECT clis.id, clis.licensetokens, clis.licensejson, TO_CHAR(clis.expiredate,'yyyy-MM-dd') expiredate, cdid_tenant, clis.id_application, clis.paidstatus, clis.active, clis.defaultactive, mapp.appname, mapp.applabel,mapp.description FROM public.cdid_liscenses clis INNER JOIN public.mst_application mapp ON clis.id_application = mapp.id where clis.cdid_tenant=$1 and clis.active=1 order by clis.defaultactive desc;",
//           [jsonbody.idtenant]
//         );
//         var application = {};
//         if (liscenseapp.rows) {
//           countapp = liscenseapp.rows.length;
//           // appObject = liscenseapp.rows;
//           if (jsonbody.leveltenant > 1) {
//             appObject = liscenseapp.rows;
//           } else {
//             var itemApp = liscenseapp.rows[0];
//             appObject.push(itemApp);
//           }

//           var menusModuleObj = [];
//           var appsModuleList = [];
//           var appsModuleObj = {};
//           application = { appscount: countapp, apps: appObject };
//         }
//         jsonbody = { ...jsonbody, ...application };
//         var token = jwttools.encryptdata(jsonbody);
//         res.status(200).json({ status: 200, data: token });
//       }
//     } else {
//       res.status(500).json({ status: 500, data: "No User found" });
//     }
//   } catch (err) {
//     console.log(err);
//     res.status(500).json({ status: 500, data: "No User found" });
//   }
// });

// router.post("/changeapp", async (req, res, next) => {
//   try {
//     const { appid } = req.body;
//     let systemdate = new Date();
//     const authHeader = req.headers.authorization;
//     var dcodeInfo = null;
//     if (authHeader) {
//       TokenArray = authHeader.split(" ");
//       dcodeInfo = await jwttools.decryptdata(TokenArray[1]);
//       var idTenant =
//         dcodeInfo.leveltenant > 1 ? dcodeInfo.idsubtenant : dcodeInfo.idtenant;
//       var query =
//         "SELECT lapp.id,lapp.expiredate,lapp.cdid_tenant,lapp.paidstatus, lapp.active,1 defaultactive,lapp.appname,lapp.applabel, oau.userid, oau.orgid, oau.grpid, mo.orgname, oapp.idapp,lapp.routelink FROM public.cdid_orguserassign oau INNER JOIN mst_organizations mo ON oau.orgid=mo.id INNER JOIN cdid_orgapplications oapp ON mo.id=oapp.idorg LEFT JOIN (SELECT clis.id, clis.licensetokens, clis.licensejson, TO_CHAR(clis.expiredate,'yyyy-MM-dd') expiredate, cdid_tenant, clis.id_application, clis.paidstatus, clis.active, clis.defaultactive, mapp.appname, mapp.applabel,mapp.description,mapp.routelink FROM public.cdid_liscenses clis INNER JOIN public.mst_application mapp ON clis.id_application = mapp.id where clis.cdid_tenant=$1 and clis.active=1 order by clis.defaultactive desc) lapp ON oapp.idapp = lapp.id_application where oau.userid = $2 and oapp.idapp=$3;";

//       // var query = "SELECT clis.id, clis.licensetokens, TO_CHAR(clis.expiredate,'yyyy-MM-dd') expiredate, cdid_tenant, clis.id_application, clis.paidstatus, clis.active, 1 defaultactive, mapp.appname, mapp.applabel FROM public.cdid_liscenses clis INNER JOIN public.mst_application mapp ON clis.id_application = mapp.id where clis.cdid_tenant=$1 and clis.active=1 and clis.id_application=$2"
//       // console.log(">>> JUMLAH Liscense APP "+query);
//       const liscenseapp = await pool.query(query, [
//         idTenant,
//         dcodeInfo.id,
//         appid,
//       ]);
//       console.log(">>> JUMLAH Liscense APP " + liscenseapp.rows.length);
//       if (liscenseapp.rows.length > 0) {
//         countapp = liscenseapp.rows.length;
//         appObject = liscenseapp.rows;

//         console.log(">>> APP Active :" + JSON.stringify(appObject));

//         var menusModuleObj = [];
//         var appsModuleList = [];
//         var appsModuleObj = {};
//         var application = { appscount: countapp, apps: appObject };
//         let modulename = "";
//         let modulenameEnd = "";
//         var queryMenu;
//         var ps;
//         /// Select side menu
//         console.log(">>> Level tenant " + dcodeInfo.leveltenant);
//         switch (parseInt(dcodeInfo.leveltenant)) {
//           case 0:
//             queryMenu =
//               "SELECT mm.id, mm.modulename,mm.status, mm.idapplication, mm.modulecode, mm.moduletype, mnu.name, mnu.title, mnu.routelink FROM public.mst_module mm INNER JOIN mst_menu mnu ON mm.id = mnu.idmodule WHERE mm.idapplication=$1 and mm.moduletype=1";
//             ps = [appid];
//             break;
//           case 1:
//             let checkUser = await pool.query(
//               "SELECT * FROM public.cdid_tenant_user_groupacl where iduser = $1 ",
//               [dcodeInfo.id]
//             );
//             let userAssignData = checkUser.rowCount;
//             //console.log("line 508 " + dcodeInfo.id);
//             console.log("line 508 " + userAssignData.grpid);

//             queryMenu =
//               userAssignData > 0
//                 ? "SELECT distinct on (mug.idmenu) mug.idmenu, tug.iduser, tug.idgroupuseracl,mmdl.idapplication,mmdl.modulename, mmnu.name, mmnu.title, mmnu.routelink, mmnu.routepath,mug.fcreate,mug.fread,mug.fupdate,mug.fdelete,mug.fview,mug.fapproval FROM public.cdid_tenant_user_groupacl tug INNER JOIN public.mst_usergroupacl mug ON tug.idgroupuseracl = mug.idgroup INNER JOIN mst_module mmdl ON mug.idmodule = mmdl.id INNER JOIN mst_menu mmnu ON mug.idmenu = mmnu.id WHERE tug.iduser= $1 order by mug.idmenu, mmdl.idapplication"
//                 : "SELECT mm.id, mm.modulename,mm.status, mm.idapplication, mm.modulecode, mm.moduletype, mnu.name, mnu.title, mnu.routelink FROM public.mst_module mm INNER JOIN mst_menu mnu ON mm.id = mnu.idmodule WHERE mm.idapplication=$1 and mm.moduletype=1";
//             ps = userAssignData > 0 ? [dcodeInfo.id] : [appid];
//             break;
//           case 2:
//             let checkUserACL = await pool.query(
//               "SELECT * FROM public.cdid_tenant_user_groupacl where iduser = $1 ",
//               [dcodeInfo.id]
//             );
//             let userAssignDataACL = checkUserACL.rowCount;
//             queryMenu =
//               userAssignDataACL > 0
//                 ? "SELECT distinct on (mug.idmenu) mug.idmenu, tug.iduser, tug.idgroupuseracl,mmdl.idapplication,mmdl.modulename, mmnu.name, mmnu.title, mmnu.routelink, mmnu.routepath,mug.fcreate,mug.fread,mug.fupdate,mug.fdelete,mug.fview,mug.fapproval FROM public.cdid_tenant_user_groupacl tug INNER JOIN public.mst_usergroupacl mug ON tug.idgroupuseracl = mug.idgroup INNER JOIN mst_module mmdl ON mug.idmodule = mmdl.id INNER JOIN mst_menu mmnu ON mug.idmenu = mmnu.id WHERE tug.iduser= $1 order by mug.idmenu, mmdl.idapplication"
//                 : "SELECT mm.id, mm.modulename,mm.status, mm.idapplication, mm.modulecode, mm.moduletype, mnu.name, mnu.title, mnu.routelink FROM public.mst_module mm INNER JOIN mst_menu mnu ON mm.id = mnu.idmodule WHERE mm.idapplication=$1 and mm.moduletype=1";
//             ps = userAssignDataACL > 0 ? [dcodeInfo.id] : [appid];
//             break;
//           default:
//             var queryMenu =
//               "SELECT mm.id, mm.modulename,mm.status, mm.idapplication, mm.modulecode, mm.moduletype, mnu.name, mnu.title, mnu.routelink FROM public.mst_module mm INNER JOIN mst_menu mnu ON mm.id = mnu.idmodule WHERE mm.idapplication=$1 and mm.moduletype=1";
//             var ps = [appid];
//             break;
//         }
//         /*
//         var queryMenu =
//           dcodeInfo.leveltenant > 1
//             ? "SELECT distinct on (mug.idmenu) mug.idmenu, tug.iduser, tug.idgroupuseracl,mmdl.idapplication,mmdl.modulename, mmnu.name, mmnu.title, mmnu.routelink, mmnu.routepath,mug.fcreate,mug.fread,mug.fupdate,mug.fdelete,mug.fview,mug.fapproval FROM public.cdid_tenant_user_groupacl tug INNER JOIN public.mst_usergroupacl mug ON tug.idgroupuseracl = mug.idgroup INNER JOIN mst_module mmdl ON mug.idmodule = mmdl.id INNER JOIN mst_menu mmnu ON mug.idmenu = mmnu.id WHERE tug.iduser= $1 order by mug.idmenu, mmdl.idapplication"
//             : "SELECT mm.id, mm.modulename,mm.status, mm.idapplication, mm.modulecode, mm.moduletype, mnu.name, mnu.title, mnu.routelink FROM public.mst_module mm INNER JOIN mst_menu mnu ON mm.id = mnu.idmodule WHERE mm.idapplication=$1 and mm.moduletype=1";
//         var ps = dcodeInfo.leveltenant > 1 ? [dcodeInfo.id] : [appid];
//         */
//         // SELECT distinct on (mug.idmenu) mug.idmenu, tug.iduser, tug.idgroupuseracl,mmdl.idapplication,mmdl.modulename, mmnu.name, mmnu.title, mmnu.routelink, mmnu.routepath,mug.fcreate,mug.fread,mug.fupdate,mug.fdelete,mug.fview,mug.fapproval FROM public.cdid_tenant_user_groupacl tug INNER JOIN public.mst_usergroupacl mug ON tug.idgroupuseracl = mug.idgroup INNER JOIN mst_module mmdl ON mug.idmodule = mmdl.id INNER JOIN mst_menu mmnu ON mug.idmenu = mmnu.id WHERE tug.iduser=$1 order by mug.idmenu, mmdl.idapplication

//         // console.log(">>> JUMLAH Module APP "+queryMenu);
//         const getmoduleadminapps = await pool.query(queryMenu, ps);
//         console.log(">>> JUMLAH Module APP " + getmoduleadminapps.rows.length);

//         if (getmoduleadminapps.rows.length > 0) {
//           let modelesTmp = getmoduleadminapps.rows;
//           modelesTmp.forEach((element) => {
//             if (element.modulename != modulename) {
//               if (modulename != "") {
//                 let modulenmenus = {
//                   label: modulename,
//                   expanded: false,
//                   items: menusModuleObj,
//                 };
//                 appsModuleList.push(modulenmenus);
//                 modulename = element.modulename;
//                 modulenameEnd = element.modulename;
//                 appsid = element.idapplication;
//                 menusModuleObj = [];
//               } else {
//                 modulename = element.modulename;
//                 modulenameEnd = element.modulename;
//                 appsid = element.idapplication;
//                 console.log(">>>>>>> modulename awal >>>> " + modulename);
//                 menusModuleObj = [];
//               }
//             }
//             menusModuleObj.push({
//               label: element.title,
//               routerLink: element.routelink,
//             });
//           });
//           /*
//           if (modulename === modulenameEnd) {
//             console.log(
//               ">>>>>>> Modul akhir " + (modulename === modulenameEnd)
//             );
//             let modulenmenus = {
//               label: modulename,
//               expanded: false,
//               items: menusModuleObj,
//             };
//             console.log("asldn");
//             console.log(menusModuleObj);
//             appsModuleList.map((mdl) => {
//               if (mdl.label === modulename) {
//                 console.log("asldn");
//                 console.log(menusModuleObj);
//                 menusModuleObj.map((dt) => {
//                   mdl.items.push(dt);
//                 });

//                 appsModuleObj = { sidemenus: appsModuleList };
//               } else {
//                 appsModuleList.push(modulenmenus);
//                 appsModuleObj = { sidemenus: appsModuleList };
//               }
//             });
//           }*/

//           if (modulename === modulenameEnd) {
//             console.log(
//               ">>>>>>> Modul akhir " + (modulename === modulenameEnd)
//             );

//             let modulenmenus = {
//               label: modulename,

//               expanded: false,

//               items: menusModuleObj,
//             };

//             appsModuleList.push(modulenmenus);

//             appsModuleObj = { sidemenus: appsModuleList };
//           }

//           dcodeInfo = { ...dcodeInfo, ...application, ...appsModuleObj };
//           console.log(">>>>>>> Kirim Balik " + JSON.stringify(dcodeInfo));
//           var token = jwttools.encryptdata(dcodeInfo);
//           res.status(200).json({ status: 200, data: token });
//         }
//       }
//     } else {
//       res.status(500).json({ status: 500, data: "Not authorized" });
//     }

//     res.status(200).json({ status: 200, data: dcodeInfo });
//   } catch (err) {
//     console.log(err);
//     res.status(500).json({ status: 500, data: "Error Change App Catch" });
//   }
// });