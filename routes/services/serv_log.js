const express = require("express");

const router = express.Router();

const checkAuth = require("../../middleware/check-auth");
const path = require("path");
const moment = require("moment");
const fs = require("fs");
const axios = require("axios");
const systemParam = require("./utils/getSystemParam");

router.use(checkAuth);
router.get("/getSystemLog", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;

  try {
    const fileData = fs.readFileSync(
      path.resolve(__dirname, "../../log/komi-logs.log"),
      "utf8"
    );
    //console.log(fileData.toString());
    res.status(200).json({ status: 200, data: fileData.toString() });
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.get("/getInBoundlog", async (req, res, next) => {
  try {
    let param = await systemParam.getAllParam();

    let komiCoreLog = await axios.get(param.KOMI_CORE_INBOUND_URL, {
      auth: {
        username: param.KOMI_CORE_INBOUND_USERNAME,
        password: param.KOMI_CORE_INBOUND_PASSWORD,
      },
    });
    res.status(200).json({ status: 200, data: komiCoreLog.data.toString() });
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: err });
  }
});

router.get("/getOutBoundLog", async (req, res, next) => {
  let param = await systemParam.getAllParam();
  try {
    let komiCoreLog = await axios.get(param.KOMI_CORE_OUTBOUND_URL, {
      auth: {
        username: param.KOMI_CORE_OUTBOUND_USERNAME,
        password: param.KOMI_CORE_OUTBOUND_PASSWORD,
      },
    });
    res.status(200).json({ status: 200, data: komiCoreLog.data.toString() });
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: err });
  }
});

module.exports = router;
