const express = require("express"),
  app = express();
const router = express.Router();
const checkAuth = require("../../middleware/check-auth");
router.use(checkAuth);
const knex = require("../../connection/dborm");
var conf = require("../../config.json");
const { body, validationResult } = require("express-validator");
const conn = knex.conn();

//getAllGroup
router.get("/getAll", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  var idtenant = dcodeInfo.idtenant;

  try {
    const bicAdmins = await conn
      .select(
        "id",
        "bank_code",
        "bic_bank",
        "bank_name",
        "status",
        conn
          .raw(`TO_CHAR("created_date", 'dd MON yyyy')`)
          .wrap("(", ") created_date"),
        "created_by",
        conn
          .raw(`TO_CHAR("updated_date", 'dd MON yyyy')`)
          .wrap("(", ") updated_date"),
        "updated_by"
      )
      .from("m_bic")
      .orderBy("created_date", "DESC");

    if (bicAdmins.length > 0) {
      setTimeout(function () {
        res.status(200).json({
          status: 200,
          data: bicAdmins,
        });
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({
          status: 202,
          data: "Data not found",
        });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({
      status: 500,
      data: "Internal Server Error",
    });
  }
});

router.post(
  "/insert",
  body("bankCode", "Bank Code  maximum 5 character").isLength({
    max: 5,
  }),
  body("bicBank", "BIC Bank maximum 35 character").isLength({
    max: 35,
  }),
  body("bankName", "Bank Name maximum 100 character").isLength({
    max: 100,
  }),
  async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        success: false,
        errors: errors.array(),
      });
    }
    var dcodeInfo = req.userData;
    const userId = dcodeInfo.id;
    const idtenant = dcodeInfo.idtenant;

    try {
      const { bankCode, bicBank, bankName, status } = req.body;

      // Check Already exist
      const bicExsist = await conn
        .select("*")
        .from("m_bic")
        .where("bank_code", bankCode)
        .andWhere("bic_bank", bicBank)
        .andWhere("bank_name", bankName);

      if (bicExsist.length > 0) {
        res.status(200).json({
          status: 409,
          data: "Data Already Exist",
        });
      } else {
        const resp = await conn("m_bic")
          .insert({
            bank_code: bankCode,
            bic_bank: bicBank,
            bank_name: bankName,
            status: status,
            created_date: conn.fn.now(),
            created_by: userId,
          })
          .returning(["id"]);
        console.log(resp);
        if (resp.length > 0) {
          console.log("Success Insert Bank Participant data");
          res.status(200).json({
            status: 200,
            data: "Success insert data",
          });
        } else {
          console.log("Failed Insert Bank Participant data");
          res.status(200).json({
            status: 500,
            data: "Failed insert data",
          });
        }
      }
    } catch (err) {
      console.log("Failed Insert Bank Participant data");
      console.log(err);
      res.status(500).json({
        status: 500,
        data: "Internal Server Error",
      });
    }
  }
);

router.post(
  "/update",
  body("bankCode", "Bank Code  maximum 5 character").isLength({
    max: 5,
  }),
  body("bicBank", "BIC Bank maximum 35 character").isLength({
    max: 35,
  }),
  body("bankName", "Bank Name maximum 100 character").isLength({
    max: 100,
  }),
  async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        success: false,
        errors: errors.array(),
      });
    }
    var dcodeInfo = req.userData;
    const userId = dcodeInfo.id;

    try {
      const { id, bankCode, bicBank, bankName, status } = req.body;

      // Check Already exist
      const bicExsist = await conn
        .select("*")
        .from("m_bic")
        .where("bank_code", bankCode)
        .andWhere("bic_bank", bicBank)
        .andWhere("bank_name", bankName)
        .andWhere("status", status);

      if (bicExsist.length > 0) {
        res.status(200).json({
          status: 409,
          data: "Data already updated",
        });
      } else {
        const resp = await conn("m_bic").where("id", id).update({
          bank_code: bankCode,
          bic_bank: bicBank,
          bank_name: bankName,
          status: status,
          updated_date: conn.fn.now(),
          updated_by: userId,
        });
        if (resp > 0) {
          console.log("Success update Bank Participant data");
          res.status(200).json({
            status: 200,
            data: resp,
          });
        } else {
          console.log("Failed update Bank Participant data");
          res.status(200).json({
            status: 500,
            data: "Failed update data",
          });
        }
      }
    } catch (err) {
      console.log("Failed update Bank Participant data");
      console.log(err);
      res.status(500).json({
        status: 500,
        data: "Internal server error",
      });
    }
  }
);

router.get("/delete/:id", async (req, res, next) => {
  var dcodeInfo = req.userData;
  let id = req.params.id;

  try {
    let resp = await conn("m_bic").where("id", id).del();
    console.log("Success delete Bank Participant data");
    res.status(200).json({
      status: 200,
      data: "Success",
    });
  } catch (err) {
    console.log("Failed delete Bank Participant data");
    console.log(err);
    res.status(500).json({
      status: 500,
      data: "Error delete data",
    });
  }
});

router.get("/getBic/:id", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;

  try {
    let bankcode = req.params.id;
    const bicAdmins = await conn
      .select(
        "id",
        "bank_code",
        "bic_bank",
        "bank_name",
        "status",
        "created_date",
        "created_by",
        "updated_date",
        "updated_by"
      )
      .from("m_bic")
      .where("bank_code", bankcode)
      .orderBy("created_date", "desc");

    if (bicAdmins.length > 0) {
      setTimeout(function () {
        res.status(200).json({
          status: 200,
          data: {
            bicAdmins: bicAdmins[0],
          },
        });
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({
          status: 202,
          data: "Data tidak ditemukan",
        });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({
      status: 500,
      data: "internal error",
    });
  }
});

module.exports = router;
