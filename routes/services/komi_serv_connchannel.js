const express = require("express"),
    app = express();
const router = express.Router();
const checkAuth = require("../../middleware/check-auth");
const knex = require("../../connection/dborm");


var conf = require("../../config.json");
const moment = require("moment");
const {duration} = require("moment-timezone");
const bcrypt = require('bcrypt');
const axios = require("axios");
const conn = knex.conn();
const apiserver = conf.kktserver;
router.use(checkAuth);

router.get("/getAll", async (req, res, next) => {
    try {

        const data = await conn.select("*", "channel_id as id").from("kc_channel");
        //console.log(data)
        if (data.length > 0) {
            setTimeout(function () {
                res
                    .status(200)
                    .json({status:200, data: { channel : data }});
            }, 500);
        } else {
            setTimeout(function () {
                res.status(200).json({
                    status: 202,
                    data: "Data tidak ditemukan"
                });
            }, 500);
        }
    } catch (e) {
        console.log(e);
        res.status(500).json({
            status: 500,
            data: "Internal Error"
        });
    }
});

router.get("/getById/:channel_id", async (req, res, next) => {

    try {
        let channel  = req.params.channel_id;
        //console.log(channel);

        const conChannelData = await conn
            .select('*')
            .from('kc_channel')
            .where('channel_id', channel);

        //console.log(conChannelData)

        if (conChannelData.length > 0) {
            res.status(200).json({ status: 200, data: { channel: conChannelData[0] } });
        } else {
            res.status(200).json({ status: 202, data: "Data tidak ditemukan" });
        }

    } catch (e) {
        console.log(e);
        res.status(500).json({ status: 500, data: "internal error" });
    }
});

router.post("/insert", async (req, res, next) => {
    var dcodeInfo = req.userData;
    //console.log(">>>>>>>>>>>>>>>.." + JSON.stringify(dcodeInfo));

    try {
        const apps = dcodeInfo.apps[0];
        const { channelId, channelName, channelType, merchantCode, secretKey, active} = req.body;
        const today = moment().format("YYYY-MM-DD HH:mm:ss");
        const password = bcrypt.hashSync(secretKey, 4);

        if (apps.withapproval > 0) {
            const resp = await conn("kc_channel")
                .insert({
                    channel_id: channelId,
                    channel_name: channelName,
                    channel_type: channelType,
                    merchant_code: merchantCode,
                    secret_key: password,
                    modif_dt: today,
                    create_dt: today,
                    active: 3,
                })
                .returning([
                    "channel_id",
                    "channel_name",
                    "channel_type",
                    "merchant_code",
                    "create_dt",
                    "modif_dt",
                    "active"
                ]);

            if (resp.length > 0) {
                res.status(201).json({
                    status: 200,
                    data: "Data inserted"
                });
            } else {
                res.status(500).json({
                    status: 500,
                    data: "Error to insert",
                });
            }
        } else {
            const resp = await conn("kc_channel")
                .insert({
                    channel_id: channelId,
                    channel_name: channelName,
                    channel_type: channelType,
                    merchant_code: merchantCode,
                    secret_key: password,
                    modif_dt: today,
                    create_dt: today,
                    active: active,
                })
                .returning([
                    "channel_id",
                    "channel_name",
                    "channel_type",
                    "merchant_code",
                    "create_dt",
                    "modif_dt",
                    "active",
                ]);

            if (resp.length > 0) {
                res.status(201).json({
                    status: 200,
                    data: "Data inserted"
                });
            } else {
                res.status(500).json({
                    status: 500,
                    data: "Error to insert",
                });
            }
        }

    } catch (e) {
        console.log(e);
        res.status(500).json({ status: 500, data: "Error insert Data" });
    }
});

router.post("/update", async (req, res, next) => {
    var dcodeInfo = req.userData;

    try {
        var apps = dcodeInfo.apps[0];
        const { channelId, channelName, channelType, merchantCode, secretKey, daily_limit_amount, active, idapproval } = req.body;
        const password = bcrypt.hashSync(secretKey, 4);

        const reqData = {
            idapp: apps.idapp,
            idtenant: dcodeInfo.idtenant,
            cdmenu: "CONCH",
            jsondata: JSON.stringify(req.body),
            status: 2,
            typedata: 7,
        };

        if (apps.withapproval > 0) {
            console.log("LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL " + apps.withapproval);
            axios({
                method: "post",
                url: conf.kktserver + "/adm/approval/insertApprovalTmp",
                headers: {},
                data: {
                    reqData: reqData,
                },
            }).then(async (resp) => {
                console.log("insertApprovalTmp", resp.status);
                if (resp.status == 200) {
                    var userObj = resp.data.data[0];
                    console.log("userObj", userObj);
                    console.log("userObj", userObj.id);
                    let idreturn = userObj.id;
                    const updateChannel = await conn("kc_channel")
                        .where("channel_id", channelId)
                        .update({
                            modif_dt: conn.fn.now(),
                            active: 7,
                            idapproval: idreturn,
                        });
                    if (updateChannel > 0) {
                        var userObj = updateChannel[0];
                        res.status(200).json({ status: 200, data: userObj });
                    } else {
                        res.status(500).json({
                            status: 500,
                            data: "Error insert kc_channel ",
                        });
                    }
                }
                // res.status(200).json({ status: 200, data: resp.data });
            });
        } else {
            const resp = await conn ("kc_channel")
                .where("channel_id", channelId)
                .update({
                    channel_name: channelName,
                    channel_type: channelType,
                    merchant_code: merchantCode,
                    secret_key: password,
                    daily_limit_amount: daily_limit_amount,
                    modif_dt:conn.fn.now(),
                })

            // crypt($1,gen_salt('bf',4))

            if (resp > 0) {
                res.status(200).json({
                    status: 200, data: "Data has been update" });
            } else {
                res.status(500).json({
                    status: 500,
                    data: "Error update data ",
                });
            }
        }

    } catch (e) {
        console.log(e);
        res.status(500).json({ status: 500, data: "Error insert data" });
    }
});

router.post("/getByParam", async (req, res, next) => {
    try {
        const { channelId, channelName, channelType } = req.body;

        const getByparam = await conn ("kc_channel")
            .select("*")
            .modify(function (queryBuilder){
                if (channelId) {
                    queryBuilder.where("channel_id", channelId);
                }
                if (channelName) {
                    queryBuilder.where("channel_name", channelName);
                }
                if (channelType) {
                    queryBuilder.where("channel_type", channelType);
                }
            });
        if (getByparam.length > 0) {
            res.status(200).json({ status: 200, data: getByparam });
        } else {
            res.status(200).json({
                status: 202,
                data: "No Content",
            });
        }

    } catch (e) {
        console.log(e);
        res.status(500).json({ status: 500, data: "internal error" });
    }
});

router.post("/delete", async (req, res, next) => {
    var dcodeInfo = req.userData;

    try {
        console.log("Delete Channel");
        var apps = dcodeInfo.apps[0];

        const { channel_id, channelName, channelType, merchantCode, secretKey, active} = req.body;
        const reqData = {
            idapp: apps.idapp,
            idtenant: dcodeInfo.idtenant,
            cdmenu: "CONCH",
            jsondata: JSON.stringify(req.body),
            status: 2,
            typedata: 9,
        };

        if (apps.withapproval > 0) {
            console.log("LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL " + apps.withapproval);
            axios({
                method: "post",
                url: conf.kktserver + "/adm/approval/insertApprovalTmp",
                headers: {},
                data: {
                    reqData: reqData,
                },
            }).then(async (resp) => {
                console.log("insertApprovalTmp", resp.status);
                if (resp.status == 200) {
                    var userObj = resp.data.data[0];
                    console.log("userObj", userObj);
                    console.log("userObj", userObj.id);
                    let idreturn = userObj.id;
                    const updateChannel = await conn("kc_channel")
                        .where("channel_id", channel_id)
                        .update({
                            modif_dt: conn.fn.now(),
                            active: 9,
                            idapproval: idreturn,
                        });
                    if (updateChannel > 0) {
                        var userObj = updateChannel[0];
                        res.status(200).json({ status: 200, data: userObj });
                    } else {
                        res.status(500).json({
                            status: 500,
                            data: "Error insert kc_channel ",
                        });
                    }
                }
                // res.status(200).json({ status: 200, data: resp.data });
            });
        } else {
            const resp = await conn("kc_channel")
                .where("channel_id", channel_id).del();

            if (resp > 0) {
                res.status(200).json({ status: 200, data: resp });
            } else {
                res.status(500).json({
                    status: 500,
                    data: "Error Delete CHANNELS",
                });
            }
        }

    } catch (e) {
        console.log(e);
        res.status(500).json({ status: 500, data: "Error retrive Users" });
    }
});

router.get("/getChannelId", async (req, res, next) => {
    try {
        const data = await conn.select("channel_id", "channel_type","channel_name")
            .from("kc_channel")

        console.log(data)

        if (data.length > 0) {
            setTimeout(function () {
                res
                    .status(200)
                    .json({status:200, data});
            }, 500);
        } else {
            setTimeout(function () {
                res.status(200).json({
                    status: 202,
                    data: "Data tidak ditemukan"
                });
            }, 500);
        }
    } catch (e) {

    }
});

router.post("/approveChannel", async (req, res, next) => {
    console.log("Approve Channel")
    var dcodeInfo = req.userData;
    try {
        var apps = dcodeInfo.apps[0];
        const { channelId, oldactive, isactive, idapproval } = req.body;
        let today = new Date();
        today.setHours(today.getHours() + 7);
        switch (oldactive) {
            case 3:
                const resp = await conn ("kc_channel")
                    .where("channel_id", channelId)
                    .update({
                        active: 1,
                        modif_dt: today,
                    })

                if (resp > 0) {
                    res.status(200).json({
                        status: 200, data: "Data has been update" });
                } else {
                    res.status(500).json({
                        status: 500,
                        data: "Error update data ",
                    });
                }
                break;
            case 7:
                console.log("LLLLLLLLLLLLLLLL")
                const reqData = {
                    id: idapproval,
                    status: 2,
                };
                axios({
                    method: "get",
                    url: apiserver + "/adm/approval/getApprovalTmp",
                    headers: {},
                    data: {
                        reqData: reqData,
                    },
                }).then(async (resp) => {
                    console.log("resp", resp.status);
                    if (resp.status == 200) {
                        let jsdata = resp.data.data[0];
                        let jdata = JSON.parse(jsdata.jsondata);
                        console.log("jdata", jdata);

                        let today = new Date();
                        today.setHours(today.getHours() + 7);

                        const respInsert7 =  await conn ("kc_channel")
                            .where("channel_id", channelId)
                            .update({
                                channel_name: jdata.channelName,
                                channel_type: jdata.channelType,
                                merchant_code: jdata.merchantCode,
                                secret_key: jdata.password,
                                daily_limit_amount: jdata.daily_limit_amount,
                                active: 1,
                                modif_dt: today,
                            });

                        if (respInsert7 > 0) {
                            // userObj = resp[0];
                            res.status(200).json({ status: 200, data: "Success" });
                        } else {
                            res
                                .status(500)
                                .json({ status: 500, data: "Error Update CHANNELS" });
                        }
                    }
                    // res.status(200).json({ status: 200, data: resp.data });
                }).catch(e => {
                    console.log(e)
                });

                break;
            case 9:
                const respDelete = await conn("kc_channel")
                    .where("channel_id", channelId).del();

                if (respDelete > 0) {
                    res.status(200).json({ status: 200, data: respDelete });
                } else {
                    res.status(500).json({
                        status: 500,
                        data: "Error Delete CHANNELS",
                    });
                }
                break;

            default:
        }
    } catch (e) {
        console.log(e);
    }
})

router.post("/rejectChannelTemp", async (req, res, next) => {
    var dcodeInfo = req.userData;
    var toknid = req.tokenID;
    var apps = dcodeInfo.apps[0];
    var userObj;

    try {
        const { channelId, oldactive, isactive, idapproval } = req.body;
        let today = new Date();
        today.setHours(today.getHours() + 7);
        switch (oldactive) {
            case 3:
                const respInsert3 =await conn("kc_channel").where("channel_id", channelId).del();
                if (respInsert3 > 0) {
                    res.status(200).json({ status: 200, data: "Success" });
                } else {
                    res.status(500).json({ status: 500, data: "Error Update CHANNELS" });
                }
                break;
            case 7:
                const respInsert7 = await conn("kc_channel").where("channel_id", channelId).update({
                    active: 1,
                });
                if (respInsert7 > 0) {
                    res.status(200).json({ status: 200, data: "Success" });
                } else {
                    res.status(500).json({ status: 500, data: "Error Update CHANNELS" });
                }

                break;
            case 9:
                const respDelete9 = await conn("kc_channel").where("channel_id", channelId).update({
                    active: 1,
                });
                if (respDelete9 > 0) {
                    res.status(200).json({ status: 200, data: "Success" });
                } else {
                    res.status(500).json({
                        status: 500,
                        data: "Error Update CHANNELS",
                    });
                }
                break;
            default:
        }
    } catch (e) {
        
    }
});

module.exports = router;
