const express = require("express"),
  app = express();
const router = express.Router();
const checkAuth = require("../../middleware/check-auth");
router.use(checkAuth);
const knex = require("../../connection/dborm");
var conf = require("../../config.json");
const { body, validationResult } = require("express-validator");
const conn = knex.conn();

//getAllGroup
router.get("/getAll", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  var idtenant = dcodeInfo.idtenant;

  try {
    const Branchs = await conn
      .select(
        "id",
        "branch_code",
        "branch_name",
        "branch_account_fee",
        "status",
        conn
          .raw(`TO_CHAR("created_date", 'dd MON yyyy')`)
          .wrap("(", ") created_date"),
        "created_by",
        conn
          .raw(`TO_CHAR("updated_date", 'dd MON yyyy')`)
          .wrap("(", ") updated_date"),
        "updated_by"
      )
      .from("m_branch")
      .orderBy("created_date", "DESC");
    // .select(
    //   "M_BRANCH.ID",
    //   "M_CITY.CITY_NAME",
    //   "M_CITY.CITY_CODE",
    //   "M_BRANCH.BRANCH_CODE",
    //   "M_BRANCH.BRANCH_NAME",
    //   "M_BRANCH.BRANCH_ACCOUNT_FEE",
    //   "M_BRANCH.STATUS",
    //   conn.raw("TO_CHAR(M_BRANCH.CREATED_DATE, 'dd MON yyyy')").wrap('(', ') CREATED_DATE'),
    //   "M_BRANCH.CREATED_BY",
    //   conn.raw("TO_CHAR(M_BRANCH.UPDATED_DATE, 'dd MON yyyy')").wrap('(', ') UPDATED_DATE'),
    //   "M_BRANCH.UPDATED_BY"
    // )
    // .from("M_BRANCH")
    // .innerJoin("M_CITY", "M_CITY.CITY_CODE", "M_BRANCH.CITY_CODE")
    // .orderBy("CREATED_DATE", "DESC");

    console.log(Branchs);
    if (Branchs.length > 0) {
      setTimeout(function () {
        res.status(200).json({
          status: 200,
          data: Branchs,
        });
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({
          status: 202,
          data: "Data not found",
        });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({
      status: 500,
      data: "Internal Server Error",
    });
  }
});

router.post(
  "/insert",
  body("branchCode", "Branch Code  maximum 10 character").isLength({
    max: 10,
  }),
  body("branchName", "Branch Name maximum 100 character").isLength({
    max: 100,
  }),
  body("branchAccountFee", "Branch Account Fee maximum 100 character").isLength(
    {
      max: 100,
    }
  ),
  async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        success: false,
        errors: errors.array(),
      });
    }
    var dcodeInfo = req.userData;
    const userId = dcodeInfo.id;
    const idtenant = dcodeInfo.idtenant;

    try {
      const { branchCode, branchName, branchAccountFee, status } = req.body;

      // Check Already exist
      const branchExsist = await conn
        .select("*")
        .from("m_branch")
        .andWhere("branch_code", branchCode)
        .andWhere("branch_name", branchName);

      if (branchExsist.length > 0) {
        res.status(200).json({
          status: 409,
          data: "Data Already Exist",
        });
      } else {
        const resp = await conn("m_branch")
          .insert({
            branch_code: branchCode,
            branch_name: branchName,
            branch_account_fee: branchAccountFee,
            status: status,
            created_date: conn.fn.now(),
            created_by: userId,
          })
          .returning(["id"]);
        console.log(resp);
        if (resp.length > 0) {
          console.log("Success Insert Branch data");
          res.status(200).json({
            status: 200,
            data: "Success insert data",
          });
        } else {
          console.log("Failed Insert Branch data");
          res.status(200).json({
            status: 500,
            data: "Failed insert data",
          });
        }
      }
    } catch (err) {
      console.log("Failed Insert Branch data");
      console.log(err);
      res.status(500).json({
        status: 500,
        data: "Internal Server Error",
      });
    }
  }
);

router.post(
  "/update",
  body("branchCode", "Branch Code  maximum 10 character").isLength({
    max: 10,
  }),
  body("branchName", "Branch Name maximum 100 character").isLength({
    max: 100,
  }),
  body("branchAccountFee", "Branch Account Fee maximum 100 character").isLength(
    {
      max: 100,
    }
  ),
  async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        success: false,
        errors: errors.array(),
      });
    }
    var dcodeInfo = req.userData;
    const userId = dcodeInfo.id;

    try {
      const { id, branchCode, branchName, branchAccountFee, status } = req.body;

      // Check Already exist
      const branchExsist = await conn
        .select("*")
        .from("m_branch")
        .where("branch_code", branchCode)
        .andWhere("branch_name", branchName)
        .andWhere("branch_account_fee", branchAccountFee)
        .andWhere("status", status);

      if (branchExsist.length > 0) {
        res.status(200).json({
          status: 409,
          data: "Data already updated",
        });
      } else {
        const resp = await conn("m_branch").where("id", id).update({
          branch_code: branchCode,
          branch_name: branchName,
          branch_account_fee: branchAccountFee,
          status: status,
          updated_date: conn.fn.now(),
          updated_by: userId,
        });
        if (resp > 0) {
          console.log("Success update Branch data");
          res.status(200).json({
            status: 200,
            data: resp,
          });
        } else {
          console.log("Failed update Branch data");
          res.status(200).json({
            status: 500,
            data: "Failed update data",
          });
        }
      }
    } catch (err) {
      console.log("Failed update Branch data");
      console.log(err);
      res.status(500).json({
        status: 500,
        data: "Internal server error",
      });
    }
  }
);

router.get("/delete/:id", async (req, res, next) => {
  var dcodeInfo = req.userData;
  let id = req.params.id;

  try {
    let resp = await conn("m_branch").where("id", id).del();
    console.log("Success delete Branch data");
    res.status(200).json({
      status: 200,
      data: "Success",
    });
  } catch (err) {
    console.log("Failed delete Branch data");
    console.log(err);
    res.status(500).json({
      status: 500,
      data: "Error delete data",
    });
  }
});

router.get("/getBranch/:id", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  // var apps = dcodeInfo.apps[0];
  try {
    let branchid = req.params.id;
    // console.log(">>>>>>>>>>>> Tenanr, Level "+JSON.stringify(dcodeInfo.leveltenant));
    //   console.log(dcodeInfo.leveltenant);TO_CHAR(clis.expiredate,'yyyy-MM-dd')
    // var query =
    //   "SELECT  id, citycode, cityname, branchcode, branchname, status FROM m_branch WHERE id=$1 order by created_date desc";
    // const Branchs = await pool.query(query, [branchid]);

    const Branchs = await conn
      .select(
        "m_branch.id",
        "m_city.city_name",
        "m_branch.branch_code",
        "m_branch.branch_name",
        "m_branch.branch_account_fee",
        "m_branch.status",
        conn
          .raw("TO_CHAR(m_branch.created_date, 'dd MON yyyy')")
          .wrap("(", ") created_date"),
        "m_branch.created_by",
        conn
          .raw("TO_CHAR(m_branch.updated_date, 'dd MON yyyy')")
          .wrap("(", ") updated_date"),
        "m_branch.updated_by"
      )
      .from("m_branch")
      .innerJoin("m_city", "m_city.city_code", "m_branch.city_code")
      .where("id", branchid)
      .orderBy("created_date", "DESC");

    if (Branchs.length > 0) {
      setTimeout(function () {
        res.status(200).json({
          status: 200,
          data: {
            Branchs: Branchs[0],
          },
        });
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({
          status: 202,
          data: "Data tidak ditemukan",
        });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({
      status: 500,
      data: "internal error",
    });
  }
});

router.get("/getByStatusActive", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  var idtenant = dcodeInfo.idtenant;

  try {
    const Branchs = await conn
      .select(
        "id",
        "branch_code",
        "branch_name",
        "branch_account_fee",
        "status",
        conn
          .raw(
            `TO_CHAR("created_date"
          , 'dd MON yyyy')`
          )
          .wrap("(", ") created_date"),
        "CREATED_BY",
        conn
          .raw(`TO_CHAR("updated_date", 'dd MON yyyy')`)
          .wrap("(", ") updated_date"),
        "updated_by"
      )
      .from("m_branch")
      .where("status", "1")
      .orderBy("created_date", "DESC");

    if (Branchs.length > 0) {
      console.log("Success Get Branch data");
      setTimeout(function () {
        res.status(200).json({
          status: 200,
          data: Branchs,
        });
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({
          status: 202,
          data: "Data not found",
        });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({
      status: 500,
      data: "Internal Server Error",
    });
  }
});

module.exports = router;
