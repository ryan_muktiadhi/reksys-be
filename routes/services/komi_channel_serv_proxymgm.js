const express = require("express"),
  app = express();
const router = express.Router();
const jwttools = require("./utils/encryptdecryptjwt");
const groupUtils = require("./utils/groupUtils");
const pool = require("../../connection/db");
const checkAuth = require("../../middleware/check-auth");
const moment = require("moment");
const { fork } = require("child_process");
const knex = require("../../connection/dborm");

var conf = require("../../config.json");
// const conn
const conn = knex.conn();
router.use(checkAuth);

router.post("/", async (req, res, next) => {
  var dcodeInfo = req.userData;
  var apps = dcodeInfo.apps[0];

  try {
    const today = moment().format("YYYY-MM-DD");

    const reqProxyMgm = JSON.stringify({
      proxyManagementRequest: {
        transactionId: "211110123456",
        cid: "",
        channelType: "MBL",
        transactionCode: "300000",
        proxyOperationType: "NEWR",
        proxyType: "01",
        proxyAlias: "0818888777666",
        customerAccountNumber: "0411920000100",
        otpCode: "H26XN7",
        customerId: "3376021207810001",
        branchCode: "4001",
        branchInput: "",
        registrationId: "",
        registrationStatus: "",
        customerType: "01",
        customerResidentStatus: "01",
        customerCityCode: "0191",
        customerAccountType: "SVGS",
        customerAccountName: "JOHN SMITH",
        customerId: "3376021207810001",
        customerSecondaryType: "01",
        customerSecondaryValue: "3376021207810001",
        customerCurrency: "IDR",
        cifNumber: "111222",
        displayName: "MR JOHN SMITH",
        userInput: "",
      },
    });

    if (bicAdmins.length > 0) {
      setTimeout(function () {
        res.status(200).json({ status: 200, data: bicAdmins });
      }, 1500);
    } else {
      setTimeout(function () {
        res.status(200).json({ status: 202, data: {} });
      }, 1500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

module.exports = router;
