const express = require("express"),
  app = express();
const router = express.Router();
const checkAuth = require("../../middleware/check-auth");
router.use(checkAuth);
const knex = require("../../connection/dborm");
// const conn
const conn = knex.conn();

//getAllGroup
router.get("/getAll", async (req, res, next) => {
  var dcodeInfo = req.userData;

  try {
    const limitData = await conn
      .select(
        "id",
        "limit_key",
        "limit_value",
        "description",
        "status",
        conn
          .raw(`TO_CHAR("created_date", 'dd MON yyyy')`)
          .wrap("(", ") created_date"),
        "created_by",
        conn
          .raw(`TO_CHAR("update_date", 'dd MON yyyy')`)
          .wrap("(", ") update_date"),
        "update_by"
      )
      .from("m_limit")
      .orderBy("created_date", "DESC");

    if (limitData.length > 0) {
      setTimeout(function () {
        res.status(200).json({
          status: 200,
          data: limitData,
        });
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({
          status: 202,
          data: "Data not found",
        });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({
      status: 500,
      data: "Internal Server Error",
    });
  }
});

router.post("/insert", async (req, res, next) => {
  var dcodeInfo = req.userData;
  const userId = dcodeInfo.id;

  try {
    const { limitKey, limitValue, description, status } = req.body;

    // Check Already exsist
    const limitExsist = await conn
      .select("limit_key")
      .from("m_limit")
      .where("limit_key", limitKey)
      .andWhere("limit_value", limitValue);

    if (limitExsist.length > 0) {
      res.status(200).json({
        status: 409,
        data: "Data Already Exist",
      });
    } else {
      const resp = await conn("m_limit")
        .insert({
          limit_key: limitKey,
          limit_value: limitValue,
          description: description,
          status: status,
          created_date: conn.fn.now(),
          created_by: userId,
        })
        .returning(["id"]);

      if (resp.length > 0) {
        console.log("Success insert Limit data");
        res.status(200).json({
          status: 200,
          data: "Success insert data",
        });
      } else {
        console.log("Failed insert Limit data");
        res.status(200).json({
          status: 500,
          data: "Failed insert data",
        });
      }
    }
  } catch (err) {
    console.log("Failed insert Limit data");
    console.log(err);
    res.status(500).json({
      status: 500,
      data: "Internal Server Error",
    });
  }
});

router.post("/update", async (req, res, next) => {
  var dcodeInfo = req.userData;
  const userId = dcodeInfo.id;

  try {
    const { id, limitKey, limitValue, description, status } = req.body;

    // Check Already exsist
    const limitExsist = await conn
      .select("*")
      .from("m_limit")
      .where("limit_key", limitKey)
      .andWhere("limit_value", limitValue)
      .andWhere("description", description)
      .andWhere("status", status);

    if (limitExsist.length > 0) {
      res.status(200).json({
        status: 204,
        data: "Data already updated",
      });
    } else {
      const resp = await conn("m_limit").where("id", id).update({
        limit_key: limitKey,
        limit_value: limitValue,
        description: description,
        status: status,
        update_date: conn.fn.now(),
        update_by: userId,
      });
      console.log("Success update Limit data");
      res.status(200).json({
        status: 200,
        data: resp,
      });
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({
      status: 500,
      data: "Internal server error",
    });
  }
});

router.get("/delete/:id", async (req, res, next) => {
  var dcodeInfo = req.userData;
  let id = req.params.id;

  try {
    let resp = await conn("m_limit").where("id", id).del();
    res.status(200).json({
      status: 200,
      data: "Success delete data",
    });
  } catch (err) {
    res.status(500).json({
      status: 500,
      data: "Error delete data",
    });
  }
});

router.get("/getById/:id", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;

  try {
    let limitId = req.params.id;
    const limitData = await conn
      .select(
        "id",
        "limit_key",
        "limit_value",
        "description",
        "status",
        "created_date",
        "created_by",
        "update_date",
        "update_by"
      )
      .from("m_limit")
      .where("id", limitId)
      .orderBy("created_date", "desc");

    if (limitData.length > 0) {
      console.log("Success get Limit data by id");
      setTimeout(function () {
        res.status(200).json({
          status: 200,
          data: {
            limit: limitData[0],
          },
        });
      }, 500);
    } else {
      setTimeout(function () {
        console.log("No Limit data");
        res.status(200).json({
          status: 202,
          data: "Data tidak ditemukan",
        });
      }, 500);
    }
  } catch (err) {
    console.log("Failed get Limit data");
    console.log(err);
    res.status(500).json({
      status: 500,
      data: "internal error",
    });
  }
});
module.exports = router;
