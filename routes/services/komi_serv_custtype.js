const express = require("express"),
  app = express();
const router = express.Router();
const checkAuth = require("../../middleware/check-auth");
// const axios = require('axios')
router.use(checkAuth);
const { body, validationResult } = require("express-validator");
const knex = require("../../connection/dborm");
const conn = knex.conn();

//getAllGroup
router.get("/getAll", async (req, res, next) => {
  var dcodeInfo = req.userData;
  const idtenant = dcodeInfo.idtenant;

  try {
    const custTypeData = await conn
      .select(
        "id",
        "bank_cust_type",
        "bi_cust_type",
        "description",
        "status",
        conn
          .raw(`TO_CHAR("created_date", 'dd MON yyyy')`)
          .wrap("(", ") created_date"),
        "created_by",
        conn
          .raw(`TO_CHAR("updated_date", 'dd MON yyyy')`)
          .wrap("(", ") updated_date"),
        "updated_by"
      )
      .from("m_customertype")
      .orderBy("created_date", "DESC");

    if (custTypeData.length > 0) {
      setTimeout(function () {
        res.status(200).json({
          status: 200,
          data: custTypeData,
        });
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({
          status: 202,
          data: "Data not found",
        });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({
      status: 500,
      data: "Internal Server Error",
    });
  }
});

router.post(
  "/insert",
  body("bankCustType", "Bank Code  maximum 5 character").isLength({
    max: 10,
  }),
  body("biCustType", "BIC Bank maximum 5 character").isLength({
    max: 5,
  }),

  async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        success: false,
        errors: errors.array(),
      });
    }
    var dcodeInfo = req.userData;
    const userId = dcodeInfo.id;
    const idtenant = dcodeInfo.idtenant;

    try {
      const { bankCustType, biCustType, description, status } = req.body;

      // Check Already exsist
      const custTypeExsist = await conn
        .select("*")
        .from("m_customertype")
        .where("bank_cust_type", bankCustType)
        .andWhere("bi_cust_type", biCustType);

      if (custTypeExsist.length > 0) {
        res.status(200).json({
          status: 409,
          data: "Data Already Exist",
        });
      } else {
        const resp = await conn("m_customertype")
          .insert({
            bank_cust_type: bankCustType,
            bi_cust_type: biCustType,
            description: description,
            status: status,
            created_date: conn.fn.now(),
            created_by: userId,
          })
          .returning(["id"]);

        if (resp.length > 0) {
          console.log("Success Insert Customer Type data");
          res.status(200).json({
            status: 200,
            data: "Success insert data",
          });
        } else {
          console.log("Failed insert Customer Type data");
          res.status(200).json({
            status: 500,
            data: "Failed insert data",
          });
        }
      }
    } catch (err) {
      console.log("Failed insert Customer Type data");
      console.log(err);
      res.status(500).json({
        status: 500,
        data: "Internal Server Error",
      });
    }
  }
);

router.post(
  "/update",
  body("bankCustType", "Bank Code  maximum 5 character").isLength({
    max: 10,
  }),
  body("biCustType", "BIC Bank maximum 5 character").isLength({
    max: 5,
  }),

  async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        success: false,
        errors: errors.array(),
      });
    }
    var dcodeInfo = req.userData;
    const userId = dcodeInfo.id;

    try {
      const { id, bankCustType, biCustType, description, status } = req.body;

      // Check eksist data
      const custTypeExsist = await conn
        .select("*")
        .from("m_customertype")
        .where("bank_cust_type", bankCustType)
        .andWhere("bi_cust_type", biCustType)
        .andWhere("description", description)
        .andWhere("status", status);

      if (custTypeExsist.length > 0) {
        res.status(200).json({
          status: 204,
          data: "Data already updated",
        });
      } else {
        const resp = await conn("m_customertype").where("id", id).update({
          bank_cust_type: bankCustType,
          bi_cust_type: biCustType,
          description: description,
          status: status,
          updated_date: conn.fn.now(),
          updated_by: userId,
        });

        if (resp > 0) {
          console.log("Success update Customer Type data");
          res.status(200).json({
            status: 200,
            data: resp,
          });
        } else {
          console.log("Failed update Customer Type data");
          res.status(200).json({
            status: 500,
            data: "Failed update data",
          });
        }
      }
    } catch (err) {
      console.log("Failed update Customer Type data");
      console.log(err);
      res.status(500).json({
        status: 500,
        data: "Internal server error",
      });
    }
  }
);

router.get("/delete/:id", async (req, res, next) => {
  var dcodeInfo = req.userData;
  let id = req.params.id;

  console.log("Success delete Customer Type data" + id);
  try {
    let resp = await conn("m_customertype").where("id", id).del();
    console.log("Success delete Customer Type data");
    res.status(200).json({
      status: 200,
      data: "Success",
    });
  } catch (err) {
    console.log("Failed delete Customer Type data");
    console.log(err);
    console.log(err);
    res.status(500).json({
      status: 500,
      data: "Error delete data",
    });
  }
});

router.get("/getById/:id", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;

  try {
    let id = req.params.id;

    const custTypeData = await conn
      .select(
        "id",
        "bank_cust_type",
        "bi_cust_type",
        "description",
        "status",
        "created_date",
        "created_by",
        "updated_date",
        "updated_by"
      )
      .from("m_customertype")
      .where("id", id)
      .orderBy("created_date", "desc");

    if (custTypeData.length > 0) {
      setTimeout(function () {
        console.log("Success get Customer Type data");
        res.status(200).json({
          status: 200,
          data: {
            custType: custTypeData[0],
          },
        });
      }, 500);
    } else {
      setTimeout(function () {
        console.log("No Customer Type data");
        res.status(200).json({
          status: 202,
          data: "Data tidak ditemukan",
        });
      }, 500);
    }
  } catch (err) {
    console.log("Failed get Customer Type data");
    console.log(err);
    res.status(500).json({
      status: 500,
      data: "Internal server error",
    });
  }
});

router.get("/getByStatusActive", async (req, res, next) => {
  var dcodeInfo = req.userData;

  try {
    const idTypeData = await conn
      .select("*")
      .from("m_customer_id_type")
      .where("status", "1")
      .orderBy("created_date", "ASC");

    if (idTypeData.length > 0) {
      setTimeout(function () {
        res.status(200).json({ status: 200, data: idTypeData });
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({ status: 202, data: "Data tidak ditemukan" });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

module.exports = router;
