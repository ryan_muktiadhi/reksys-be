const axios = require('axios');

const request = async (options, body) => { 
  // console.log(body);
  const response = await axios({
      method: options.method,
      url: options.url,
      headers: options.headers,
      data: body
    })
    .then(function (response) {
      console.log(response);
      return response
    })
    .catch(function (error) {
      console.log(error);
      return error
    });
    
    return response;
}

module.exports = {
    request
};