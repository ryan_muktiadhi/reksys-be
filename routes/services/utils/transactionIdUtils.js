const moment = require("moment");

const seq = async (seq) => {
    // Get today date
    const today = moment().format("YYYYMMDD");
    
    // Set sequence
    var strseq = "" + seq
    var pad = "000000"
    var seqNum = pad.substring(0, pad.length - strseq.length) + strseq

    const seqNumber = today + seqNum ;

    return seqNumber;
};

module.exports = {
   seq
};