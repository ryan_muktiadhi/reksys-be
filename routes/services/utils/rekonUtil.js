var jsonxml = require("jsontoxml");

const generateCompareXML = async (matchingField1, matchingField2) => {
  try {
    let compareParams1 = await matchingField1.map((field, index) => {
      return {
        entry: [
          { name: "string", text: index + 1 },
          { name: "string", text: field },
        ],
      };
    });

    const comparatorObj1 = {
      string: "mapPropertyNames1",
      "linked-hash-map": [compareParams1],
    };

    let compareParams2 = await matchingField2.map((field, index) => {
      return {
        entry: [
          { name: "string", text: index + 1 },
          { name: "string", text: field },
        ],
      };
    });

    const comparatorObj2 = {
      string: "mapPropertyNames2",
      "linked-hash-map": [compareParams2],
    };

    const mergedCompareObj = {
      "linked-hash-map": [
        { name: "entry", text: jsonxml(comparatorObj1) },
        { name: "entry", text: jsonxml(comparatorObj2) },
      ],
    };

    let comparatorParams = await jsonxml(mergedCompareObj);
    return comparatorParams;
  } catch (err) {
    console.log(err);
    return;
  }
};

const generateReportXML = async (reportField1, reportField2) => {
  try {
    let reportDisplay1 = await reportField1.map((field) => {
      return { string: field };
    });

    let reportLabel1 = await reportField1.map((field) => {
      return {
        entry: [
          { name: "string", text: field },
          { name: "string", text: field },
        ],
      };
    });

    let reportDisplay2 = await reportField2.map((field) => {
      return { string: field };
    });

    let reportLabel2 = await reportField2.map((field) => {
      return {
        entry: [
          { name: "string", text: field },
          { name: "string", text: field },
        ],
      };
    });

    const reportParamsObj = {
      "linked-hash-map": [
        {
          entry: {
            string: "refPropDisplay",
            list: [reportDisplay2],
          },
        },
        {
          entry: {
            string: "refPropLabel",
            "linked-hash-map": [reportLabel2],
          },
        },
        {
          entry: {
            string: "billedPropDisplay",
            list: [reportDisplay1],
          },
        },
        {
          entry: {
            string: "billedPropLabel",
            "linked-hash-map": [reportLabel1],
          },
        },
      ],
    };

    let reportParams = await jsonxml(reportParamsObj);
    return reportParams;
  } catch (err) {
    console.log(err);
    return;
  }
};

module.exports = {
  generateCompareXML,
  generateReportXML,
};
