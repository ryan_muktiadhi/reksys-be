const ADD_DATA_SUCCESS = "Berhasil tambah data";
const ADD_DATA_ALREADY_EXSIST = "Gagal Tambah data, data sudah ada";
const ADD_DATA_FAILED = "Gagal tambah data";

const UPDATA_DATA_SUCCESS = "Berhasil update data";
const UPDATE_DATA_FAILED = "Gagal update data";
const UPDATE_DATA_DOSENT_EXSIST = "Gagal update data, data tidak ditemukan";

const DELETE_DATA_SUCCESS = "Berhasil hapus data";
const DELETE_DATA_FAILED = "Gagal hapus data";



module.exports = {
    ADD_DATA_SUCCESS,
    ADD_DATA_ALREADY_EXSIST,
    ADD_DATA_FAILED,
    UPDATA_DATA_SUCCESS,
    UPDATE_DATA_FAILED,
    UPDATE_DATA_DOSENT_EXSIST
 };