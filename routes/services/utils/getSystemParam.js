const knex = require("../../../connection/dborm");
const conn = knex.conn();

const getAllParam = async () => {
  let param = await conn("m_systemparam").select("*");
  let rowparam = param;
  let obj = {};
  rowparam.forEach(function (column) {
    var columnName = column.paramname;
    obj[columnName] = column.paramvalua;
  });
  //console.log(obj);

  return obj;
};

module.exports = { getAllParam };
