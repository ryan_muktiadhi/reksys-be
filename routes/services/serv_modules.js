const express = require("express"),
  app = express();
const router = express.Router();
const pool = require("../../connection/db");
const jwttools = require("../../routes/services/utils/encryptdecryptjwt");
const logger = require("../../routes/services/utils/logservices");
const moment = require("moment");
const checkAuth = require("../../middleware/check-auth");
router.use(checkAuth);
/*POST END*/

//******************** Insert bulk atau Update ke table mst_menususers  */
router.post("/updatemmacl", async (req, res, next) => {
  try {
    const { credential, secret } = req.body;
    const resp = await pool.query(
      "SELECT tu.id, tu.fullname, tu.userid, tu.pwd, tu.idtenant, tu.idsubtenant, tu.leveltenant, tn.tnname, tn.tnstatus, tn.tnflag, tn.tnparentid, bio.bioname, bio.bioemailactive, bio.biophoneactive, bio.bioaddress, bio.bionik, bio.bionpwp, bio.bioidtipenik FROM public.cdid_tenant_user tu INNER JOIN public.cdid_tenant tn ON tu.idtenant=tn.id INNER JOIN (select * from public.mst_biodata_corell where bioidcorel=3) bio ON tu.id = bio.biocorelobjid WHERE tu.userid=$1 and tu.pwd=crypt($2, pwd)",
      [credential, secret]
    );
    var jsonbody = resp.rows[0];
    var token = jwttools.encryptdata({
      id: jsonbody.id,
      fullname: jsonbody.fullname,
      userid: jsonbody.userid,
      idtenant: jsonbody.idtenant,
      idsubtenant: jsonbody.idsubtenant,
      leveltenant: jsonbody.leveltenant,
      tnname: jsonbody.tnname,
      tnflag: jsonbody.tnflag,
      tnparentid: jsonbody.tnparentid,
      bioname: jsonbody.bioname,
      bioemailactive: jsonbody.bioemailactive,
      biophoneactive: jsonbody.biophoneactive,
      bioaddress: jsonbody.bioaddress,
      bionik: jsonbody.bionik,
      bionpwp: jsonbody.bionpwp,
      bioidtipenik: jsonbody.bioidtipenik,
    });
    let tokenEncoded = { token: token };
    res.status(200).json({ status: 200, data: token });
    // }
  } catch (err) {
    res.status(500).json({ status: 500, data: "No User found" });
  }
});

router.put("/updatestat/:id", async (req, res, next) => {
  try {
    const {
      id,
      modulename,
      created_byid,
      status,
      idapplication,
      modulecode,
      applabel,
    } = req.body;
    const resp = await pool.query(
      "update public.mst_moduleby_tenant SET status=$1 WHERE id=$2 RETURNING *",
      [status, req.params.id]
    );
    var jsonbody = resp.rows[0];

    res.status(200).json({ status: 200, data: jsonbody });
    // }
  } catch (err) {
    res.status(500).json({ status: 500, data: "No User found" });
  }
});

router.get("/", async (req, res, next) => {
  try {
    let systemdate = new Date();
    const authHeader = req.headers.authorization;
    var dcodeInfo = null;
    if (authHeader) {
      TokenArray = authHeader.split(" ");
      dcodeInfo = await jwttools.decryptdata(TokenArray[1]);
    } else {
      res.status(500).json({ status: 500, data: "Not authorized" });
    }

    res.status(200).json({ status: 200, data: dcodeInfo });
  } catch (err) {
    res.status(500).json({ status: 500, data: "Error insert log Catch" });
  }
});

router.get("/:id", async (req, res, next) => {
  try {
    let systemdate = new Date();
    const authHeader = req.headers.authorization;
    var dcodeInfo = req.userData;
    
      var tipeModule = 1;
      // const resp = await pool.query("SELECT mmod.id, mmod.modulename, mmod.created_byid, mmod.status, mmod.idapplication, mmod.modulecode, mapp.applabel FROM public.mst_module mmod INNER JOIN mst_application mapp ON mmod.idapplication = mapp.id WHERE mmod.idapplication = $1;",[req.params.id]);
      console.log(">>>>>>>>>>>> " + dcodeInfo.idtenant);
      var query =
        "SELECT mmod.id, mmod.modulename, mmod.created_byid, mmod.status, mmod.idapplication, mmod.modulecode, mapp.applabel FROM public.mst_moduleby_tenant mmod INNER JOIN mst_application mapp ON mmod.idapplication = mapp.id WHERE mmod.idapplication = $1 and mmod.idtenant=$2 order by mmod.id;";
      if (dcodeInfo.leveltenant !== "0") {
        query =
          "SELECT mmod.id, mmod.modulename, mmod.created_byid, mmod.status, mmod.idapplication, mmod.modulecode, mapp.applabel, 1 as view FROM public.mst_moduleby_tenant mmod INNER JOIN mst_application mapp ON mmod.idapplication = mapp.id WHERE mmod.idapplication = $1 and mmod.idtenant=$2 and mmod.moduletype>=1 order by mmod.id;";
      }

      const resp = await pool.query(query, [req.params.id, dcodeInfo.idtenant]);

      var jsonbody = resp.rows;

      res.status(200).json({ status: 200, data: jsonbody });
    
  } catch (err) {
    res.status(500).json({ status: 500, data: "Error insert log Catch" });
  }
});

router.get("/menusmodule/:id", async (req, res, next) => {
  try {
    let systemdate = new Date();
    const authHeader = req.headers.authorization;
    var dcodeInfo = null;
    //console.log(authHeader);
    if (authHeader) {
      TokenArray = authHeader.split(" ");
      dcodeInfo = await jwttools.decryptdata(TokenArray[1]);
      let module = {};
      let menus = [];
      const resp = await pool.query(
        "SELECT  id,modulename from public.mst_moduleby_tenant WHERE id = $1;",
        [req.params.id]
      );
      module = resp.rows[0];
      console.log(req.params.id);
      let menusData = await pool.query(
        "SELECT mmen.idmodule,mmen.id,mmen.title,mmod.modulename FROM public.mst_moduleby_tenant mmod INNER JOIN mst_menu mmen ON mmod.id = mmen.idmodule  WHERE mmod.id = $1;",
        [req.params.id]
      );
      menus = menusData.rows;

      module.menus = menus;
      let jsonbody = module;
      res.status(200).json({ status: 200, data: jsonbody });
    } else {
      res.status(500).json({ status: 500, data: "Not authorized" });
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error insert log Catch" });
  }
});

router.get("/modulesgroup/:id", async (req, res, next) => {
  try {
    let systemdate = new Date();
    const authHeader = req.headers.authorization;
    var dcodeInfo = null;
    if (authHeader) {
      TokenArray = authHeader.split(" ");
      dcodeInfo = await jwttools.decryptdata(TokenArray[1]);
      const resp = await pool.query(
        "SELECT mtg.groupname, mtg.idtenant, mtg.created_byid, mtg.created_at, mtg.updated_at, mtg.id, mtg.issubgroup, mtg.idapplication, mtg.idowner, mapp.appname, mapp.applabel FROM public.mst_tenantgroup mtg INNER JOIN mst_application mapp ON mapp.id = mtg.idapplication WHERE mtg.idapplication=$1 and mtg.idtenant=$2;",
        [req.params.id, dcodeInfo.idtenant]
      );
      var jsonbody = resp.rows;
      res.status(200).json({ status: 200, data: jsonbody });
    } else {
      res.status(500).json({ status: 500, data: "Not authorized" });
    }

    res.status(200).json({ status: 200, data: dcodeInfo });
  } catch (err) {
    res.status(500).json({ status: 500, data: "Error insert log Catch" });
  }
});

router.get("/menusmodule/:id/:groupid", async (req, res, next) => {
  try {
    var dcodeInfo = req.userData;
      let module = {};
      let menus = [];
      const resp = await pool.query(
        "SELECT  id,modulename from public.mst_moduleby_tenant WHERE id = $1;",
        [req.params.id]
      );
      module = resp.rows[0];
      let menusDataWithAcl = await pool.query(
        "SELECT mmen.idmodule,mmen.id,uga.fread,uga.fupdate,uga.fdelete,uga.fcreate,mmen.title,mmod.modulename FROM public.mst_moduleby_tenant mmod INNER JOIN mst_menu mmen ON mmod.id = mmen.idmodule  right join mst_usergroupacl uga on mmen.id = uga.idmenu WHERE mmod.id = $1 and uga.idgroup = $2",
        [req.params.id, req.params.groupid]
      );
      let menusById = await pool.query(
        "Select * from mst_menu where idmodule = $1",
        [req.params.id]
      );
      let mergedMenus = [];
      await menusById.rows.map(async (mid) => {
        let mergedMenu = {
          fcreate: 0,
          fdelete: 0,
          fread: 0,
          fupdate: 0,
          id: "",
          idmodule: "",
          modulename: "",
          title: "",
        };

        mergedMenu.id = mid.id;
        mergedMenu.modulename = mid.name;
        mergedMenu.title = mid.title;
        mergedMenus.push(mergedMenu);
      });

      menus = await mergedMenus.map((mergMen) => ({
        ...mergMen,
        ...menusDataWithAcl.rows.find((acmMen) => acmMen.id === mergMen.id),
      }));
      module.menus = menus;
      let jsonbody = module;

      res.status(200).json({ status: 200, data: jsonbody });
    
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error insert log Catch" });
  }
});

module.exports = router;


// router.get("/:id", async (req, res, next) => {
//   try {
//     let systemdate = new Date();
//     const authHeader = req.headers.authorization;
//     var dcodeInfo = null;
//     if (authHeader) {
//       TokenArray = authHeader.split(" ");
//       dcodeInfo = await jwttools.decryptdata(TokenArray[1]);
//       var tipeModule = 1;
//       // const resp = await pool.query("SELECT mmod.id, mmod.modulename, mmod.created_byid, mmod.status, mmod.idapplication, mmod.modulecode, mapp.applabel FROM public.mst_module mmod INNER JOIN mst_application mapp ON mmod.idapplication = mapp.id WHERE mmod.idapplication = $1;",[req.params.id]);
//       console.log(">>>>>>>>>>>> " + dcodeInfo.idtenant);
//       var query =
//         "SELECT mmod.id, mmod.modulename, mmod.created_byid, mmod.status, mmod.idapplication, mmod.modulecode, mapp.applabel FROM public.mst_moduleby_tenant mmod INNER JOIN mst_application mapp ON mmod.idapplication = mapp.id WHERE mmod.idapplication = $1 and mmod.idtenant=$2 order by mmod.id;";
//       if (dcodeInfo.leveltenant !== "0") {
//         query =
//           "SELECT mmod.id, mmod.modulename, mmod.created_byid, mmod.status, mmod.idapplication, mmod.modulecode, mapp.applabel FROM public.mst_moduleby_tenant mmod INNER JOIN mst_application mapp ON mmod.idapplication = mapp.id WHERE mmod.idapplication = $1 and mmod.idtenant=$2 and mmod.moduletype>=1 order by mmod.id;";
//       }

//       const resp = await pool.query(query, [req.params.id, dcodeInfo.idtenant]);

//       var jsonbody = resp.rows;

//       res.status(200).json({ status: 200, data: jsonbody });
//     } else {
//       res.status(500).json({ status: 500, data: "Not authorized" });
//     }
//   } catch (err) {
//     res.status(500).json({ status: 500, data: "Error insert log Catch" });
//   }
// });



// router.get("/menusmodule/:id/:groupid", async (req, res, next) => {
//   try {
//     let systemdate = new Date();
//     const authHeader = req.headers.authorization;
//     var dcodeInfo = null;
//     //console.log(authHeader);
//     if (authHeader) {
//       TokenArray = authHeader.split(" ");
//       dcodeInfo = await jwttools.decryptdata(TokenArray[1]);
//       let module = {};
//       let menus = [];
//       const resp = await pool.query(
//         "SELECT  id,modulename from public.mst_moduleby_tenant WHERE id = $1;",
//         [req.params.id]
//       );
//       module = resp.rows[0];
//       let menusDataWithAcl = await pool.query(
//         "SELECT mmen.idmodule,mmen.id,uga.fread,uga.fupdate,uga.fdelete,uga.fcreate,mmen.title,mmod.modulename FROM public.mst_moduleby_tenant mmod INNER JOIN mst_menu mmen ON mmod.id = mmen.idmodule  inner join mst_usergroupacl uga on mmen.id = uga.idmenu WHERE mmod.id = $1 and uga.idgroup = $2",
//         [req.params.id, req.params.groupid]
//       );
//       let menusById = await pool.query(
//         "Select * from mst_menu where idmodule = $1",
//         [req.params.id]
//       );
//       let mergedMenus = [];
//       await menusById.rows.map(async (mid) => {
//         let mergedMenu = {
//           fcreate: 0,
//           fdelete: 0,
//           fread: 0,
//           fupdate: 0,
//           id: "",
//           idmodule: "",
//           modulename: "",
//           title: "",
//         };

//         mergedMenu.id = mid.id;
//         mergedMenu.modulename = mid.name;
//         mergedMenu.title = mid.title;
//         mergedMenus.push(mergedMenu);
//       });

//       menus = await mergedMenus.map((mergMen) => ({
//         ...mergMen,
//         ...menusDataWithAcl.rows.find((acmMen) => acmMen.id === mergMen.id),
//       }));
//       module.menus = menus;
//       let jsonbody = module;

//       res.status(200).json({ status: 200, data: jsonbody });
//     } else {
//       res.status(500).json({ status: 500, data: "Not authorized" });
//     }
//   } catch (err) {
//     console.log(err);
//     res.status(500).json({ status: 500, data: "Error insert log Catch" });
//   }
// });