const express = require("express"),
  app = express();
const router = express.Router();
const checkAuth = require("../../middleware/check-auth");
const moment = require("moment");
router.use(checkAuth);
const { body, validationResult } = require("express-validator");
const knex = require("../../connection/dborm");

// const conn
const conn = knex.conn();

//getAllGroup
router.get("/getAll", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;

  try {
    const channelType = await conn
      .select(
        "id",
        "cid",
        "komi_channel_id",
        "bank_channel_type",
        "iso_merchant_type",
        "bi_channel_id",
        "ip_address",
        "description",
        "status",
        "key",
        "is_authorized",
        conn
          .raw(`TO_CHAR("created_date", 'dd MON yyyy')`)
          .wrap("(", ") created_date"),
        "created_by",
        conn
          .raw(`TO_CHAR("updated_date", 'dd MON yyyy')`)
          .wrap("(", ") updated_date"),
        "updated_by"
      )
      .from("m_channeltype")
      .orderBy("created_date", "DESC");

    if (channelType.length > 0) {
      setTimeout(function () {
        console.log("Success get Channel Type data");
        res.status(200).json({
          status: 200,
          data: channelType,
        });
      }, 500);
    } else {
      setTimeout(function () {
        console.log("Failed get Channel Type data");
        res.status(200).json({
          status: 202,
          data: {},
        });
      }, 500);
    }
  } catch (err) {
    console.log("Failed get Channel Type data");
    console.log(err);
    res.status(500).json({
      status: 500,
      data: "internal error",
    });
  }
});

router.post(
  "/insert",
  body("cid", "CID maximum 16 character").isLength({
    max: 16,
  }),
  body("bankChannelType", "Bank Channel Type maximum 10 character").isLength({
    max: 10,
  }),
  body("isoMerchantType", "ISO Merchant Type maximum 4 character").isLength({
    max: 4,
  }),
  body("komiChannelId", "KOMI Channel Id maximum 2 character").isLength({
    max: 2,
  }),
  body("biChannelId", "BI Channel Id maximum 2 character").isLength({
    max: 2,
  }),
  body("ipAddress", "IP Address maximum 15 character").isLength({
    max: 15,
  }),
  body("description", "Description maximum 100 character").isLength({
    max: 100,
  }),
  body("key", "Key maximum 25 character").isLength({
    max: 25,
  }),
  async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        success: false,
        errors: errors.array(),
      });
    }
    var dcodeInfo = req.userData;
    const userId = dcodeInfo.id;
    const idtenant = dcodeInfo.idtenant;

    try {
      const {
        cid,
        komiChannelId,
        bankChannelType,
        isoMerchantType,
        biChannelId,
        ipAddress,
        description,
        status,
        key,
        isAuthorized,
      } = req.body;

      // Check Already exist
      const channelTypeExsist = await conn
        .select("*")
        .from("m_channeltype")
        .where("bank_channel_type", bankChannelType)
        .andWhere("iso_merchant_type", isoMerchantType)
        .andWhere("cid", cid);

      if (channelTypeExsist.length > 0) {
        res.status(200).json({
          status: 409,
          data: "Data Already Exist",
        });
      } else {
        const resp = await conn("m_channeltype")
          .insert({
            cid: cid,
            komi_channel_id: komiChannelId,
            bank_channel_type: bankChannelType,
            iso_merchant_type: isoMerchantType,
            bi_channel_id: biChannelId,
            ip_address: ipAddress,
            description: description,
            status: status,
            key: key,
            is_authorized: isAuthorized,
            created_date: conn.fn.now(),
            created_by: userId,
          })
          .returning(["id"]);
        console.log(resp);
        if (resp.length > 0) {
          console.log("Success Insert Channel Type data");
          res.status(200).json({
            status: 200,
            data: "Success insert data",
          });
        } else {
          console.log("Failed Insert Channel Type data");
          res.status(200).json({
            status: 500,
            data: "Failed insert data",
          });
        }
      }
    } catch (err) {
      console.log("Failed Insert Channel Type data");
      console.log(err);
      res.status(500).json({
        status: 500,
        data: "Internal Server Error",
      });
    }
  }
);

router.post(
  "/update",
  body("cid", "CID maximum 16 character").isLength({
    max: 16,
  }),
  body("bankChannelType", "Bank Channel Type maximum 10 character").isLength({
    max: 10,
  }),
  body("isoMerchantType", "ISO Merchant Type maximum 4 character").isLength({
    max: 4,
  }),
  body("komiChannelId", "KOMI Channel Id maximum 2 character").isLength({
    max: 2,
  }),
  body("biChannelId", "BI Channel Id maximum 2 character").isLength({
    max: 2,
  }),
  body("ipAddress", "IP Address maximum 15 character").isLength({
    max: 15,
  }),
  body("description", "Description maximum 100 character").isLength({
    max: 100,
  }),
  body("key", "Key maximum 25 character").isLength({
    max: 25,
  }),
  async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        success: false,
        errors: errors.array(),
      });
    }
    var dcodeInfo = req.userData;
    const userId = dcodeInfo.id;

    try {
      const {
        id,
        cid,
        komiChannelId,
        bankChannelType,
        isoMerchantType,
        biChannelId,
        ipAddress,
        description,
        status,
        key,
        isAuthorized,
      } = req.body;

      // Check Already exsist
      const channelTypeExsist = await conn
        .select("*")
        .from("m_channeltype")
        .where("bank_channel_type", bankChannelType)
        .andWhere("cid", cid)
        .andWhere("komi_channel_id", komiChannelId)
        .andWhere("iso_merchant_type", isoMerchantType)
        .andWhere("bi_channel_id", biChannelId)
        .andWhere("ip_address", ipAddress)
        .andWhere("key", key)
        .andWhere("description", description)
        .andWhere("status", status)
        .andWhere("is_authorized", isAuthorized);

      if (channelTypeExsist.length > 0) {
        res.status(200).json({
          status: 204,
          data: "Data already updated",
        });
      } else {
        const resp = await conn("m_channeltype").where("id", id).update({
          bank_channel_type: bankChannelType,
          iso_merchant_type: isoMerchantType,
          bi_channel_id: biChannelId,
          ip_address: ipAddress,
          description: description,
          status: status,
          key: key,
          updated_date: conn.fn.now(),
          updated_by: userId,
        });

        if (resp > 0) {
          console.log("Success update Channel Type data");
          res.status(200).json({
            status: 200,
            data: resp,
          });
        } else {
          console.log("Failed update Channel Type data");
          res.status(200).json({
            status: 500,
            data: "Failed update data",
          });
        }
      }
    } catch (err) {
      console.log("Failed update Channel Type data");
      console.log(err);
      res.status(500).json({
        status: 500,
        data: "Internal server error",
      });
    }
  }
);

router.get("/delete/:id", async (req, res, next) => {
  var dcodeInfo = req.userData;
  let id = req.params.id;

  try {
    let resp = await conn("m_channeltype").where("id", id).del();
    console.log("Success delete Channel Type data");
    res.status(200).json({
      status: 200,
      data: "Success",
    });
  } catch (err) {
    console.log("Failed delete Channel Type data");
    console.log(err);
    res.status(500).json({
      status: 500,
      data: "Error delete data",
    });
  }
});

router.get("/getById", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;

  try {
    let id = req.params.id;
    const channelType = await conn
      .select(
        "id",
        "bank_channel_type",
        "iso_merchant_type",
        "bi_channel_id",
        "ip_address",
        "description",
        "status",
        "key",
        "created_date",
        "created_by",
        "updated_date",
        "updated_by"
      )
      .from("m_channeltype")
      .where("id", id)
      .orderBy("created_date", "desc");

    if (channelType.length > 0) {
      setTimeout(function () {
        console.log("Success get Channel Type data by id");
        res.status(200).json({
          status: 200,
          data: channelType[0],
        });
      }, 500);
    } else {
      setTimeout(function () {
        console.log("Failed get Channel Type data by id");
        res.status(200).json({
          status: 202,
          data: {},
        });
      }, 500);
    }
  } catch (err) {
    console.log("Failed get Channel Type data by id");
    console.log(err);
    res.status(500).json({
      status: 500,
      data: "internal error",
    });
  }
});

router.get("/getAllConChannel", async (req, res, next) => {
  try {
    const data = await conn.select("*").from("kc_channel");
    console.log(data);
    if (data.length > 0) {
      setTimeout(function () {
        res.status(200).json({ status: 200, data: { channel: data } });
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({
          status: 202,
          data: "Data tidak ditemukan",
        });
      }, 500);
    }
  } catch (e) {
    console.log(err);
    res.status(500).json({
      status: 500,
      data: "Internal Error",
    });
  }
});

module.exports = router;
