const express = require("express"),
  app = express();
const router = express.Router();
const pool = require("../../connection/db");
const jwttools = require("../../routes/services/utils/encryptdecryptjwt");
const logger = require("../../routes/services/utils/logservices");
const moment = require("moment");
const format = require("pg-format");
const checkAuth = require("../../middleware/check-auth");
router.use(checkAuth);
router.get("/allbytenantid", async (req, res, next) => {
  try {
    let systemdate = new Date();
    const dcodeInfo = req.userData;
    const idtrx = req.tokenID;
    // console.log("User data : "+JSON.stringify(dcodeInfo));
    // res.status(200).json({ status: 200, data: dcodeInfo });
    // const authHeader = req.headers.authorization;
      var tipeModule = 1;
      // const resp = await pool.query("SELECT mmod.id, mmod.modulename, mmod.created_byid, mmod.status, mmod.idapplication, mmod.modulecode, mapp.applabel FROM public.mst_module mmod INNER JOIN mst_application mapp ON mmod.idapplication = mapp.id WHERE mmod.idapplication = $1;",[req.params.id]);
      // console.log(">>>>>>>>>>>> "+dcodeInfo.leveltenant)
      var query =
        "SELECT mo.id, mo.orgname, mo.orgdescription, mo.idowner, mo.idtenant, mo.idsubtenant, mo.orglevel, mo.created_byid, mtu.fullname created_by, mo.orgcode FROM public.mst_organizations mo INNER JOIN cdid_tenant_user mtu ON mo.created_byid = mtu.id where mo.idtenant = $1;";
      const resp = await pool.query(query, [dcodeInfo.idtenant]);
      var jsonbody = resp.rows;
      setTimeout(function() {
        res.status(200).json({ status: 200, data: jsonbody });
      }, 500);
  } catch (err) {
    res.status(500).json({ status: 500, data: "Error insert log Catch" });
  }
});
router.post("/addorganization", async (req, res, next) => {
  try {
    let systemdate = new Date();
    const authHeader = req.headers.authorization;
    var dcodeInfo = req.userData;
      const { orgname, orgdescription, apps } = req.body;
      console.log(">>>> APPLICATION INSERT "+JSON.stringify(dcodeInfo));
      const resp = await pool.query(
        "INSERT INTO public.mst_organizations(orgname, orgdescription, idowner, idtenant, idsubtenant, orglevel, created_byid) VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING *;",
        [
          orgname,
          orgdescription,
          dcodeInfo.idtenant,
          dcodeInfo.idtenant,
          0,
          dcodeInfo.leveltenant,
          dcodeInfo.id,
        ]
      );
      if (resp.rows.length > 0) {
        // console.log(">>>> APPLICATION INSERT "+JSON.stringify(apps.length));
        var insertObj = resp.rows[0];
        if (apps.length > 0) {
          var idOrg = insertObj.id;
          let qrapp =
            "INSERT INTO public.cdid_orgapplications (idorg, idapp, idtenant, idsubtenant) VALUES ";
          let first = 0;
          apps.forEach((element) => {
            // console.log(">>>>> TEST "+JSON.stringify(element));
            if (first == 0) {
              // qrapp = qrapp+"("+idorg+", 0)";
              qrapp =
                qrapp +
                "(" +
                idOrg +
                ", " +
                element.id_application +
                ", " +
                dcodeInfo.idtenant +
                ", 0)";
            } else {
              // qrapp = qrapp+"("+idorg+", 0)";
              qrapp =
                qrapp +
                ", (" +
                idOrg +
                ", " +
                element.id_application +
                ", " +
                dcodeInfo.idtenant +
                ", 0)";
            }
            first++;
          });
          console.log(">>>>> BULK INSERT " + qrapp);
          const respApps = await pool.query(qrapp + " RETURNING *;", []);
          if (respApps.rows.length > 0) {
            res.status(200).json({ status: 200, data: resp.rows[0] });
          } else {
            res.status(500).json({
              status: 500,
              data: "No Application on the organizations",
            });
          }
        } else {
          res.status(200).json({ status: 200, data: resp.rows[0] });
        }
      } else {
      }

      // }
    
  } catch (err) {
    res.status(500).json({ status: 500, data: "Error insert organization" });
  }
});
router.get("/retriveByTenantAndOrgId/:id", async (req, res, next) => {
  try {
    let systemdate = new Date();
    const authHeader = req.headers.authorization;
    var dcodeInfo = req.userData;
      var tipeModule = 1;
      const orgid = req.params.id;
      var query =
        "SELECT mo.id, mo.orgname, mo.orgdescription, mo.idowner, mo.idtenant, mo.idsubtenant, mo.orglevel, mo.created_byid, mtu.fullname created_by, mo.orgcode FROM public.mst_organizations mo INNER JOIN cdid_tenant_user mtu ON mo.created_byid = mtu.id where mo.idtenant = $1 and mo.id=$2;";
      const resp = await pool.query(query, [dcodeInfo.idtenant, orgid]);

      var jsonbody = resp.rows;

      res.status(200).json({ status: 200, data: jsonbody });
   
  } catch (err) {
    res.status(500).json({ status: 500, data: "Error insert log Catch" });
  }
});

router.post("/editorganization", async (req, res, next) => {
  try {
    let systemdate = new Date();
    var dcodeInfo = req.userData;
      const { organization, apps } = req.body;
      console.log(
        organization.name,
        organization.description,
        Date.now(),
        organization.id
      );
      // console.log(">>>> APPLICATION INSERT "+JSON.stringify(apps));
      let orgResult = await pool.query(
        "UPDATE public.mst_organizations SET  orgname = $1, orgdescription = $2, updated_at = to_timestamp($3 / 1000.0) WHERE id = $4 Returning *;",
        [
          organization.name,
          organization.description,
          Date.now(),
          organization.id,
        ]
      );
      if (orgResult.rows.length > 0) {
        await pool.query(
          "DELETE FROM public.cdid_orgapplications where idorg = $1 ",
          [organization.id]
        );
        var appsToInsert = [];
        console.log("cek panjang apps: " + apps.length);
        if (apps.length > 0) {
          await apps.map((app) => {
            console.log(app);
            let appToInsert = [];
            appToInsert.push(organization.id);
            appToInsert.push(app.id_application);
            appToInsert.push(dcodeInfo.idtenant);
            appToInsert.push(0);
            appsToInsert.push(appToInsert);
          });
          let updateAppQuery = format(
            "INSERT INTO public.cdid_orgapplications (idorg, idapp, idtenant, idsubtenant) VALUES %L returning *",
            appsToInsert
          );
          let updateApp = await pool.query(updateAppQuery);
          if (updateApp.rows.length > 0) {
            res.status(200).json({ status: 200, data: updateApp.rows[0] });
          } else {
            res.status(500).json({
              status: 500,
              data: "No Application on the organizations",
            });
          }
        }
      }
    
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error insert organization" });
  }
});

router.post("/deleteorganization", async (req, res, next) => {
  try {
    let systemdate = new Date();
    var dcodeInfo = req.userData;
      const { organization } = req.body;
      console.log(organization);
      // console.log(">>>> APPLICATION INSERT "+JSON.stringify(apps));
      let resp = await pool.query(
        "DELETE FROM public.mst_organizations where id  = $1 ",
        [organization.id]
      );
      resp = await pool.query(
        "DELETE FROM public.cdid_orgapplications where idorg = $1 ",
        [organization.id]
      );
      res
        .status(200)
        .json({ status: 200, data: "Organization data deleted successfully" });
    
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error Delete organization" });
  }
});

module.exports = router;

// router.post("/addorganization", async (req, res, next) => {
//   try {
//     let systemdate = new Date();
//     const authHeader = req.headers.authorization;
//     var dcodeInfo = null;
//     if (authHeader) {
//       TokenArray = authHeader.split(" ");
//       dcodeInfo = await jwttools.decryptdata(TokenArray[1]);
//       const { orgname, orgdescription, apps } = req.body;
//       // console.log(">>>> APPLICATION INSERT "+JSON.stringify(apps));
//       const resp = await pool.query(
//         "INSERT INTO public.mst_organizations(orgname, orgdescription, idowner, idtenant, idsubtenant, orglevel, created_byid) VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING *;",
//         [
//           orgname,
//           orgdescription,
//           dcodeInfo.idtenant,
//           dcodeInfo.idtenant,
//           0,
//           dcodeInfo.leveltenant,
//           dcodeInfo.id,
//         ]
//       );
//       if (resp.rows.length > 0) {
//         // console.log(">>>> APPLICATION INSERT "+JSON.stringify(apps.length));
//         var insertObj = resp.rows[0];
//         if (apps.length > 0) {
//           var idOrg = insertObj.id;
//           let qrapp =
//             "INSERT INTO public.cdid_orgapplications (idorg, idapp, idtenant, idsubtenant) VALUES ";
//           let first = 0;
//           apps.forEach((element) => {
//             // console.log(">>>>> TEST "+JSON.stringify(element));
//             if (first == 0) {
//               // qrapp = qrapp+"("+idorg+", 0)";
//               qrapp =
//                 qrapp +
//                 "(" +
//                 idOrg +
//                 ", " +
//                 element.id_application +
//                 ", " +
//                 dcodeInfo.idtenant +
//                 ", 0)";
//             } else {
//               // qrapp = qrapp+"("+idorg+", 0)";
//               qrapp =
//                 qrapp +
//                 ", (" +
//                 idOrg +
//                 ", " +
//                 element.id_application +
//                 ", " +
//                 dcodeInfo.idtenant +
//                 ", 0)";
//             }
//             first++;
//           });
//           console.log(">>>>> BULK INSERT " + qrapp);
//           const respApps = await pool.query(qrapp + " RETURNING *;", []);
//           if (respApps.rows.length > 0) {
//             res.status(200).json({ status: 200, data: resp.rows[0] });
//           } else {
//             res.status(500).json({
//               status: 500,
//               data: "No Application on the organizations",
//             });
//           }
//         } else {
//           res.status(200).json({ status: 200, data: resp.rows[0] });
//         }
//       } else {
//       }

//       // }
//     } else {
//       res.status(500).json({ status: 500, data: "Not authorized" });
//     }
//   } catch (err) {
//     res.status(500).json({ status: 500, data: "Error insert organization" });
//   }
// });
// router.get("/retriveByTenantAndOrgId/:id", async (req, res, next) => {
//   try {
//     let systemdate = new Date();
//     const authHeader = req.headers.authorization;
//     var dcodeInfo = null;
//     if (authHeader) {
//       TokenArray = authHeader.split(" ");
//       dcodeInfo = await jwttools.decryptdata(TokenArray[1]);
//       var tipeModule = 1;
//       const orgid = req.params.id;
//       var query =
//         "SELECT mo.id, mo.orgname, mo.orgdescription, mo.idowner, mo.idtenant, mo.idsubtenant, mo.orglevel, mo.created_byid, mtu.fullname created_by, mo.orgcode FROM public.mst_organizations mo INNER JOIN cdid_tenant_user mtu ON mo.created_byid = mtu.id where mo.idtenant = $1 and mo.id=$2;";
//       const resp = await pool.query(query, [dcodeInfo.idtenant, orgid]);

//       var jsonbody = resp.rows;

//       res.status(200).json({ status: 200, data: jsonbody });
//     } else {
//       res.status(500).json({ status: 500, data: "Not authorized" });
//     }
//   } catch (err) {
//     res.status(500).json({ status: 500, data: "Error insert log Catch" });
//   }
// });

// router.post("/editorganization", async (req, res, next) => {
//   try {
//     let systemdate = new Date();
//     const authHeader = req.headers.authorization;
//     var dcodeInfo = null;
//     if (authHeader) {
//       TokenArray = authHeader.split(" ");
//       dcodeInfo = await jwttools.decryptdata(TokenArray[1]);
//       const { organization, apps } = req.body;
//       console.log(
//         organization.name,
//         organization.description,
//         Date.now(),
//         organization.id
//       );
//       // console.log(">>>> APPLICATION INSERT "+JSON.stringify(apps));

//       let orgResult = await pool.query(
//         "UPDATE public.mst_organizations SET  orgname = $1, orgdescription = $2, updated_at = to_timestamp($3 / 1000.0) WHERE id = $4 Returning *;",
//         [
//           organization.name,
//           organization.description,
//           Date.now(),
//           organization.id,
//         ]
//       );
//       if (orgResult.rows.length > 0) {
//         await pool.query(
//           "DELETE FROM public.cdid_orgapplications where idorg = $1 ",
//           [organization.id]
//         );
//         var appsToInsert = [];
//         console.log("cek panjang apps: " + apps.length);
//         if (apps.length > 0) {
//           await apps.map((app) => {
//             console.log(app);
//             let appToInsert = [];
//             appToInsert.push(organization.id);
//             appToInsert.push(app.id_application);
//             appToInsert.push(dcodeInfo.idtenant);
//             appToInsert.push(0);
//             appsToInsert.push(appToInsert);
//           });
//           let updateAppQuery = format(
//             "INSERT INTO public.cdid_orgapplications (idorg, idapp, idtenant, idsubtenant) VALUES %L returning *",
//             appsToInsert
//           );
//           let updateApp = await pool.query(updateAppQuery);
//           if (updateApp.rows.length > 0) {
//             res.status(200).json({ status: 200, data: updateApp.rows[0] });
//           } else {
//             res.status(500).json({
//               status: 500,
//               data: "No Application on the organizations",
//             });
//           }
//         }
//       }
//     } else {
//       res.status(500).json({ status: 500, data: "Not authorized" });
//     }
//   } catch (err) {
//     console.log(err);
//     res.status(500).json({ status: 500, data: "Error insert organization" });
//   }
// });