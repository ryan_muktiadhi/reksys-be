const express = require("express"),
  app = express();
const router = express.Router();
/*
const jwttools = require("./utils/encryptdecryptjwt");
const groupUtils = require("./utils/groupUtils");
const pool = require("../../connection/db");
const knex = require("../../connection/dborm");
var conf = require("../../config.json");
// const conn
const conn = knex.conn();
*/

const moment = require("moment");
const knex = require("../../connection/dborm");
const checkAuth = require("../../middleware/check-auth");
router.use(checkAuth);

var conf = require("../../config.json");
const conn = knex.conn();

//getAllGroup
router.get("/getAllMonTrx", async (req, res, next) => {
  var dcodeInfo = req.userData;
  try {
    /* NEW ADD: GET DATA CURRENT DATE (Today) FROM KOMICORE */
    const usernamePasswordBuffer = Buffer.from(
      "mii_hafidz" + ":" + "metrodata.1"
    );
    const base64data = usernamePasswordBuffer.toString("base64");
    const headers = {
      Authorization: "Basic " + base64data,
    };

    var url =
      "http://192.168.226.221:5555/invoke/bjb.bifast.provider.services:getTransactionByCurrentDate";

    const resCore = await axios.get(url, {
      headers,
    });

    // console.log(resCore);

    if (
      resCore.data.getTransactionByCurrentDateResponse.esbBody.results.length >
      0
    ) {
      setTimeout(function () {
        res.status(200).json({
          status: 200,
          data: resCore.data.getTransactionByCurrentDateResponse.esbBody,
        });
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({
          status: 202,
          data: resCore.data.getTransactionByCurrentDateResponse.esbBody,
        });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({
      status: 500,
      data: "internal error",
    });
  }
});

router.post("/getbyparam", async (req, res, next) => {
  const today = moment().format("YYYY-MM-DD HH:mm:ss");
  try {
    const { start_date, end_date, firstRow, maxRow, filters, fieldOrders } =
      req.body;

    const usernamePasswordBuffer = Buffer.from(
      "mii_hafidz" + ":" + "metrodata.1"
    );
    const base64data = usernamePasswordBuffer.toString("base64");
    const headers = {
      Authorization: "Basic " + base64data,
    };

    var url =
      "http://192.168.226.221:5555/invoke/bjb.bifast.provider.services:getTransactionByDateInterval";

    const resCore = await axios.post(
      url,
      {
        getTransactionByDateIntervalRequest: {
          esbHeader: {
            externalId: " ",
            timestamp: today,
          },
          esbBody: {
            trxDate1: start_date,
            trxDate2: end_date,
            minRow: firstRow.toString(),
            maxRow: maxRow.toString(),
            filters: filters,
            fieldOrders: fieldOrders,
          },
        },
      },
      {
        headers,
      }
    );

    // console.log(JSON.stringify(resCore.data.getTransactionByDateIntervalResponse.esbBody));

    if (
      resCore.data.getTransactionByDateIntervalResponse.esbBody.results.length >
      0
    ) {
      setTimeout(function () {
        res.status(200).json({
          status: 200,
          data: resCore.data.getTransactionByDateIntervalResponse.esbBody,
        });
      }, 1500);
    } else {
      setTimeout(function () {
        res.status(200).json({
          status: 202,
          data: resCore.data.getTransactionByDateIntervalResponse.esbBody,
        });
      }, 1500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({
      status: 500,
      data: "Internal Server Error",
    });
  }
});

router.post("/gettranshkbyparam", async (req, res, next) => {
  const today = moment().format("YYYY-MM-DD HH:mm:ss");
  try {
    const { start_date, end_date, firstRow, maxRow, filters, fieldOrders } =
      req.body;

    const usernamePasswordBuffer = Buffer.from(
      "mii_hafidz" + ":" + "metrodata.1"
    );
    const base64data = usernamePasswordBuffer.toString("base64");
    const headers = {
      Authorization: "Basic " + base64data,
    };

    var url =
      "http://192.168.226.221:5555/invoke/bjb.bifast.provider.services:getTransactionHkByDateInterval";

    const resCore = await axios.post(
      url,
      {
        getTransactionHkByDateIntervalRequest: {
          esbHeader: {
            externalId: " ",
            timestamp: today,
          },
          esbBody: {
            trxDate1: start_date,
            trxDate2: end_date,
            minRow: firstRow.toString(),
            maxRow: maxRow.toString(),
            filters: filters,
            fieldOrders: fieldOrders,
          },
        },
      },
      {
        headers,
      }
    );

    // console.log(JSON.stringify(resCore.data.getTransactionByDateIntervalResponse.esbBody));

    if (
      resCore.data.getTransactionHkByDateIntervalResponse.esbBody.results
        .length > 0
    ) {
      setTimeout(function () {
        res.status(200).json({
          status: 200,
          data: resCore.data.getTransactionHkByDateIntervalResponse.esbBody,
        });
      }, 1500);
    } else {
      setTimeout(function () {
        res.status(200).json({
          status: 202,
          data: resCore.data.getTransactionHkByDateIntervalResponse.esbBody,
        });
      }, 1500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({
      status: 500,
      data: "Internal Server Error",
    });
  }
});

module.exports = router;
