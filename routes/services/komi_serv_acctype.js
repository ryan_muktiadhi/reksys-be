const express = require("express"),
  app = express();
const router = express.Router();
const checkAuth = require("../../middleware/check-auth");
router.use(checkAuth);
const knex = require("../../connection/dborm");
var conf = require("../../config.json");
const { body, validationResult } = require("express-validator");
const conn = knex.conn();
//getAllGroup
router.get("/getAll", async (req, res, next) => {
  var dcodeInfo = req.userData;
  var idtenant = dcodeInfo.idtenant;

  try {
    const accTypeData = await conn
      .select(
        "id",
        "bank_account_type",
        "bi_account_type",
        "description",
        "status",
        conn
          .raw(`TO_CHAR("created_date", 'dd MON yyyy')`)
          .wrap("(", ") created_date"),
        "created_by",
        conn
          .raw(`TO_CHAR("updated_date", 'dd MON yyyy')`)
          .wrap("(", ") updated_date"),
        "updated_by"
      )
      .from("m_accounttype");

    if (accTypeData.length > 0) {
      setTimeout(function () {
        res.status(200).json({
          status: 200,
          data: accTypeData,
        });
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({
          status: 202,
          data: "Data not found",
        });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({
      status: 500,
      data: "Internal Server Error",
    });
  }
});

router.post(
  "/insert",
  body("bankAccountType", "Bank Account Type maximum 5 character").isLength({
    max: 5,
  }),
  body("biAccountType", "BI Account Type maximum 5 character").isLength({
    max: 5,
  }),
  async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(400).json({
        success: false,
        errors: errors.array(),
      });
    }

    var dcodeInfo = req.userData;
    const userId = dcodeInfo.id;
    var idtenant = dcodeInfo.idtenant;

    try {
      const { bankAccountType, biAccountType, description, status } = req.body;

      // Check Already exsist
      const accTypeExsist = await conn
        .select("*")
        .from("m_accounttype")
        .where("bank_account_type", bankAccountType)
        .andWhere("bi_account_type", biAccountType);

      if (accTypeExsist.length > 0) {
        res.status(200).json({
          status: 409,
          data: "Data Already Exist",
        });
      } else {
        const resp = await conn("m_accounttype")
          .insert({
            bank_account_type: bankAccountType,
            bi_account_type: biAccountType,
            description: description,
            status: status,
            created_date: conn.fn.now(),
            created_by: userId,
          })
          .returning(["id"]);

        if (resp.length > 0) {
          console.log("Success Insert Account Type data");
          res.status(200).json({
            status: 200,
            data: "Success insert data",
          });
        } else {
          console.log("Failed Insert Account Type data");
          res.status(200).json({
            status: 500,
            data: "Failed insert data",
          });
        }
      }
    } catch (err) {
      console.log("Failed insert Account Type data");
      console.log(err);
      res.status(500).json({
        status: 500,
        data: "Internal Server Error",
      });
    }
  }
);

router.post(
  "/update",
  body("bankAccountType", "Bank Account Type maximum 5 character").isLength({
    max: 5,
  }),
  body("biAccountType", "BI Account Type maximum 5 character").isLength({
    max: 5,
  }),
  async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(400).json({
        success: false,
        errors: errors.array(),
      });
    }
    var dcodeInfo = req.userData;
    const userId = dcodeInfo.id;

    try {
      const { id, bankAccountType, biAccountType, description, status } =
        req.body;

      // Check eksist data
      const accTypeExsist = await conn
        .select("*")
        .from("m_accounttype")
        .where("bank_account_type", bankAccountType)
        .andWhere("bi_account_type", biAccountType)
        .andWhere("description", description)
        .andWhere("status", status);

      if (accTypeExsist.length > 0) {
        console.log("Failed update Account Type data");
        res.status(200).json({
          status: 204,
          data: "Data already updated",
        });
      } else {
        const resp = await conn("m_accounttype").where("id", id).update({
          bank_account_type: bankAccountType,
          bi_account_type: biAccountType,
          description: description,
          status: status,
          updated_date: conn.fn.now(),
          updated_by: userId,
        });

        if (resp > 0) {
          console.log("Success Update Account Type data");
          res.status(200).json({
            status: 200,
            data: resp,
          });
        } else {
          console.log("Failed update Account Type data");
          res.status(500).json({
            status: 500,
            data: "Failed update data",
          });
        }
      }
    } catch (err) {
      console.log(err);
      res.status(500).json({
        status: 500,
        data: "Internal server error",
      });
    }
  }
);

router.get("/delete/:id", async (req, res, next) => {
  var dcodeInfo = req.userData;
  let id = req.params.id;

  try {
    let resp = await conn("m_accounttype").where("id", id).del();
    res.status(200).json({
      status: 200,
      data: "Success delete data",
    });
  } catch (err) {
    console.log("Failed delete Account Type data");
    console.log(err);
    console.log(err);
    res.status(500).json({
      status: 500,
      data: "Error delete data",
    });
  }
});

router.get("/getById/:id", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;

  try {
    let id = req.params.id;

    const accTypeData = await conn
      .select(
        "id",
        "bank_account_type",
        "bi_account_type",
        "description",
        "status",
        "created_date",
        "created_by",
        "updated_date",
        "updated_by"
      )
      .where("id", id)
      .from("m_accounttype");

    if (accTypeData.length > 0) {
      console.log("Success get Account Type data by id");
      res.status(200).json({
        status: 200,
        data: {
          accType: accTypeData[0],
        },
      });
    } else {
      setTimeout(function () {
        console.log("No  Account Type by id data");
        res.status(200).json({
          status: 202,
          data: "Data tidak ditemukan",
        });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({
      status: 500,
      data: "internal error",
    });
  }
});
module.exports = router;
