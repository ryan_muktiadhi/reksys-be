const express = require("express"),app = express();
const router = express.Router();
const pool = require("../../connection/db");
const jwttools = require("./utils/encryptdecryptjwt");
const bcrypt = require("bcrypt");
const logger = require("./utils/logservices");
const moment = require("moment");

/*POST END*/


router.get("/:id", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = null;
  if(authHeader){
    TokenArray = authHeader.split(" ");
    dcodeInfo = await jwttools.decryptdata(TokenArray[1]);

    var apps = dcodeInfo.apps[0];

    try {
      const resp = await pool.query("SELECT fullname, userid, tnname, leveltenant FROM cdid_tenant_user ctu JOIN cdid_tenant t ON ctu.idtenant = t.id WHERE ctu.id = $1",[req.params.id]);
      res.status(200).json({ status: 200, data: resp.rows[0] });
    } catch (err) {
      res.status(500).json({ status: 500, data: "No data Profile found" });
    }
  }
  else{
    res.status(500).json({ status: 500, data: "Not authorized" });
  }
});

router.put("/changepassword", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = null;
  if(authHeader){
    TokenArray = authHeader.split(" ");
    dcodeInfo = await jwttools.decryptdata(TokenArray[1]);

    var apps = dcodeInfo.apps[0];
    
    try {
      const { id, oldpassword, newpassword } = req.body;
      
      const check = await pool.query("SELECT id FROM cdid_tenant_user WHERE id = $1 AND pwd=crypt($2, pwd)",[id,oldpassword]);
      if(check.rows.length > 0) {
        const resp = await pool.query("UPDATE cdid_tenant_user SET pwd = crypt($1,gen_salt('bf',4)) WHERE id = $2",[newpassword, id]);
        
        res.status(200).json({ status: 200, data: resp.rows });
      }
      else{
        res.status(500).json({ status: 500, data: "Wrong Password" });  
      }
    } catch (err) {
      res.status(500).json({ status: 500, data: "No data found" });
    }
  }
  else{
    res.status(500).json({ status: 500, data: "Not authorized" });
  }
});

module.exports = router;
