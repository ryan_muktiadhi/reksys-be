const express = require("express"),
  app = express();
const router = express.Router();
const jwttools = require("../../routes/services/utils/encryptdecryptjwt");
const groupUtils = require("./utils/groupUtils");
const pool = require("../../connection/db");
const checkAuth = require("../../middleware/check-auth");
const moment = require("moment");
// const axios = require('axios')
router.use(checkAuth);
const knex = require("../../connection/dborm");
var conf = require("../../config.json");
const { body, validationResult } = require("express-validator");
// const conn
const conn = knex.conn();

//getAllGroup
router.get("/getAll", async (req, res, next) => {
  var dcodeInfo = req.userData;

  try {
    const idTypeData = await conn
      .select("*")
      .from("m_customer_id_type")
      .orderBy("created_date", "DESC");
    if (idTypeData.length > 0) {
      setTimeout(function () {
        res.status(200).json({ status: 200, data: idTypeData });
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({ status: 202, data: "Data tidak ditemukan" });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.get("/getByStatusActive", async (req, res, next) => {
  var dcodeInfo = req.userData;

  try {
    const idTypeData = await conn
      .select("*")
      .from("m_customer_id_type")
      .where("status", "1")
      .orderBy("created_date", "ASC");

    if (idTypeData.length > 0) {
      setTimeout(function () {
        res.status(200).json({ status: 200, data: idTypeData });
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({ status: 202, data: "Data tidak ditemukan" });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.get("/getSecType", async (req, res, next) => {
  var dcodeInfo = req.userData;

  try {
    const idTypeData = [
      {
        ID_TYPE: "1",
        DESCRIPTION: "KTP",
      },
      {
        ID_TYPE: "2",
        DESCRIPTION: "SIM",
      },
      {
        ID_TYPE: "3",
        DESCRIPTION: "PASSPORT",
      },
      {
        ID_TYPE: "4",
        DESCRIPTION: "NIK ANAK",
      },
      {
        ID_TYPE: "5",
        DESCRIPTION: "PASSPORT & SURAT REFFREDENSI",
      },
      {
        ID_TYPE: "6",
        DESCRIPTION: "KITAS/KITAP",
      },
      {
        ID_TYPE: "T",
        DESCRIPTION: "TDP",
      },
      {
        ID_TYPE: "N",
        DESCRIPTION: "NPWP",
      },
    ];

    if (idTypeData.length > 0) {
      setTimeout(function () {
        res.status(200).json({ status: 200, data: idTypeData });
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({ status: 202, data: "Data tidak ditemukan" });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.get("/getById/:id", async (req, res, next) => {
  var dcodeInfo = req.userData;

  try {
    let id = req.params.id;

    const idTypeData = await conn
      .select("*")
      .from("m_customer_id_type")
      .where("id", id);

    if (idTypeData.length > 0) {
      setTimeout(function () {
        res.status(200).json({ status: 200, data: idTypeData[0] });
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({ status: 202, data: "Data tidak ditemukan" });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Internal Server Error" });
  }
});

router.post(
  "/insert",
  body("bankIdType", "Bank Code  maximum 2 character").isLength({
    max: 2,
  }),
  body("biIdType", "BIC Bank maximum 2 character").isLength({
    max: 2,
  }),

  async (req, res, next) => {
    if (!errors.isEmpty()) {
      return res.status(400).json({
        success: false,
        errors: errors.array(),
      });
    }
    var dcodeInfo = req.userData;
    const userId = dcodeInfo.id;

    try {
      const { bankIdType, biIdType, description, status } = req.body;

      // Check Already exsist
      const custIdType = await conn
        .select("bi_id_type")
        .from("m_customer_id_type")
        .where("bank_id_type", bankIdType)
        .andWhere("bi_id_type", biIdType);

      if (custIdType.length > 0) {
        return res
          .status(200)
          .json({ status: 409, data: "Data Already Exist" });
      }

      const resp = await conn("m_customer_id_type").insert({
        bank_id_type: bankIdType,
        bi_id_type: biIdType,
        description: description,
        status: status,
        created_date: conn.fn.now(),
        created_by: userId,
      });
      return res.status(200).json({ status: 200, data: "Success insert data" });
    } catch (err) {
      console.log(err);
      res.status(500).json({ status: 500, data: "Internal Server Error" });
    }
  }
);

router.post(
  "/update",
  body("bankIdType", "Id Type  maximum 2 character").isLength({
    max: 2,
  }),
  body("biIdType", "BI Id Type  maximum 2 character").isLength({
    max: 2,
  }),

  async (req, res, next) => {
    if (!errors.isEmpty()) {
      return res.status(400).json({
        success: false,
        errors: errors.array(),
      });
    }
    var dcodeInfo = req.userData;
    const userId = dcodeInfo.id;
    try {
      const { id, bankIdType, biIdType, description, status } = req.body;

      const resp = await conn("m_customer_id_type").where("id", id).update({
        bank_id_type: bankIdType,
        bi_id_type: biIdType,
        description: description,
        status: status,
        updated_date: conn.fn.now(),
        updated_by: userId,
      });
      res.status(200).json({ status: 200, data: resp });
    } catch (err) {
      console.log(err);
      res.status(500).json({ status: 500, data: "Error insert User" });
    }
  }
);
router.get("/delete/:id", async (req, res, next) => {
  var dcodeInfo = req.userData;
  let id = req.params.id;
  try {
    let resp = await conn("m_customer_id_type").where("id", id).del();
    console.log("Success delete Customer Id Type data");
    res.status(200).json({ status: 200, data: "Success" });
  } catch (err) {
    console.log("Failed delete Customer Id Type data");
    console.log(err);
    res.status(500).json({ status: 500, data: "Error retrive Users" });
  }
});

module.exports = router;
