const express = require("express");
const router = express.Router();
const logs = require("./utils/logger");
// const eventLog = require("./utils/eventLog");
const pool = require("../../connection/db");
const checkAuth = require("../../middleware/check-auth");
var jsonxml = require("jsontoxml");
var conf = require("../../config.json");
const reconServer = conf.reconServer;
var fileupload = require("express-fileupload");
const _ = require("lodash");

const fxp = require("fast-xml-parser");

const axios = require("axios");
const { generateCompareXML, generateReportXML } = require("./utils/rekonUtil");

router.use(checkAuth);

router.post("/deleteJob/:id", async (req, res, next) => {
  try {
    let postJob = await axios.post(
      reconServer + `/sentinel-recon/management/job/delete/${req.params.id}`
    );
    console.log(postJob);
    if (postJob.status == 200) {
      return res.status(200).json({ status: 200, data: "Post sucess" });
    } else {
      return res.status(200).json({ status: 200, data: postJob.data });
    }
  } catch (err) {
    console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.post("/postJob", async (req, res, next) => {
  try {
    let {
      jobName,
      jobDesc,
      source1,
      source2,
      matchingField1,
      matchingField2,
      reportField1,
      reportField2,
      autoGenerateReport,
      cronsc1,
      cronsc2,
      execSc,
      filePathSc1,
      filePathSc2,
      dataPopulateSc1,
      dataPopulateSc2,
      execScType
    } = req.body;

    let comparatorParams = await generateCompareXML(
      matchingField1,
      matchingField2
    );
    let reportParams = await generateReportXML(reportField1, reportField2);

    const payload = {
      billedEntity: source1,
      referenceEntity: source2,
      comparatorBean: "nativeQueryComparator",
      jobName: jobName,
      jobDesc: jobDesc,
      maxAge: 3,
      autoGenerateReport,
      cron: execSc,
      comparatorParams,
      reportParams,
      billedPopulationFolder: filePathSc1,
      billedPopulationCron: cronsc1,
      referencePopulationFolder: filePathSc2,
      referencePopulationCron: cronsc2,
      dataPopulateSc1:dataPopulateSc1,
      dataPopulateSc2:dataPopulateSc2,
      execScType:execScType
    };
    // console.log(JSON.stringify(payload));
    // res.status(200).json({ status: 200, data: "Post sucess" });

    let postJob = await axios.post(
      reconServer + "/sentinel-recon/management/job/add",
      payload
    );
    console.log(postJob);
    if (postJob.status == 200) {
      return res.status(200).json({ status: 200, data: "Post sucess" });
    } else {
      return res.status(200).json({ status: 200, data: postJob.data });
    }
  } catch (err) {
    console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.post("/editJob", async (req, res, next) => {
  try {
    let {
      idJob,
      jobName,
      jobDesc,
      source1,
      source2,
      matchingField1,
      matchingField2,
      reportField1,
      reportField2,
      autoGenerateReport,
      cronsc1,
      cronsc2,
      execSc,
      filePathSc1,
      filePathSc2,
      dataPopulateSc1,
      dataPopulateSc2,
      execScType
    } = req.body;
    let comparatorParams = await generateCompareXML(
      matchingField1,
      matchingField2
    );
    let reportParams = await generateReportXML(reportField1, reportField2);
    const payload = {
      id: idJob,
      billedEntity: source1,
      referenceEntity: source2,
      comparatorBean: "matchingKeyComparator",
      jobName: jobName,
      jobDesc: jobDesc,
      maxAge: 3,
      cron: "",
      comparatorParams,
      autoGenerateReport,
      reportParams,
      billedPopulationFolder: filePathSc1,
      billedPopulationCron: cronsc1,
      referencePopulationFolder: filePathSc2,
      referencePopulationCron: cronsc2,
      dataPopulateSc1:dataPopulateSc1,
      dataPopulateSc2:dataPopulateSc2,
      execScType:execScType
    };
    // console.log(JSON.stringify(payload));
    // res.status(200).json({ status: 200, data: "Post sucess" });

    let postJob = await axios.post(
      reconServer + "/sentinel-recon/management/job/edit",
      payload
    );
    console.log(postJob);
    if (postJob.status == 200) {
      return res.status(200).json({ status: 200, data: "Edit sucess" });
    } else {
      return res.status(200).json({ status: 200, data: postJob.data });
    }
  } catch (err) {
    console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.get("/getEntity", async (req, res, next) => {
  try {
    let entityList;
    let getEntity = await axios.get(
      reconServer + "/sentinel-recon/management/entity/list"
    );
    // let dummy = ["cbs", "bifast"];
    if (getEntity.status == 200) {
      entityList = getEntity.data;
    }
    res.status(200).json({ status: 200, data: entityList });
  } catch (err) {
    console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.get("/getEntityProps/:entity1", async (req, res, next) => {
  try {
    let enitityList;
    let getEntity = await axios.get(
      reconServer +
        `/sentinel-recon/management/entity/property/${req.params.entity1}`
    );

    if (getEntity.status == 200) {
      enitityList = getEntity.data;
    }
    res.status(200).json({ status: 200, data: enitityList });
  } catch (err) {
    console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.get("/getEntityProps/:entity1/:entity2", async (req, res, next) => {
  try {
    let enitityList;
    let getEntity = await axios.get(
      reconServer +
        `/sentinel-recon/management/entity/property/${req.params.entity1}/${req.params.entity2}`
    );

    if (getEntity.status == 200) {
      enitityList = getEntity.data;
    }
    res.status(200).json({ status: 200, data: enitityList });
  } catch (err) {
    console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.get("/getJobs", async (req, res, next) => {
  try {
    var dcodeInfo = req.userData;

    let enitityList;
    let getEntity = await axios.get(
      reconServer + `/sentinel-recon/management/job/list`
    );

    if (getEntity.status == 200) {
      await getEntity.data.sort((a, b) => (a.id < b.id ? 1 : -1));
      enitityList = getEntity.data;
    }
    res.status(200).json({ status: 200, data: enitityList });
  } catch (err) {
    console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.get("/getJobs/:id", async (req, res, next) => {
  try {
    let entityList;
    let getEntity = await axios.get(
      reconServer + `/sentinel-recon/management/job/find/${req.params.id}`
    );

    if (getEntity.status == 200) {
      entityList = getEntity.data;
      const parser = new fxp.XMLParser();
      let matchData = parser.parse(entityList.comparatorParams);
      let listMatch = matchData["linked-hash-map"].entry;
      let match1 = [];
      let match2 = [];
      if (listMatch.length > 0) {
        let listMatch1 =
          matchData["linked-hash-map"].entry[0]["linked-hash-map"];

        if (listMatch1.entry.length > 0) {
          await listMatch1.entry.map((data1) => {
            match1.push(data1.string[1]);
          });
        } else {
          match1.push(listMatch1.entry.string[1]);
        }
        let listMatch2 =
          matchData["linked-hash-map"].entry[1]["linked-hash-map"];
        if (listMatch1.entry.length > 0) {
          await listMatch2.entry.map((data2) => {
            match2.push(data2.string[1]);
          });
        } else {
          match2.push(listMatch2.entry.string[1]);
        }
      }

      let reportData = parser.parse(entityList.reportParams);
      let reportList = reportData["linked-hash-map"].entry;
      let report1 = [];
      let report2 = [];
      if (reportList.length > 0) {
        console.log(reportData["linked-hash-map"].entry);
        report1 = reportData["linked-hash-map"].entry[2].list.string;
        report2 = reportData["linked-hash-map"].entry[0].list.string;
      }
      entityList.matchField1 = match1;
      entityList.matchingField2 = match2;
      entityList.reportField1 = report1;
      entityList.reportField2 = report2;
    }
    res.status(200).json({ status: 200, data: entityList });
  } catch (err) {
    console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.get("/triggerJob/:jobId/:file1Id/:file2Id", async (req, res, next) => {
  try {
    let enitityList;
    let getEntity = await axios.get(
      reconServer +
        `/sentinel-recon/matching/${req.params.jobId}/${req.params.file1Id}/${req.params.file2Id}`
    );
    console.log(getEntity);
    if (getEntity.status == 200) {
      enitityList = getEntity.data;
    }
    res.status(200).json({ status: 200, data: enitityList });
  } catch (err) {
    console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.get("/getUploadHistory/:id", async (req, res, next) => {
  try {
    let enitityList;
    let getEntity = await axios.get(
      reconServer + `/sentinel-recon/fileupload/history/list`
    );
    console.log(getEntity);
    if (getEntity.status == 200) {
      if (getEntity.data.length > 0) {
        await getEntity.data.map((dt) => {
          dt.datetimeEndProcess = new Date(dt.dateTimeFinish).toUTCString();
          dt.datetimeStartProcess = new Date(dt.dateTimeCreation).toUTCString();
          let fileName = dt.path.split("/");
          dt.fileName = fileName[4];
          dt.status = dt.completed == true ? "Completed" : "In Completed";
        });
        getEntity.data = await await getEntity.data.filter(
          (dt) => (dt.reconJobId = req.params.id)
        );
      }
      enitityList = await getEntity.data;
    }
    res.status(200).json({ status: 200, data: enitityList });
  } catch (err) {
    console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.get("/getReconSessions", async (req, res, next) => {
  try {
    let enitityList;
    let getEntity = await axios.get(
      reconServer + `/sentinel-recon/management/session/list`
    );
    
    if (getEntity.status == 200) {
      if (getEntity.data.length > 0) {
        await getEntity.data.map(async (dt) => {
          dt.datetimeFinish = dt.datetimeFinish
            ? new Date(dt.datetimeFinish).toUTCString()
            : dt.datetimeFinish;
          dt.datetimeCreation = dt.datetimeCreation
            ? new Date(dt.datetimeCreation).toUTCString()
            : dt.datetimeCreation;
          let source1Total = JSON.parse(dt.billedReconCountRecords);
          dt.billedMatchTotal = source1Total?.Match;
          dt.billedUnMatchTotal = source1Total?.Not_Match;
          dt.billedPendingTotal = source1Total?.Pending;
          let source2Total = JSON.parse(dt.referenceReconCountRecords);
          dt.refMatchTotal = source2Total?.Match;
          dt.refUnMatchTotal = source2Total?.Not_Match;
          dt.refPendingTotal = source2Total?.Pending;
        });
      }

      enitityList = getEntity.data;
    }
    res.status(200).json({ status: 200, data: enitityList });
  } catch (err) {
    console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.get("/getReconSessionsById/:id", async (req, res, next) => {
  try {
    let enitityList;

    let getEntity = await axios.get(
      reconServer +
        // `/sentinel-recon/management/session/list/detail/${req.params.id}`
        `/sentinel-recon/management/session/recon-data/${req.params.id}`
    );

    if (getEntity.status == 200) {
      console.log(getEntity.data);
      let resultReq = await JSON.parse(getEntity.data);
      let entities = [resultReq.entity1, resultReq.entity2];
      enitityList = entities;
    }
    res.status(200).json({ status: 200, data: enitityList });
  } catch (err) {
    console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});
// http://localhost:3101/sentinel-recon/report/generate/:sessionId
router.post("/getGenerateReportRekon", async (req, res, next) => {
  try {
    let {
      sessionId
    } = req.body;
    let enitityList;
    let getEntity = await axios.get(
      reconServer + `/sentinel-recon/report/generate/${sessionId}`
    );

    if (getEntity.status == 200) {
      res.status(200).json({ status: 200, data: enitityList });
      // if (getEntity.data.length > 0) {
      //   await getEntity.data.map(async (dt) => {
      //     dt.datetimeFinish = dt.datetimeFinish
      //       ? new Date(dt.datetimeFinish).toUTCString()
      //       : dt.datetimeFinish;
      //     dt.datetimeCreation = dt.datetimeCreation
      //       ? new Date(dt.datetimeCreation).toUTCString()
      //       : dt.datetimeCreation;
      //     let source1Total = JSON.parse(dt.billedReconCountRecords);
      //     dt.billedMatchTotal = source1Total?.Match;
      //     dt.billedUnMatchTotal = source1Total?.Not_Match;
      //     dt.billedPendingTotal = source1Total?.Pending;
      //     let source2Total = JSON.parse(dt.referenceReconCountRecords);
      //     dt.refMatchTotal = source2Total?.Match;
      //     dt.refUnMatchTotal = source2Total?.Not_Match;
      //     dt.refPendingTotal = source2Total?.Pending;
      //   });
      // }
      // enitityList = getEntity.data;
    }
    
  } catch (err) {
    console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.get(
  "/getSessionDetail/:id/:sourcename/:stat",
  async (req, res, next) => {
    try {
      let result = {};
      //http://localhost:3101/sentinel-recon/management/session/recon-data/:sessionId/:entityName/:status
      let url =
        req.params.stat.toUpperCase() == "ALL"
          ? reconServer +
            `/sentinel-recon/management/sessiondetail/all/${req.params.id}/${req.params.sourcename}`
          : reconServer +
            `/sentinel-recon/management/session/recon-data/${req.params.id}/${req.params.sourcename}/${req.params.stat}`;

      console.log(">>>>>>>>>>>>>>>>>>>>> URL INVOKW ", url);
      let getEntity = await axios.get(url);

      if (req.params.stat.toUpperCase() != "ALL") {
        if (getEntity.status == 200) {
          if (getEntity.data) {
            getEntity.data.data = JSON.parse(getEntity.data);
          }
          result = getEntity.data;
          console.log(result);
        }
      } else {
        let resultData = [];
        if (getEntity.status == 200) {
          if (getEntity.data.length > 0) {
            await getEntity.data.map((dt) => {
              let dataArry = JSON.parse(dt.data);

              resultData = _.concat(resultData, dataArry);
            });
          }
          result.data = resultData;
        }
      }
      res.status(200).json({ status: 200, data: result });
    } catch (err) {
      console.log(err);
      next(err);
      res.status(500).json({ status: 500, data: "internal error" });
    }
  }
);

router.get("/downloadReport/:id", async (req, res, next) => {
  try {
    let enitityList;
    let getEntity = await axios.get(
      reconServer + `/sentinel-recon/report/download-matching/${req.params.id}`,
      { responseType: "arraybuffer" }
    );

    if (getEntity.status == 200) {
      enitityList = getEntity.data;
    }

    res.end(Buffer.from(getEntity.data, "binary"));
  } catch (err) {
    console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});



router.use(fileupload());

router.post("/uploadfile/:entityName/:reconJobId", async (req, res, next) => {
  try {
    console.log(req.files.file);
    const fileName = req.files.file.name.split(".");
    const fileOriName = fileName[0];
    const fileExt = fileName[1];
    const entityName = req.params.entityName;
    const jobId = req.params.reconJobId;

    var config = {
      method: "post",
      url:
        reconServer +
        `/sentinel-recon/upload/entity/${entityName}/${fileExt}/${fileOriName}/${jobId}`,
      headers: {
        "Content-Type": req.files.file.mimetype,
      },
      data: req.files.file.data,
    };

    let postFile = await axios(config);

    console.log(postFile);
    res.status(200).json({ status: 200, data: postFile.data });
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error Upload Jobs File" });
  }
});
// http://182.169.41.153:3101/sentinel-recon/management/entity/template-upload/{headerRowNumber}


router.post("/uploadfileTemplate/:headerRowNumber", async (req, res, next) => {
  try {
    console.log(req.files.file);
    const fileName = req.files.file.name.split(".");
    // const fileOriName = fileName[0];
    // const fileExt = fileName[1];
    const headerRowNumber = req.params.headerRowNumber;
    // const jobId = req.params.reconJobId;

    var config = {
      method: "post",
      url:
        reconServer +
        `/sentinel-recon/management/entity/template-upload/${headerRowNumber}`,
      headers: {
        "Content-Type": req.files.file.mimetype,
      },
      data: req.files.file.data,
    };

    let postFile = await axios(config);

    console.log(postFile);
    res.status(200).json({ status: 200, data: postFile.data });
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error Upload Template File" });
  }
});




router.post("/addEntityRekon", async (req, res, next) => {
  try {
    let {
      formatName,
      formatDesc,
      isTemplate,
      startRow,
      fieldSpt,
      templateData,
      availableMatchingKey
    } = req.body;

    const payload = {
      sourceName: formatName,
      availableMatchingKey: "",
      active: true,
      sourceDescription: formatDesc,
      isTemplate: true,
      driverClass: "",
      defVersion:"1",
      startRowNumber: startRow,
      separator:fieldSpt,
      templateData:templateData,
      availableMatchingKey:availableMatchingKey
    };
    console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> TERIMA ", req.body);
    // console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> PAYLOAD KIRIM ", payload);
    let postJob = await axios.post(
      reconServer + "/sentinel-recon/management/entity/add",
      payload
    );
    console.log(postJob);

    if (postJob.status == 200) {
      res.status(200).json({ status: 200, data: postJob.data });
    }
    
  } catch (err) {
    console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});
router.get("/getEntityFormat", async (req, res, next) => {
  console.log("##################################################### KAMPREEEEEEEEEEEEEEEEEEEEEEST");
  try {
    let entityList;
    let getEntity = await axios.get(
      reconServer + "/sentinel-recon/management/entity/"
    );
    // let dummy = ["cbs", "bifast"];
    if (getEntity.status == 200) {
      entityList = getEntity.data;
    }
    res.status(200).json({ status: 200, data: entityList });
  } catch (err) {
    console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.get("/getScheduleRekon", async (req, res, next) => {
  try {
    let entityList;
    let coba=[];
    let getEntity = await axios.get(
      reconServer + "/sentinel-recon/management/scheduler/monitor"
    );
    // let dummy = ["cbs", "bifast"];
    console.log(">>>>> balikan data ", getEntity.data);
    if (getEntity.status == 200) {
     
      entityList = getEntity.data;
      // coba = JSON.parse(entityList);

      // console.log(">>>>> DATA MONITOR ", getEntity.data[1]);
    }
    res.status(200).json({ status: 200, data: entityList });
  } catch (err) {
    console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});


module.exports = router;
