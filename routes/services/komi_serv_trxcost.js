const express = require("express"),
  app = express();
const router = express.Router();
const jwttools = require("../../routes/services/utils/encryptdecryptjwt");
const groupUtils = require("./utils/groupUtils");
const pool = require("../../connection/db");
const checkAuth = require("../../middleware/check-auth");
const moment = require("moment");
const knex = require("../../connection/dborm");
router.use(checkAuth);

var conf = require("../../config.json");
const conn = knex.conn();

//getAllGroup
router.get("/getAlltrx", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  var apps = dcodeInfo.apps[0];
  try {
    console.log(
      ">>>>>>>>>>>> Tenant, Level " + JSON.stringify(dcodeInfo.leveltenant)
    );
    //   console.log(dcodeInfo.leveltenant);TO_CHAR(clis.expiredate,'yyyy-MM-dd')
    // var query =
    //   "SELECT id, fee, values, status FROM public.m_trxcost WHERE idtenant=$1 order by created_date desc";
    //const trxcosts = await pool.query(query, [dcodeInfo.idtenant]);
    const trxcosts = await conn
      .where("id_tenant", dcodeInfo.idtenant)
      .select("id", "fee", "values", "status")
      .from("m_trxcost");

    if (trxcosts.length > 0) {
      setTimeout(function () {
        res.status(200).json({ status: 200, data: { trxcosts: trxcosts } });
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({ status: 202, data: "Data tidak ditemukan" });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.get("/getTrx/:id", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  // var apps = dcodeInfo.apps[0];
  try {
    let trxid = req.params.id;
    // console.log(">>>>>>>>>>>> Tenanr, Level "+JSON.stringify(dcodeInfo.leveltenant));
    //   console.log(dcodeInfo.leveltenant);TO_CHAR(clis.expiredate,'yyyy-MM-dd')
    // var query =
    //   "SELECT id, fee, values, status FROM public.m_trxcost WHERE id=$1 order by created_date desc";
    // const trxcosts = await pool.query(query, [trxid]);
    const trxcosts = await conn
      .where("id", trxid)
      .select("id", "fee", "values", "status")
      .from("m_trxcost");

    if (trxcosts.length > 0) {
      setTimeout(function () {
        res.status(200).json({ status: 200, data: { trxcosts: trxcosts[0] } });
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({ status: 202, data: "Data tidak ditemukan" });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});
router.post("/insertTrx", async (req, res, next) => {
  var dcodeInfo = req.userData;
  try {
    var apps = dcodeInfo.apps[0];
    const { fee, values, status } = req.body;

    const resp = await conn("m_trxcost")
      .returning(["id", "fee", "values", "status", "change_who", "id_tenant"])
      .insert({
        fee: fee,
        values: values,
        status: status,
        change_who: dcodeInfo.id,
        id_tenant: dcodeInfo.idtenant,
      });

    if (resp.length > 0) {
      res.status(200).json({ status: 200, data: resp });
    } else {
      res.status(500).json({
        status: 500,
        data: "Error insert m_bic ",
      });
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error insert Data" });
  }
});

router.post("/updateTrx", async (req, res, next) => {
  var dcodeInfo = req.userData;
  try {
    var apps = dcodeInfo.apps[0];
    const { id, fee, values, status } = req.body;
    const today = moment().format("YYYY-MM-DD HH:mm:ss");
    // const resp = await pool.query(
    //   "UPDATE public.m_trxcost SET fee=$1, values=$2, status=$3, last_update_date=$4 WHERE id=$5",
    //   [fee, values, status, today, id]
    // );
    const resp = await conn("m_trxcost")
      .returning(["id", "fee", "values", "status", "last_update_date"])
      .where("id", id)
      .update({
        fee: fee,
        values: values,
        status: status,
        last_update_date: today,
      });

    if (resp.length > 0) {
      res.status(200).json({ status: 200, data: resp });
    } else {
      res.status(500).json({
        status: 500,
        data: "Error insert m_bic ",
      });
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error insert User" });
  }
});
router.get("/deleteTrx/:id", async (req, res, next) => {
  // const authHeader = req.headers.authorization;
  console.log("################## >>>> ##################################");
  var dcodeInfo = req.userData;
  var apps = dcodeInfo.apps[0];
  let trxid = req.params.id;
  console.log("### transaction cost id ### " + trxid);
  try {
    // let resp = await pool.query("DELETE FROM public.m_trxcost where id = $1", [
    //   trxid,
    // ]);
    const resp = await conn("m_trxcost").where("id", trxid).del();
    res.status(200).json({ status: 200, data: "Success" });
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error retrive Users" });
  }
});

module.exports = router;
