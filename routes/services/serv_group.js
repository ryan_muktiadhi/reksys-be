const express = require("express"),
  app = express();
const router = express.Router();
const jwttools = require("../../routes/services/utils/encryptdecryptjwt");
const groupUtils = require("./utils/groupUtils");
const pool = require("../../connection/db");
const checkAuth = require("../../middleware/check-auth");
router.use(checkAuth);

//getAllGroup
router.get("/getAllGroup", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
    // console.log(">>>>>>>>>>>> decoded "+JSON.stringify(dcodeInfo));
    var apps = dcodeInfo.apps[0];
    // console.log(">>>>>>>>>>>> decoded "+JSON.stringify(apps));
    try {
      // console.log(">>>>>>>>>>>> Tenanr, Level "+JSON.stringify(dcodeInfo.leveltenant));
      console.log(dcodeInfo.leveltenant);
      var query =
        "Select * from public.mst_usergroup where idapplication=$1 and idtenant=$2";
      // var idtype = dcodeInfo.leveltenant;

      // console.log(">>>>>>>>>>>> APPLICATION, Level "+(idtype != "0") );

      // if(idtype != "0" ) {
      // query =
      //   "Select * from public.mst_usergroup where idapplication=$1 and idtenant=$2 and grouptype=$3 or grouptype = $4";
      query =
        "Select * from public.mst_usergroup where idapplication=$1 and idtenant=$2 and grouptype >= $3";
      // };
      // console.log(">>>>>>>>>>>> Query, Level "+(dcodeInfo.leveltenant == "0") );
      // console.log(">>>>>>>>>>>> IDApp "+apps.id_application );
      // console.log(query);
      // console.log(apps.id, dcodeInfo.idtenant, 1, dcodeInfo.leveltenant);
      const userGroup = await pool.query(
        query,
        dcodeInfo.leveltenant == "0"
          ? [apps.id, dcodeInfo.idtenant, 0]
          : [apps.id, dcodeInfo.idtenant, dcodeInfo.leveltenant]
      );
      // const userGroup = await pool.query(
      //   "Select * from public.mst_usergroup",
      //   []
      // );
      if (userGroup.rows.length > 0) {
        setTimeout(function() {
          res.status(200).json({ status: 200, data: { userGroup: userGroup.rows } });
        }, 1000);
        
      } else {
        res.status(200).json({ status: 200, data: "Data tidak ditemukan" });
      }
    } catch (err) {
      console.log(err);
      res.status(500).json({ status: 500, data: "internal error" });
    }
 
});

//getGroupBy Type
router.get("/getExternalGroup", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = null;
  if (authHeader) {
    TokenArray = authHeader.split(" ");
    dcodeInfo = await jwttools.decryptdata(TokenArray[1]);
    var apps = dcodeInfo.apps[0];
    try {
      console.log(dcodeInfo.leveltenant);
      var query =
        "Select * from public.mst_usergroup where idapplication=$1 and idtenant=$2 and grouptype = 2";

      const userGroup = await pool.query(
        query,

        [apps.id, dcodeInfo.idtenant]
      );
      if (userGroup.rows.length > 0) {
        res
          .status(200)
          .json({ status: 200, data: { userGroup: userGroup.rows } });
      } else {
        res.status(200).json({ status: 200, data: "Data tidak ditemukan" });
      }
    } catch (err) {
      console.log(err);
      res.status(500).json({ status: 500, data: "internal error" });
    }
  } else {
    console.log("no header");
    res.sendStatus(403);
  }
});

//Get all Module and menu
router.get("/getallmodulewithmenu", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
    var apps = dcodeInfo.apps[0];
    try {
      const modules = await pool.query(
        "Select * from public.mst_moduleby_tenant ",
        []
      );
      if (modules.rows.length > 0) {
        let result = [];
        try {
          let modulesList = modules.rows;

          await Promise.all(
            modulesList.map(async (module) => {
              let moduleDetail = {};
              moduleDetail.moduleId = module.id;
              moduleDetail.moduleName = module.modulename;
              let menus = await pool.query(
                "select * from public.mst_menu where idmodule = $1",
                [module.id]
              );
              if (menus.rows.length > 0) {
                let menuRows = menus.rows;
                let menuList = [];
                let menuDetail = {};
                await Promise.all(
                  menuRows.map((menu) => {
                    menuDetail = {};
                    menuDetail.menuName = menu.title;
                    menuDetail.menuId = menu.id;
                    menuList.push(menuDetail);
                  })
                );

                moduleDetail.moduleMenus = menuList;
              }
              result.push(moduleDetail);
            })
          );

          res.status(200).json({ status: 200, data: result });
        } catch (error) {
          console.log(err);
        }
      } else {
        res.status(200).json({ status: 200, data: "Data tidak ditemukan" });
      }
    } catch (err) {
      console.log(err);
      res.status(500).json({ status: 500, data: "internal error" });
    }
  
});

//Get all Module
router.get("/getallmodules", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = null;
  if (authHeader) {
    TokenArray = authHeader.split(" ");
    dcodeInfo = await jwttools.decryptdata(TokenArray[1]);

    var apps = dcodeInfo.apps[0];
    try {
      const modules = await pool.query(
        "Select * from public.mst_moduleby_tenant ",
        []
      );
      if (modules.rows.length > 0) {
        let result = [];
        result = modules.rows;
        res.status(200).json({ status: 200, data: result });
      } else {
        res.status(200).json({ status: 200, data: "Data tidak ditemukan" });
      }
    } catch (err) {
      console.log(err);
      res.status(500).json({ status: 500, data: "internal error" });
    }
  } else {
    console.log("no header");
    res.sendStatus(403);
  }
});

//Add Group
router.post("/regisGroup", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
    var apps = dcodeInfo.apps[0];
    let idTenant = dcodeInfo.idtenant;
    try {
      // const { group, modules, menus } = req.body;
      let insertData = await groupUtils.insertGroup(
        req.body,
        apps,
        idTenant,
        dcodeInfo.leveltenant
      );
      if (insertData === 1) {
        res.status(200).json({ status: 200, data: "" });
      } else {
        res.status(422).json({ status: 422, data: "Unprocessable data" });
      }
    } catch (err) {
      console.log(err);
      res.status(422).json({ status: 422, data: "Unprocessable data" });
    }
 
});
//Get Detail Group
router.get("/getDetailsGroup/:id", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  
    var apps = dcodeInfo.apps[0];
    try {
      let groupId = req.params.id;
      let result = {};
      const userGroup = await pool.query(
        "Select * from public.mst_usergroup where id = $1",
        [groupId]
      );
      if (userGroup.rows.length > 0) {
        result.group = userGroup.rows[0];
        const modules = await pool.query(
          "select mm.modulename ,idgroup, mm.idtenant, idmodule as id, idmenu, fcreate, fread, fupdate, fdelete, fview, fapproval, uga.created_byid, uga.created_at, uga.updated_at from public.mst_usergroupacl uga inner join  public.mst_moduleby_tenant mm on uga.idmodule = mm.id where uga.idgroup = $1 and uga.idmenu is null ; ",
          [groupId]
        );
        if (modules.rows.length > 0) {
          result.modules = modules.rows;
          const menus = await pool.query(
            `select mm.title,mm.id  as "menuId",uga.* from public.mst_usergroupacl uga inner join  public.mst_menu mm on uga.idmenu = mm.id  where uga.idgroup = $1 and uga.idmenu is not null`,
            [groupId]
          );
          result.menus = menus.rows;
          console.log("############# RESULT "+menus.rows.length)
        }
        if (result.menus.length > 0) {
          await result.menus.map((menu) => {
            menu.acl = {
              read: menu.fread,
              create: menu.fcreate,
              update: menu.fupdate,
              delete: menu.fdelete,
            };
          });
          res.status(200).json({ status: 200, data: { result } });
        }
      } else {
        res.status(200).json({ status: 200, data: "Data tidak ditemukan" });
      }
    } catch (err) {
      console.log(err);
      res.status(500).json({ status: 500, data: "internal error" });
    }
  
});

//Edit Group
router.post("/editGroup", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
 
    var apps = dcodeInfo.apps[0];
    try {
      const { group, modules, menus } = req.body;
      let idTenant = dcodeInfo.idtenant;
      let editGroup = await groupUtils.editGroup(group, apps, idTenant);

      let editModules = await groupUtils.editModules(
        modules,
        group.groupId,
        apps,
        idTenant,
        dcodeInfo.leveltenant
      );
      let editMenus = await groupUtils.editMenus(
        menus,
        group.groupId,
        apps,
        idTenant,
        dcodeInfo.leveltenant
      );
      if (editGroup && editModules === 1 && editMenus === 1) {
        res.status(200).json({ status: 200, data: "" });
      } else {
        res.status(422).json({ status: 422, data: "Unprocessable data" });
      }
    } catch (err) {
      console.log(err);
      res.status(422).json({ status: 422, data: "Unprocessable data" });
    }
  
});
//Delete Group
router.post("/deleteGroup", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  
    var apps = dcodeInfo.apps[0];
    try {
      const { group, modules, menus } = req.body;
      let deleteData = await groupUtils.deleteGroup(req.body, apps);
      console.log(deleteData);
      if (deleteData === 1) {
        res.status(200).json({ status: 200, data: "" });
      } else {
        res.status(422).json({ status: 422, data: "Unprocessable data" });
      }
    } catch (err) {
      console.log(err);
      res.status(422).json({ status: 422, data: "Unprocessable data" });
    }
  
});

module.exports = router;

// router.get("/getAllGroup", async (req, res, next) => {
//   const authHeader = req.headers.authorization;
//   var dcodeInfo = null;
//   if (authHeader) {
//     TokenArray = authHeader.split(" ");
//     dcodeInfo = await jwttools.decryptdata(TokenArray[1]);
//     // console.log(">>>>>>>>>>>> decoded "+JSON.stringify(dcodeInfo));
//     var apps = dcodeInfo.apps[0];
//     // console.log(">>>>>>>>>>>> decoded "+JSON.stringify(apps));
//     try {
//       // console.log(">>>>>>>>>>>> Tenanr, Level "+JSON.stringify(dcodeInfo.leveltenant));
//       console.log(dcodeInfo.leveltenant);
//       var query =
//         "Select * from public.mst_usergroup where idapplication=$1 and idtenant=$2";
//       // var idtype = dcodeInfo.leveltenant;

//       // console.log(">>>>>>>>>>>> APPLICATION, Level "+(idtype != "0") );

//       // if(idtype != "0" ) {
//       // query =
//       //   "Select * from public.mst_usergroup where idapplication=$1 and idtenant=$2 and grouptype=$3 or grouptype = $4";
//       query =
//         "Select * from public.mst_usergroup where idapplication=$1 and idtenant=$2 and grouptype >= $3";
//       // };
//       // console.log(">>>>>>>>>>>> Query, Level "+(dcodeInfo.leveltenant == "0") );
//       // console.log(">>>>>>>>>>>> IDApp "+apps.id_application );
//       console.log(query);
//       console.log(apps.id, dcodeInfo.idtenant, 1, dcodeInfo.leveltenant);
//       const userGroup = await pool.query(
//         query,
//         dcodeInfo.leveltenant == "0"
//           ? [apps.id, dcodeInfo.idtenant, 0]
//           : [apps.id, dcodeInfo.idtenant, dcodeInfo.leveltenant]
//       );
//       // const userGroup = await pool.query(
//       //   "Select * from public.mst_usergroup",
//       //   []
//       // );
//       if (userGroup.rows.length > 0) {
//         res
//           .status(200)
//           .json({ status: 200, data: { userGroup: userGroup.rows } });
//       } else {
//         res.status(200).json({ status: 200, data: "Data tidak ditemukan" });
//       }
//     } catch (err) {
//       console.log(err);
//       res.status(500).json({ status: 500, data: "internal error" });
//     }
//   } else {
//     console.log("no header");
//     res.sendStatus(403);
//   }
// });


// router.get("/getallmodulewithmenu", async (req, res, next) => {
//   const authHeader = req.headers.authorization;
//   var dcodeInfo = null;
//   if (authHeader) {
//     TokenArray = authHeader.split(" ");
//     dcodeInfo = await jwttools.decryptdata(TokenArray[1]);

//     var apps = dcodeInfo.apps[0];
//     try {
//       const modules = await pool.query(
//         "Select * from public.mst_moduleby_tenant ",
//         []
//       );
//       if (modules.rows.length > 0) {
//         let result = [];
//         try {
//           let modulesList = modules.rows;

//           await Promise.all(
//             modulesList.map(async (module) => {
//               let moduleDetail = {};
//               moduleDetail.moduleId = module.id;
//               moduleDetail.moduleName = module.modulename;
//               let menus = await pool.query(
//                 "select * from public.mst_menu where idmodule = $1",
//                 [module.id]
//               );
//               if (menus.rows.length > 0) {
//                 let menuRows = menus.rows;
//                 let menuList = [];
//                 let menuDetail = {};
//                 await Promise.all(
//                   menuRows.map((menu) => {
//                     menuDetail = {};
//                     menuDetail.menuName = menu.title;
//                     menuDetail.menuId = menu.id;
//                     menuList.push(menuDetail);
//                   })
//                 );

//                 moduleDetail.moduleMenus = menuList;
//               }
//               result.push(moduleDetail);
//             })
//           );

//           res.status(200).json({ status: 200, data: result });
//         } catch (error) {
//           console.log(err);
//         }
//       } else {
//         res.status(200).json({ status: 200, data: "Data tidak ditemukan" });
//       }
//     } catch (err) {
//       console.log(err);
//       res.status(500).json({ status: 500, data: "internal error" });
//     }
//   } else {
//     console.log("no header");
//     res.sendStatus(403);
//   }
// });

// router.post("/regisGroup", async (req, res, next) => {
//   const authHeader = req.headers.authorization;
//   var dcodeInfo = null;
//   if (authHeader) {
//     TokenArray = authHeader.split(" ");
//     dcodeInfo = await jwttools.decryptdata(TokenArray[1]);
//     var apps = dcodeInfo.apps[0];
//     let idTenant = dcodeInfo.idtenant;
//     try {
//       // const { group, modules, menus } = req.body;
//       let insertData = await groupUtils.insertGroup(
//         req.body,
//         apps,
//         idTenant,
//         dcodeInfo.leveltenant
//       );

//       if (insertData === 1) {
//         res.status(200).json({ status: 200, data: "" });
//       } else {
//         res.status(422).json({ status: 422, data: "Unprocessable data" });
//       }
//     } catch (err) {
//       console.log(err);
//       res.status(422).json({ status: 422, data: "Unprocessable data" });
//     }
//   } else {
//     console.log("no header");
//     res.sendStatus(403);
//   }
// });

// router.post("/regisGroup", async (req, res, next) => {
//   const authHeader = req.headers.authorization;
//   var dcodeInfo = null;
//   if (authHeader) {
//     TokenArray = authHeader.split(" ");
//     dcodeInfo = await jwttools.decryptdata(TokenArray[1]);
//     var apps = dcodeInfo.apps[0];
//     let idTenant = dcodeInfo.idtenant;
//     try {
//       // const { group, modules, menus } = req.body;
//       let insertData = await groupUtils.insertGroup(
//         req.body,
//         apps,
//         idTenant,
//         dcodeInfo.leveltenant
//       );

//       if (insertData === 1) {
//         res.status(200).json({ status: 200, data: "" });
//       } else {
//         res.status(422).json({ status: 422, data: "Unprocessable data" });
//       }
//     } catch (err) {
//       console.log(err);
//       res.status(422).json({ status: 422, data: "Unprocessable data" });
//     }
//   } else {
//     console.log("no header");
//     res.sendStatus(403);
//   }
// });



// router.get("/getDetailsGroup/:id", async (req, res, next) => {
//   const authHeader = req.headers.authorization;
//   var dcodeInfo = null;
//   if (authHeader) {
//     TokenArray = authHeader.split(" ");
//     dcodeInfo = await jwttools.decryptdata(TokenArray[1]);

//     var apps = dcodeInfo.apps[0];
//     try {
//       let groupId = req.params.id;
//       let result = {};
//       const userGroup = await pool.query(
//         "Select * from public.mst_usergroup where id = $1",
//         [groupId]
//       );
//       if (userGroup.rows.length > 0) {
//         result.group = userGroup.rows[0];
//         const modules = await pool.query(
//           "select mm.modulename ,idgroup, mm.idtenant, idmodule as id, idmenu, fcreate, fread, fupdate, fdelete, fview, fapproval, uga.created_byid, uga.created_at, uga.updated_at from public.mst_usergroupacl uga inner join  public.mst_moduleby_tenant mm on uga.idmodule = mm.id where uga.idgroup = $1 and uga.idmenu is null ; ",
//           [groupId]
//         );
//         if (modules.rows.length > 0) {
//           result.modules = modules.rows;
//           const menus = await pool.query(
//             `select mm.title,mm.id  as "menuId",uga.* from public.mst_usergroupacl uga inner join  public.mst_menu mm on uga.idmenu = mm.id  where uga.idgroup = $1 and uga.idmenu is not null`,
//             [groupId]
//           );
//           result.menus = menus.rows;
//         }
//         if (result.menus.length > 0) {
//           await result.menus.map((menu) => {
//             menu.acl = {
//               read: menu.fread,
//               create: menu.fcreate,
//               update: menu.fupdate,
//               delete: menu.fdelete,
//             };
//           });
//           res.status(200).json({ status: 200, data: { result } });
//         }
//       } else {
//         res.status(200).json({ status: 200, data: "Data tidak ditemukan" });
//       }
//     } catch (err) {
//       console.log(err);
//       res.status(500).json({ status: 500, data: "internal error" });
//     }
//   } else {
//     console.log("no header");
//     res.sendStatus(403);
//   }
// });
// router.post("/editGroup", async (req, res, next) => {
//   const authHeader = req.headers.authorization;
//   var dcodeInfo = null;
//   if (authHeader) {
//     TokenArray = authHeader.split(" ");
//     dcodeInfo = await jwttools.decryptdata(TokenArray[1]);

//     var apps = dcodeInfo.apps[0];
//     try {
//       const { group, modules, menus } = req.body;
//       let idTenant = dcodeInfo.idtenant;
//       let editGroup = await groupUtils.editGroup(group, apps, idTenant);

//       let editModules = await groupUtils.editModules(
//         modules,
//         group.groupId,
//         apps,
//         idTenant,
//         dcodeInfo.leveltenant
//       );
//       let editMenus = await groupUtils.editMenus(
//         menus,
//         group.groupId,
//         apps,
//         idTenant,
//         dcodeInfo.leveltenant
//       );
//       if (editGroup && editModules === 1 && editMenus === 1) {
//         res.status(200).json({ status: 200, data: "" });
//       } else {
//         res.status(422).json({ status: 422, data: "Unprocessable data" });
//       }
//     } catch (err) {
//       console.log(err);
//       res.status(422).json({ status: 422, data: "Unprocessable data" });
//     }
//   } else {
//     console.log("no header");
//     res.sendStatus(403);
//   }
// });


// router.post("/deleteGroup", async (req, res, next) => {
//   const authHeader = req.headers.authorization;
//   var dcodeInfo = null;
//   if (authHeader) {
//     TokenArray = authHeader.split(" ");
//     dcodeInfo = await jwttools.decryptdata(TokenArray[1]);

//     var apps = dcodeInfo.apps[0];
//     try {
//       const { group, modules, menus } = req.body;
//       let deleteData = await groupUtils.deleteGroup(req.body, apps);
//       console.log(deleteData);
//       if (deleteData === 1) {
//         res.status(200).json({ status: 200, data: "" });
//       } else {
//         res.status(422).json({ status: 422, data: "Unprocessable data" });
//       }
//     } catch (err) {
//       console.log(err);
//       res.status(422).json({ status: 422, data: "Unprocessable data" });
//     }
//   } else {
//     console.log("no header");
//     res.sendStatus(403);
//   }
// });