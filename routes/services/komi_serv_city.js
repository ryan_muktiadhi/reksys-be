const express = require("express"),
  app = express();
const router = express.Router();
const checkAuth = require("../../middleware/check-auth");
router.use(checkAuth);
const knex = require("../../connection/dborm");
// const conn
const conn = knex.conn();

//getAllGroup
router.get("/getAll", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  var idtenant = dcodeInfo.idtenant;

  try {
    const city = await conn
      .select(
        "id",
        "city_code",
        "city_name",
        "status",
        conn
          .raw(`TO_CHAR("created_date", 'dd MON yyyy')`)
          .wrap("(", ") created_date"),
        "created_by",
        conn
          .raw(`TO_CHAR("updated_date", 'dd MON yyyy')`)
          .wrap("(", ") updated_date"),
        "updated_by"
      )
      .from("m_city")
      .orderBy("created_date", "DESC");

    if (city.length > 0) {
      setTimeout(function () {
        res.status(200).json({
          status: 200,
          data: city,
        });
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({
          status: 202,
          data: "Data not found",
        });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({
      status: 500,
      data: "Internal Server Error",
    });
  }
});

router.post("/insert", async (req, res, next) => {
  var dcodeInfo = req.userData;
  const userId = dcodeInfo.id;
  const idtenant = dcodeInfo.idtenant;

  try {
    const { cityCode, cityName, status } = req.body;

    // Check Already exist
    const cityExsist = await conn
      .select("*")
      .from("m_city")
      .where("city_code", cityCode)
      .andWhere("city_name", cityName);

    if (cityExsist.length > 0) {
      res.status(200).json({
        status: 409,
        data: "Data Already Exist",
      });
    } else {
      const resp = await conn("m_city")
        .insert({
          city_code: cityCode,
          city_name: cityName,
          status: status,
          created_date: conn.fn.now(),
          created_by: userId,
        })
        .returning(["id"]);
      console.log(resp);
      if (resp.length > 0) {
        console.log("Success Insert City data");
        res.status(200).json({
          status: 200,
          data: "Success insert data",
        });
      } else {
        console.log("Failed Insert City data");
        res.status(200).json({
          status: 500,
          data: "Failed insert data",
        });
      }
    }
  } catch (err) {
    console.log("Failed Insert City data");
    console.log(err);
    res.status(500).json({
      status: 500,
      data: "Internal Server Error",
    });
  }
});

router.post("/update", async (req, res, next) => {
  var dcodeInfo = req.userData;
  const userId = dcodeInfo.id;

  try {
    const { id, cityCode, cityName, status } = req.body;

    // Check Already exist
    const cityExsist = await conn
      .select("*")
      .from("m_city")
      .where("city_code", cityCode)
      .andWhere("city_name", cityName)
      .andWhere("status", status);

    if (cityExsist.length > 0) {
      res.status(200).json({
        status: 409,
        data: "Data already updated",
      });
    } else {
      const resp = await conn("m_city").where("id", id).update({
        city_code: cityCode,
        city_name: cityName,
        status: status,
        updated_date: conn.fn.now(),
        updated_by: userId,
      });
      if (resp > 0) {
        console.log("Success update City data");
        res.status(200).json({
          status: 200,
          data: resp,
        });
      } else {
        console.log("Failed update City data");
        res.status(200).json({
          status: 500,
          data: "Failed update data",
        });
      }
    }
  } catch (err) {
    console.log("Failed update City data");
    console.log(err);
    res.status(500).json({
      status: 500,
      data: "Internal server error",
    });
  }
});

router.get("/delete/:id", async (req, res, next) => {
  var dcodeInfo = req.userData;
  let id = req.params.id;

  try {
    let resp = await conn("m_city").where("id", id).del();
    console.log("Success delete City data");
    res.status(200).json({
      status: 200,
      data: "Success",
    });
  } catch (err) {
    console.log("Failed delete City data");
    console.log(err);
    res.status(500).json({
      status: 500,
      data: "Error delete data",
    });
  }
});

module.exports = router;
