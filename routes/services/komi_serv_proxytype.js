const express = require("express"),
  app = express();
const router = express.Router();
const jwttools = require("./utils/encryptdecryptjwt");
const groupUtils = require("./utils/groupUtils");
const pool = require("../../connection/db");
const checkAuth = require("../../middleware/check-auth");
const moment = require("moment");
const knex = require("../../connection/dborm");
const msg = require("./utils/responseMessage");
const { body, validationResult } = require("express-validator");
router.use(checkAuth);

var conf = require("../../config.json");
// const conn
const conn = knex.conn();

//getAllGroup
router.get("/getAll", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  var apps = dcodeInfo.apps[0];
  try {
    console.log(
      ">>>>>>>>>>>> Tenanr, Level " + JSON.stringify(dcodeInfo.leveltenant)
    );

    const proxyType = await conn
      .select("*")
      .from("m_proxytype")
      .orderBy("created_date", "ASC");

    if (proxyType.length > 0) {
      setTimeout(function () {
        res.status(200).json({ status: 200, data: proxyType });
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({ status: 202, data: {} });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Internal server error" });
  }
});

router.get("/getByStatusActive", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  var apps = dcodeInfo.apps[0];

  try {
    console.log(
      ">>>>>>>>>>>> Tenanr, Level " + JSON.stringify(dcodeInfo.leveltenant)
    );

    const proxyType = await conn
      .select("*")
      .from("m_proxytype")
      .where("status", "1")
      .orderBy("created_date", "ASC");

    if (proxyType.length > 0) {
      console.log("Success get Proxy Status data By Status active");
      setTimeout(function () {
        res.status(200).json({ status: 200, data: proxyType });
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({ status: 202, data: {} });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Internal server error" });
  }
});

router.get("/getById/:id", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  // var apps = dcodeInfo.apps[0];

  try {
    let id = req.params.id;

    const proxyType = await conn
      .select(
        "id",
        "proxy_type",
        "description",
        "status",
        "created_date",
        "created_by",
        "updated_date",
        "updated_by"
      )
      .from("m_proxytype")
      .where("id", id)
      .orderBy("created_date", "ASC");

    if (proxyType.length > 0) {
      setTimeout(function () {
        res.status(200).json({ status: 200, data: proxyType[0] });
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({ status: 202, data: {} });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Internal server error" });
  }
});

router.post(
  "/insert",
  body("proxyCode", "Proxy Code   maximum 255 character").isLength({
    max: 255,
  }),
  body("proxyName", "Proxy Name maximum 255 character").isLength({
    max: 255,
  }),
  async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        success: false,
        errors: errors.array(),
      });
    }
    var dcodeInfo = req.userData;
    var apps = dcodeInfo.apps[0];
    const userid = dcodeInfo.userid;

    try {
      const { proxyCode, proxyName, status } = req.body;
      console.log(proxyName);
      // Check Already exsist
      const proxyTypeExsist = await conn
        .select("proxy_type")
        .where("proxy_type", proxyCode.trim())
        .from("m_proxytype");

      if (proxyTypeExsist.length > 0) {
        return res
          .status(200)
          .json({ status: 409, data: msg.ADD_DATA_ALREADY_EXSIST });
      }

      const resp = await conn("m_proxytype").insert({
        proxy_code: proxyCode,
        proxy_name: proxyName,
        status: status,
        created_by: userid,
        created_date: conn.fn.now(),
      });
      res.status(200).json({ status: 200, data: msg.ADD_DATA_SUCCESS });
    } catch (err) {
      console.log(err);
      res.status(500).json({ status: 500, data: "Internal server error" });
    }
  }
);

router.post(
  "/update",
  body("proxyCode", "Proxy Code  maximum 255 character").isLength({
    max: 255,
  }),
  body("proxyName", "Proxy Name maximum 255 character").isLength({
    max: 255,
  }),
  async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        success: false,
        errors: errors.array(),
      });
    }
    var dcodeInfo = req.userData;
    var apps = dcodeInfo.apps[0];
    const userid = dcodeInfo.userid;

    try {
      const { id, proxyCode, proxyName, status } = req.body;
      const today = moment().format("YYYY-MM-DD HH:mm:ss");

      // Check eksist data
      const proxyTypeExsist = await conn
        .select("*")
        .where("id", id)
        .from("m_proxytype");

      if (proxyTypeExsist.length < 0) {
        console.log("Failed update Account Type data");
        return res
          .status(200)
          .json({ status: 204, data: msg.UPDATE_DATA_DOSENT_EXSIST });
      }

      const resp = await conn("m_proxytype")
        .update({
          proxy_code: proxyCode,
          proxy_name: proxyName,
          status: status,
          created_by: proxyTypeExsist.CREATED_BY,
          created_date: proxyTypeExsist.CREATED_DATE,
          updated_by: userid,
          updated_date: conn.fn.now(),
        })
        .where("id", id);
      res.status(200).json({ status: 200, data: msg.UPDATA_DATA_SUCCESS });
    } catch (err) {
      console.log(err);
      res.status(500).json({ status: 500, data: "Internal server error" });
    }
  }
);

router.get("/delete/:id", async (req, res, next) => {
  var dcodeInfo = req.userData;
  var apps = dcodeInfo.apps[0];
  let id = req.params.id;

  try {
    let resp = await conn("m_proxytype").where("id", id).del();
    res.status(200).json({ status: 200, data: msg.DELETE_DATA_SUCCESS });
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error retrive Users" });
  }
});

module.exports = router;
