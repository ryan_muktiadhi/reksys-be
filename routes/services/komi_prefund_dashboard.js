const express = require("express"),
  app = express();
const router = express.Router();
const jwttools = require("../../routes/services/utils/encryptdecryptjwt");
const groupUtils = require("./utils/groupUtils");
const pool = require("../../connection/db");
const checkAuth = require("../../middleware/check-auth");
const moment = require("moment");
const knex = require("../../connection/dborm");
const axios = require("axios");
const conn = knex.conn();
const shell = require("shelljs");
var osu = require("node-os-utils");
const conf = require("../../config.json");
const systemParam = require("./utils/getSystemParam");
const apiserver1 = conf.serverkomicore;

router.use(checkAuth);
let param;
const getParam = async () => {
  param = await systemParam.getAllParam();
};

//getAllGroup
router.get("/getProp", async (req, res, next) => {
  var dcodeInfo = req.userData;
  try {
    // var query = "SELECT * FROM prefund_dashboard";
    //const data = await pool.query(query, []);
    const data = await conn.select("*").from("prefund_dashboard");
    if (data.length > 0) {
      setTimeout(function () {
        res.status(200).json({ status: 200, data: { dashboardProp: data[0] } });
      }, 500);
    } else {
      setTimeout(function () {
        let rsult = { dashboardProp: { min: 0, max: 0, curent: 0 } };

        res.status(200).json({ status: 202, data: "Data tidak ditemukan" });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.get("/getHistory", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  // var apps = dcodeInfo.apps[0];
  try {
    let id = req.params.id;
    // console.log(">>>>>>>>>>>> Tenanr, Level "+JSON.stringify(dcodeInfo.leveltenant));
    //   console.log(dcodeInfo.leveltenant);TO_CHAR(clis.expiredate,'yyyy-MM-dd')
    // var query =
    //   "SELECT  *, TO_CHAR(datetime,'yyyy-MM-dd HH:mm:ss') formated_date FROM prefund_history  order by datetime desc";
    // const accTypeData = await pool.query(query, []);

    const accTypeData = await conn.select("*").from("prefund_history");
    if (accTypeData.length > 0) {
      setTimeout(function () {
        res
          .status(200)
          .json({ status: 200, data: { historyData: accTypeData } });
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({ status: 202, data: "Data tidak ditemukan" });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.post("/update", async (req, res, next) => {
  var dcodeInfo = req.userData;

  try {
    var apps = dcodeInfo.apps[0];
    const { id, min, max, upperLimit, referenceLimit, amberLevel, redLevel } = req.body;
    const today = moment().format("YYYY-MM-DD HH:mm:ss");
    // const resp = await pool.query(
    //   "UPDATE prefund_dashboard SET min=$1, max=$2 where id=$3",
    //   [min, max, id]
    // );

    const reqData = {
      idapp: apps.idapp,
      idtenant: dcodeInfo.idtenant,
      cdmenu: "PRFUND",
      jsondata: JSON.stringify(req.body),
      status: 2,
      typedata: 7,
    };

    if (apps.withapproval > 0) {
      console.log("LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL " + apps.withapproval);
      axios({
        method: "post",
        url: conf.kktserver + "/adm/approval/insertApprovalTmp",
        headers: {},
        data: {
          reqData: reqData,
        },
      }).then(async (resp) => {
        console.log("insertApprovalTmp", resp.status);
        if (resp.status == 200) {
          var userObj = resp.data.data[0];
          console.log("userObj", userObj);
          console.log("userObj", userObj.id);
          let idreturn = userObj.id;
          const updatePrefund = await conn("prefund_dashboard")
              .where("id", id)
              .update({
                active: 7,
                idapproval: idreturn,
              });
          if (updatePrefund > 0) {
            var userObj = updatePrefund[0];
            res.status(200).json({ status: 200, data: userObj });
          } else {
            res.status(500).json({
              status: 500,
              data: "Error insert kc_channel ",
            });
          }
        }
        // res.status(200).json({ status: 200, data: resp.data });
      });
    } else {
      const resp = await conn("prefund_dashboard")
          .where("id", id)
          .update({
            min: min,
            max: max,
            upper_limit: upperLimit,
            reference_limit: referenceLimit,
            amber_level: amberLevel,
            red_level: redLevel,
      });
      console.log(resp);
      if (resp > 0) {
        res.status(200).json({ status: 200, data: resp });
      } else {
        res.status(500).json({
          status: 500,
          data: "Error insert m_bic ",
        });
      }
    }


  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error insert User" });
  }
});

router.post("/external/balanceinquiry", async (req, res, next) => {
  console.log(">>>>>>>>>>>>>>>>>>>>>>>>>/external/balanceInquiry");
  await getParam();
  let paramNoACC = param.KOMI_PORTAL_BALANCE_NOACC;
  console.log(paramNoACC);

  //var requestBody = req.body;
  //const apiserver3 = "http://10.11.100.116:9003";
  //console.log(req.body);

  //const requestBody = req.body;

  var requestBody2 = {
    transactionId: "000001",
    dateTime: "2021-12-21T11:52:32.923",
    merchantType: "6666",
    terminalId: "KOMI000001",
    noRef: "KOM21122111523210000",
    accountNumber: paramNoACC,
  };

  try {
    const resp = await axios
      .post(
        param.KOMI_PORTAL_PORT_MANTAP + "/komi/api/v1/adapter/balanceinquiry",
        requestBody2
      )
      .then((response) => res.json(response.data))
      .catch((err) => res.send(err));
  } catch (err) {
    console.error("error", err);
  }
});

router.get("/getMonitoringUsed", async (req, res, next) => {
  // console.log(">>>>>>>>>>>>>>>>>>>>>>>>>getMonitoringUsed");
  await getParam();
  try {
    axios
      .get(param.KOMI_PORTAL_API_SURR_URL + "/api/monitoring/getMonitoringUsed")
      .then((response) => res.json(response.data))
      .catch((err) => res.send(err));
  } catch (err) {
    console.error("error", err);
  }
});

router.get("/getMonitoringPortal", async (req, res, next) => {
  // console.log(">>>>>>>>>>>>>>>>>>>>>>>>>getMonitoring");
  try {
    // cpu
    var cpu = osu.cpu;
    var mem = osu.mem;
    var drive = osu.drive;
    cpu.usage().then((infoCpu) => {
      // console.log("cpu : ", infoCpu);
      mem.info().then((infoMemo) => {
        // console.log("memo : ", infoMemo);
        drive.info().then((infoDrive) => {
          // console.log("drive : ", infoDrive);
          resp = [
            { processor: { used: infoCpu }, memory: infoMemo, disk: infoDrive },
          ];
          res.status(200).json({ status: 200, data: resp[0] });
        });
      });
    });
  } catch (err) {
    console.error("error", err);
  }
});

router.get("/getCheckService", async (req, res, next) => {
  console.log(">>>>>>>>>>>>>>>>>>>>>>>>>checkService");
  try {
    await getParam();
    const moduleName = "server.mantabcorebanking";

    axios({
      method: "get",
      url: param.KOMI_PORTAL_API_SURR_URL + "/api/monitoringApp/checkService",
      headers: {},
    })
      .then(async (response) => {
        res.status(200).json({ status: 200, data: response.data });
      })
      .catch((err) => res.send(err));
  } catch (err) {
    console.error("error", err);
    res.status(500).json({ status: 500, data: "Error" });
  }
});
module.exports = router;
