const express = require("express"),
  app = express();
const router = express.Router();
const checkAuth = require("../../middleware/check-auth");
const knex = require("../../connection/dborm");

var conf = require("../../config.json");
const moment = require("moment");
const { duration } = require("moment-timezone");
const conn = knex.conn();

const multer = require("multer");
const upload = multer({ dest: "./public/fileupload/" });
const fs = require("fs");
// const csv = require("fast-csv");
const csv = require("csvtojson");
const systemParam = require("./utils/getSystemParam");
var readline = require("readline");
router.use(checkAuth);

router.post("/uploadfile", upload.single("doccsv"), async (req, res, next) => {
  var dcodeInfo = req.userData;
  let param = await systemParam.getAllParam();
  try {
    let origFile = req.file;
    let csvFilePath2 = "";

    /*
    fs.readFile(
      "./public/fileupload/" + origFile.filename,
      async function (err, data) {
        // console.log("Data :>> ", data);
        fs.writeFile(
          "./public/fileupload/" + origFile.originalname,
          data,
          async function (err) {
            if (err) {
              return console.log("Error pas Nulis : ", err);
            }
            fs.unlink(
              "./public/fileupload/" + origFile.originalname,
              async function () {
                // console.log("The file was deleted!");
                if (err) throw err;
              }
            );
            fs.unlink(
              "./public/fileupload/" + origFile.filename,
              async function () {
                // console.log("The file was deleted!");
                if (err) throw err;
              }
            );
            // console.log("The file was saved!");
          }
        );
      }
    );
    */

    var cntr = 1;
    let header = param.KOMI_PORTAL_REKON_HEADER;
    // "No,Time,Transaction ID,Business Message ID,Message ID,Reff. Number,Counterparty,Transaction By Service Type,Debit Amount,Credit Amount,Balance,Status,Status Reason Code";

    var rl = readline.createInterface({
      input: fs.createReadStream("./public/fileupload/" + origFile.filename),
    });
    csvFilePath2 = fs.createWriteStream(
      "./public/fileupload/" + origFile.originalname,
      {
        flags: "a", // 'a' means appending (old data will be preserved)
      }
    );
    csvFilePath2.write(header + "\n");

    rl.on("line", function (line) {
      if (cntr++ >= param.KOMI_PORTAL_REKON_FROMLINE) {
        // console.log(`Line read: ${line}`);
        csvFilePath2.write(line + "\n");
      }
    });

    fs.unlink("./public/fileupload/" + origFile.filename, async function () {});
    res.status(200).json({ status: 200, data: origFile.originalname });
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error insert User" });
  }
});

router.post("/rekonsiliasi", async (req, res, next) => {
  let param = await systemParam.getAllParam();
  let origFile = req.body;
  try {
    const today = moment().format("YYYY-MM-DD HH:mm:ss");
    const jsonObj3 = await conn.select("*").from("t_transaction");
    let csvFilePath2 = "public/fileupload/" + origFile.originalname;

    csv()
      .fromFile(csvFilePath2)
      .then(async (jsonObj2) => {
        /*
      Business Message ID (done)
      Message ID ??
      Counterparty == Debit itu Outgoing = recipent_bank == Credit itu Incoming = sender_bank  (done)
      Transaction By Service Type = trx_proxy_flag ( T, Credit Transfer && Y, Credit Transfer with Proxy) (notyet)
      Debit Amount = trx_type = O (done)
      Credit Amount = trx_type = I (done)
      Status = trx_status_code (done)
      Status Reason Code = trx_reason_code (done)
      */

        // mencari date
        // const date = await searchDate(jsonObj2);

        // select db
        // const jsonObj3 = await selectTrx(
        //   date.startDate,
        //   date.endDate,
        //   date.interStartDate,
        //   date.interEndDate
        // );

        //mencari Status
        const stat = await changeStatus(jsonObj2);

        //mencari Transaction By Service Type
        const trxType = await changeTrxType(jsonObj2);

        // match data
        if (jsonObj2.length < param.KOMI_PORTAL_REKON_COUNT) {
          const result = jsonObj2.filter(
            (f) =>
              !jsonObj3.some(
                (d) =>
                  d.bifast_trx_no == f["Business Message ID"] &&
                  (d.recipient_bank == f["Counterparty"] ||
                    d.sender_bank == f["Counterparty"]) &&
                  (d.trx_amount == f["Debit Amount"] ||
                    d.trx_amount == f["Credit Amount"]) &&
                  d.trx_reason_code == f["Status Reason Code"] &&
                  d.trx_status_code == f["Status"] &&
                  d.trx_proxy_flag == f["Transaction By Service Type"]
                // JSON.stringify(d.trx_initiation_date) ==
                //   JSON.stringify(f.trx_initiation_date)
              )
          );
          console.log(result.length);
          res.status(200).json({ status: 200, data: result });
          if (result.length > -1) {
            fs.unlink(
              "./public/fileupload/" + origFile.originalname,
              async function () {}
            );
          }
        } else {
          let cek = param.KOMI_PORTAL_REKON_COUNT;
          let resp = [
            {
              resp: 1,
              msg: "Data Tidak Boleh Lebih Dari " + cek,
            },
          ];
          res.status(200).json({ status: 200, data: resp[0] });
        }
      });

    // res.status(200).json({ status: 200, data: "Success" });
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error insert User" });
  }
});

const selectTrx = async (startDate, endDate, interStartDate, interEndDate) => {
  try {
    console.log("MASUK SELECT TRX");
    const jsonObj3 = conn("t_transaction")
      .select("*")
      .from("t_transaction")
      .modify(function (queryBuilder) {
        if (startDate && endDate && !(startDate == endDate)) {
          // console.log("MASUK FALSE");
          queryBuilder.whereBetween("trx_initiation_date", [
            startDate,
            interEndDate,
          ]);
        }
        if (startDate == endDate) {
          queryBuilder.whereBetween("trx_initiation_date", [
            startDate,
            interStartDate,
          ]);
        }
      });
    return jsonObj3;
  } catch (err) {
    console.log(err);
    return 0;
  }
};

const changeStatus = async (jsonObj2) => {
  try {
    console.log("MASUK Change Status");
    const elementStat = [];
    for (let i = 0; i < jsonObj2.length; i++) {
      if (jsonObj2[i]["Status"] == "Successful") {
        jsonObj2[i]["Status"] = "S";
      } else {
        jsonObj2[i]["Status"] = "E";
      }
    }
    return jsonObj2;
  } catch (err) {
    console.log(err);
    return 0;
  }
};

const changeTrxType = async (jsonObj2) => {
  try {
    console.log("MASUK Change Trx Type");
    const elementTrxType = [];
    for (let i = 0; i < jsonObj2.length; i++) {
      if (jsonObj2[i]["Transaction By Service Type"] == "Credit Transfer") {
        jsonObj2[i]["Transaction By Service Type"] = "T";
      } else {
        jsonObj2[i]["Transaction By Service Type"] = "Y";
      }
    }

    return jsonObj2;
  } catch (err) {
    console.log(err);
    return 0;
  }
};

const searchDate = async (jsonObj2) => {
  try {
    console.log("MASUK SEARCH TRX DATE");
    var elementDate = [];
    for (let i = 0; i < jsonObj2.length; i++) {
      elementDate.push(
        new Date(moment(jsonObj2[i].trx_initiation_date).format("YYYY-MM-DD"))
      );
    }
    var endDate = new Date(Math.max.apply(null, elementDate));
    var startDate = new Date(Math.min.apply(null, elementDate));

    var interStartDate = new Date(
      startDate.getFullYear(),
      startDate.getMonth(),
      startDate.getDate() + 1
    );

    var interEndDate = new Date(
      endDate.getFullYear(),
      endDate.getMonth(),
      endDate.getDate() + 1
    );

    startDate = moment(startDate).format("YYYY-MM-DD");
    endDate = moment(endDate).format("YYYY-MM-DD");
    interStartDate = moment(interStartDate).format("YYYY-MM-DD");
    interEndDate = moment(interEndDate).format("YYYY-MM-DD");

    const resp = [
      {
        startDate: startDate,
        endDate: endDate,
        interStartDate: interStartDate,
        interEndDate: interEndDate,
      },
    ];

    return resp[0];
  } catch (err) {
    console.log(err);
    return 0;
  }
};

module.exports = router;
