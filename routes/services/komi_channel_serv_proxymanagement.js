const express = require("express"),
  app = express();
const router = express.Router();
const checkAuth = require("../../middleware/check-auth");
const moment = require("moment");
const httpUtil = require("./utils/httpUtils");
const trxIdUtil = require("./utils/transactionIdUtils");
const { fork } = require("child_process");
const knex = require("../../connection/dborm");
var conf = require("../../config.json");

const conn = knex.conn();
//const kktconn = knex.kktdbconn();

const cid = conf.komichannel.id;
const channelType = conf.komichannel.channel;
const merchantType = conf.komichannel.merchant;

router.use(checkAuth);

router.post("/", async (req, res, next) => {
  var dcodeInfo = req.userData;
  var appid = dcodeInfo.apps[0].idapp;
  var iduser = dcodeInfo.id;
  // var branchInput = dcodeInfo.biokodecabang;

  try {
    const {
      proxyOperationType,
      proxyType,
      proxyAlias,
      customerAccountNumber,
      otpCode,
      otpMessage,
      branchInput,
      branchCode,
      registrationId,
      registrationStatus,
      customerType,
      customerResidentStatus,
      customerCityCode,
      customerAccountType,
      customerAccountName,
      customerSecondaryType,
      customerSecondaryValue,
      customerCurrency,
      cifNumber,
      displayName,
      userInput,
    } = req.body;

    // Generate Proxy List transactionId
    const trxSeq = await conn.raw(
      "select channel_trxid_proxymanagement_seq.nextval from dual"
    );
    const transactionId = await trxIdUtil.seq(trxSeq[0].NEXTVAL);

    const reqBody = JSON.stringify({
      proxyManagementRequest: {
        transactionId: transactionId,
        cid: cid,
        channelType: channelType,
        transactionCode: "200000",
        proxyOperationType: proxyOperationType,
        proxyType: proxyType,
        proxyAlias: proxyAlias,
        customerAccountNumber: customerAccountNumber,
        otpCode: otpCode,
        otpMessage: otpMessage,
        branchInput: branchInput,
        branchCode: branchCode,
        registrationId: registrationId,
        registrationStatus: registrationStatus,
        customerType: customerType,
        customerResidentStatus: customerResidentStatus,
        customerCityCode: customerCityCode,
        customerAccountType: customerAccountType,
        customerAccountName: customerAccountName,
        customerSecondaryType: customerSecondaryType,
        customerSecondaryValue: customerSecondaryValue,
        customerCurrency: customerCurrency,
        cifNumber: cifNumber,
        displayName: displayName,
        userInput: userInput,
      },
    });

    // Request Option
    const options = {
      url:
        conf.komicore.url +
        "/invoke/bjb.bifast.provider.services:proxyManagement",
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization:
          "Basic " +
          Buffer.from(
            conf.komicore.username + ":" + conf.komicore.password
          ).toString("base64"),
      },
    };

    // Send Request
    const request = await httpUtil.request(options, reqBody);

    // Get response
    const response = request.data.proxyManagementResponse;

    // Event log
    // await kktconn("trx_eventlog").insert({
    //   created_by: iduser,
    //   value: "Channel Proxy Management",
    //   idapp: appid,
    //   idmenu: 0,
    //   description:
    //     "Channel Proxy Management Request with with transaction id " +
    //     transactionId +
    //     " and Komi Unique Id " +
    //     response.komiUniqueId,
    //   cdaction: 0,
    //   refevent: req.tokenID ? req.tokenID : 0,
    //   valuejson: req.body ? JSON.stringify(req.body) : null,
    // });

    if (response) {
      setTimeout(function () {
        res.status(200).json({ status: 200, data: response });
      }, 1500);
    } else {
      setTimeout(function () {
        res.status(200).json({ status: 202, data: {} });
      }, 1500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.post("/list", async (req, res, next) => {
  var dcodeInfo = req.userData;
  var appid = dcodeInfo.apps[0].idapp;
  var iduser = dcodeInfo.id;

  try {
    const { cif, proxyAlias, accountNumber } = req.body;

    // Generate Proxy List transactionId
    const trxSeq = await conn.raw(
      "select channel_trxid_proxylist_seq.nextval from dual"
    );
    const transactionId = await trxIdUtil.seq(trxSeq[0].NEXTVAL);

    // Proxy list request
    const reqBody = JSON.stringify({
      proxyListRequest: {
        transactionId: transactionId,
        transactionCode: "230000",
        cid: cid,
        channelType: channelType,
        cifNumber: cif,
        proxyAlias: proxyAlias,
        accountNumber: accountNumber,
      },
    });

    // Request Option
    const options = {
      url: conf.komicore.url + "/invoke/bjb.bifast.provider.services:proxyList",
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization:
          "Basic " +
          Buffer.from(
            conf.komicore.username + ":" + conf.komicore.password
          ).toString("base64"),
      },
    };

    // Send Request
    const request = await httpUtil.request(options, reqBody);

    // Get response
    const response = request.data.proxyListResponse;

    // Event log
    // await kktconn("trx_eventlog").insert({
    //   created_by: iduser,
    //   value: "Channel Proxylist",
    //   idapp: appid,
    //   idmenu: 0,
    //   description:
    //     "Channel Proxylist Request with with transaction id " +
    //     transactionId +
    //     " and Komi Unique Id " +
    //     response.komiUniqueId,
    //   cdaction: 0,
    //   refevent: req.tokenID ? req.tokenID : 0,
    //   valuejson: req.body ? JSON.stringify(req.body) : null,
    // });

    if (response) {
      setTimeout(function () {
        res.status(200).json({ status: 200, data: response });
      }, 1500);
    } else {
      setTimeout(function () {
        res.status(200).json({ status: 202, data: {} });
      }, 1500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.post("/inquiry", async (req, res, next) => {
  var dcodeInfo = req.userData;
  var appid = dcodeInfo.apps[0].idapp;
  var iduser = dcodeInfo.id;

  try {
    const {
      customerAccountNumber,
      customerSecondaryType,
      customerSecondaryValue,
      proxyOperationType,
      proxyType,
      proxyAlias,
    } = req.body;

    // Generate Acc Enquiry transactionId
    const trxSeq = await conn.raw(
      "select channel_trxid_proxymanagementinquiry_seq.nextval from dual"
    );
    const transactionId = await trxIdUtil.seq(trxSeq[0].NEXTVAL);

    // Request
    const reqBody = JSON.stringify({
      proxyManagementInquiryRequest: {
        transactionId: transactionId,
        transactionCode: "210000",
        cid: cid,
        channelType: channelType,
        customerAccountNumber: customerAccountNumber,
        customerSecondaryType: customerSecondaryType,
        customerSecondaryValue: customerSecondaryValue,
        proxyOperationType: proxyOperationType,
        proxyType: proxyType,
        proxyAlias: proxyAlias,
      },
    });

    // Request Option
    const options = {
      url:
        conf.komicore.url +
        "/invoke/bjb.bifast.provider.services:proxyManagementInquiry",
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization:
          "Basic " +
          Buffer.from(
            conf.komicore.username + ":" + conf.komicore.password
          ).toString("base64"),
      },
    };

    // Send Request
    const request = await httpUtil.request(options, reqBody);
    // Get response
    const response = request.data.proxyManagementInquiryResponse;

    // Event log
    // await kktconn("trx_eventlog").insert({
    //   created_by: iduser,
    //   value: "Channel Proxy Management Inquiry",
    //   idapp: appid,
    //   idmenu: 0,
    //   description:
    //     "Proxy Management Inquiry Request with with transaction id " +
    //     transactionId +
    //     " and Komi Unique Id " +
    //     response.komiUniqueId,
    //   cdaction: 0,
    //   refevent: req.tokenID ? req.tokenID : 0,
    //   valuejson: req.body ? JSON.stringify(req.body) : null,
    // });

    if (response.data) {
      setTimeout(function () {
        res.status(200).json({ status: 200, data: response });
      }, 1500);
    } else {
      setTimeout(function () {
        res.status(200).json({ status: 202, data: response });
      }, 1500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.post("/inquiryBySecId", async (req, res, next) => {
  var dcodeInfo = req.userData;
  var appid = dcodeInfo.apps[0].idapp;
  var iduser = dcodeInfo.id;

  try {
    const { registrationId } = req.body;

    // Generate Acc Enquiry transactionId
    const trxSeq = await conn.raw(
      "select channel_trxid_proxyregistrationinquiry_seq.nextval from dual"
    );
    const transactionId = await trxIdUtil.seq(trxSeq[0].NEXTVAL);

    // Request
    const reqBody = JSON.stringify({
      proxyRegistrationInquiryRequest: {
        transactionId: transactionId,
        transactionCode: "220000",
        channelType: channelType,
        registrationId: registrationId,
      },
    });

    // Request Option
    const options = {
      url:
        conf.komicore.url +
        "/invoke/bjb.bifast.provider.services:proxyRegistrationInquiry",
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization:
          "Basic " +
          Buffer.from(
            conf.komicore.username + ":" + conf.komicore.password
          ).toString("base64"),
      },
    };

    // Send Request
    const request = await httpUtil.request(options, reqBody);
    // Get response
    const response = request.data.proxyRegistrationInquiryResponse;

    // Event log
    // await kktconn("trx_eventlog").insert({
    //   created_by: iduser,
    //   value: "Channel Proxy Management Inquiry By Second Id",
    //   idapp: appid,
    //   idmenu: 0,
    //   description:
    //     "Proxy Management Inquiry By Second Id Request with transaction id " +
    //     transactionId +
    //     " and Komi Unique Id " +
    //     response.komiUniqueId,
    //   cdaction: 0,
    //   refevent: req.tokenID ? req.tokenID : 0,
    //   valuejson: req.body ? JSON.stringify(req.body) : null,
    // });

    if (response.data) {
      setTimeout(function () {
        res.status(200).json({ status: 200, data: response });
      }, 1500);
    } else {
      setTimeout(function () {
        res.status(200).json({ status: 202, data: response });
      }, 1500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

router.post("/otp", async (req, res, next) => {
  var dcodeInfo = req.userData;
  var appid = dcodeInfo.apps[0].idapp;
  var iduser = dcodeInfo.id;

  try {
    const {
      customerAccountType,
      customerAccountNumber,
      customerAccountName,
      customerSecondaryValue,
      customerResidentStatus,
      ammountCurrency,
      customerCityName,
      proxyType,
      proxyAlias,
      proxyOperationType,
    } = req.body;

    // Generate Acc Enquiry transactionId
    const trxSeq = await conn.raw(
      "select channel_trxid_proxymanagementotp_seq.nextval from dual"
    );
    const transactionId = await trxIdUtil.seq(trxSeq[0].NEXTVAL);

    // Request
    const reqBody = JSON.stringify({
      proxyManagementOtpRequest: {
        transactionId: transactionId,
        transactionCode: "240000",
        cid: cid,
        channelType: channelType,
        customerAccountType: customerAccountType,
        customerAccountNumber: customerAccountNumber,
        customerAccountName: customerAccountName,
        customerSecondaryValue: customerSecondaryValue,
        customerResidentStatus: customerResidentStatus,
        ammountCurrency: ammountCurrency,
        customerCityName: customerCityName,
        proxyType: proxyType,
        proxyAlias: proxyAlias,
        proxyOperationType: proxyOperationType,
      },
    });

    // Request Option
    const options = {
      url:
        conf.komicore.url +
        "/invoke/bjb.bifast.provider.services:proxyManagementOtp",
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization:
          "Basic " +
          Buffer.from(
            conf.komicore.username + ":" + conf.komicore.password
          ).toString("base64"),
      },
    };

    // Send Request
    const request = await httpUtil.request(options, reqBody);
    // Get response
    const response = request.data.proxyManagementOtpResponse;

    // Event log
    // await kktconn("trx_eventlog").insert({
    //   created_by: iduser,
    //   value: "Channel Proxy Management OTP",
    //   idapp: appid,
    //   idmenu: 0,
    //   description:
    //     "Proxy Management OTP Request with transaction id " +
    //     transactionId +
    //     " and Komi Unique Id " +
    //     response.komiUniqueId,
    //   cdaction: 0,
    //   refevent: req.tokenID ? req.tokenID : 0,
    //   valuejson: req.body ? JSON.stringify(req.body) : null,
    // });

    if (response.data) {
      setTimeout(function () {
        res.status(200).json({ status: 200, data: response });
      }, 1500);
    } else {
      setTimeout(function () {
        res.status(200).json({ status: 202, data: response });
      }, 1500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

module.exports = router;
