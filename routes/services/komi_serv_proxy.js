const express = require("express"),
  app = express();
const router = express.Router();
/*
const jwttools = require("./utils/encryptdecryptjwt");
const groupUtils = require("./utils/groupUtils");
const pool = require("../../connection/db");
var conf = require("../../config.json");
const moment = require("moment");
*/
const knex = require("../../connection/dborm");

// const conn
const conn = knex.conn();
const moment = require("moment");
const checkAuth = require("../../middleware/check-auth");
router.use(checkAuth);
var conf = require("../../config.json");

//getAllGroup
router.post("/getAllProxy", async (req, res, next) => {
  const today = moment().format("YYYY-MM-DD HH:mm:ss");

  try {
    const { branchId, proxyAlias } = req.body;

    const usernamePasswordBuffer = Buffer.from(
      "mii_hafidz" + ":" + "metrodata.1"
    );
    const base64data = usernamePasswordBuffer.toString("base64");
    const headers = {
      Authorization: "Basic " + base64data,
    };

    var url =
      "http://192.168.226.221:5555/invoke/bjb.bifast.provider.services:getTProxyManagement";
    const resCore = await axios.post(
      url,
      {
        getTProxyManagementRequest: {
          esbHeader: {
            externalId: " ",
            timestamp: today,
          },
          esbBody: {
            branchId: branchId,
            proxyAlias: proxyAlias,
          },
        },
      },
      {
        headers,
      }
    );

    if (resCore.data.getTProxyManagementResponse.esbBody.results.length > 0) {
      setTimeout(function () {
        res.status(200).json({
          status: 200,
          data: resCore.data.getTProxyManagementResponse.esbBody,
        });
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({
          status: 202,
          data: resCore.data.getTProxyManagementResponse.esbBody,
        });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({
      status: 500,
      data: "Internal Server Error",
    });
  }
});

router.post("/getProxyByParam", async (req, res, next) => {
  const today = moment().format("YYYY-MM-DD HH:mm:ss");

  try {
    var apps = dcodeInfo.apps[0];
    // {"start_date":"2021-09-24","end_date":"2021-09-27","branch":"B023","channel":"C002","trxtype":"PRX01"}
    const { start_date, end_date, branch, channel, prxtype, secondid } =
      req.body;

    const resp = await conn("m_proxyalias")
      .join("m_branch", "m_proxyalias.branch", "=", "m_branch.branchcode")
      .join(
        "m_channeltype",
        "m_proxyalias.channel",
        "=",
        "m_channeltype.channelcode"
      )
      .join(
        "m_proxytype",
        "m_proxyalias.proxytype",
        "=",
        "m_proxytype.proxycode"
      )
      .join("m_resident", "m_proxyalias.custresident", "=", "m_resident.code")
      .select(
        "m_proxyalias.id",
        "m_proxyalias.name",
        "m_proxyalias.accnum",
        "m_proxyalias.prxaddress",
        "m_proxyalias.branch",
        "m_channeltype.channeltype as channel",
        "m_proxytype.proxyname as proxytype",
        "m_proxyalias.secondid",
        "m_resident.name as custresident",
        "m_proxyalias.proxystat",
        "m_proxyalias.created_date",
        "m_proxyalias.change_who",
        "m_proxyalias.last_update_date",
        "m_proxyalias.idtenant",
        "m_proxyalias.stsprxadd",
        "m_proxyalias.proxyactivity"
      )
      .modify(function (queryBuilder) {
        if (branch) {
          queryBuilder.where("m_proxyalias.proxytype", prxtype);
        }
        if (channel) {
          queryBuilder.where("m_proxyalias.branch", branch);
        }
        if (prxtype) {
          queryBuilder.where("m_proxyalias.channel", channel);
        }
        if (secondid) {
          queryBuilder.where("m_proxyalias.secondid", secondid);
        }
        if (start_date && end_date) {
          queryBuilder.whereBetween("m_proxyalias.created_date", [
            start_date,
            end_date,
          ]);
        }
      });

    // console.log(resCore.data.getTProxyTransactionResponse.esbBody);

    if (resCore.data.getTProxyTransactionResponse.esbBody.results.length > 0) {
      res.status(200).json({
        status: 200,
        data: resCore.data.getTProxyTransactionResponse.esbBody,
      });
    } else {
      res.status(200).json({
        status: 202,
        data: resCore.data.getTProxyTransactionResponse.esbBody,
      });
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({
      status: 500,
      data: "Internal Server Error",
    });
  }
});

router.post("/getProxySummary", async (req, res, next) => {
  const today = moment().format("YYYY-MM-DD HH:mm:ss");

  try {
    // const {
    //   branchId,
    //   proxyAlias,
    //   trxDate1,
    //   trxDate2,
    // } = req.body;

    const usernamePasswordBuffer = Buffer.from(
      "mii_hafidz" + ":" + "metrodata.1"
    );
    const base64data = usernamePasswordBuffer.toString("base64");
    const headers = {
      Authorization: "Basic " + base64data,
    };

    var url =
      "http://192.168.226.221:5555/invoke/bjb.bifast.business.services:getSummaryProxyManagement";
    const resCore = await axios.post(
      url,
      {
        getSummaryProxyManagementRequest: {
          esbHeader: {
            externalId: " ",
            timestamp: today,
          },
          esbBody: {},
        },
      },
      {
        headers,
      }
    );

    // console.log(resCore.data.getSummaryProxyManagementResponse.esbBody);

    if (
      resCore.data.getSummaryProxyManagementResponse.esbBody.results.length > 0
    ) {
      res.status(200).json({
        status: 200,
        data: resCore.data.getSummaryProxyManagementResponse.esbBody,
      });
    } else {
      res.status(200).json({
        status: 202,
        data: resCore.data.getSummaryProxyManagementResponse.esbBody,
      });
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({
      status: 500,
      data: "Internal Server Error",
    });
  }
});

// //getAllGroup
// router.get("/getAllProxy", async (req, res, next) => {
//   const authHeader = req.headers.authorization;
//   var dcodeInfo = req.userData;
//   var apps = dcodeInfo.apps[0];
//   try {
//     // var query = "SELECT id, name, accnum, prxaddress, branch, channel, proxytype, secondid, custresident, proxystat, TO_CHAR('CREATED_DATE','yyyy-MM-dd HH:mm:ss') created_date, change_who, last_update_date, idtenant,stsprxadd FROM public.m_proxyalias order by created_date desc";

//     const ProxyAdmins = await conn("m_proxyalias")
//       .join("m_branch", "m_proxyalias.branch", "=", "m_branch.branchcode")
//       .join(
//         "m_channeltype",
//         "m_proxyalias.channel",
//         "=",
//         "m_channeltype.channelcode"
//       )
//       .join(
//         "m_proxytype",
//         "m_proxyalias.proxytype",
//         "=",
//         "m_proxytype.proxycode"
//       )
//       .join("m_resident", "m_proxyalias.custresident", "=", "m_resident.code")
//       .select(
//         "m_proxyalias.id",
//         "m_proxyalias.name",
//         "m_proxyalias.accnum",
//         "m_proxyalias.prxaddress",
//         "m_proxyalias.branch",
//         "m_channeltype.channeltype as channel",
//         "m_proxytype.proxyname as proxytype",
//         "m_proxyalias.secondid",
//         "m_resident.name as custresident",
//         "m_proxyalias.proxystat",
//         "m_proxyalias.created_date",
//         "m_proxyalias.change_who",
//         "m_proxyalias.last_update_date",
//         "m_proxyalias.idtenant",
//         "m_proxyalias.stsprxadd"
//       )
//       .orderBy("created_date");

//     if (ProxyAdmins.length > 0) {
//       setTimeout(function () {
//         res
//           .status(200)
//           .json({ status: 200, data: { ProxyAdmins: ProxyAdmins } });
//       }, 500);
//     } else {
//       setTimeout(function () {
//         res.status(200).json({ status: 202, data: {} });
//       }, 500);
//     }
//   } catch (err) {
//     console.log(err);
//     res.status(500).json({ status: 500, data: "internal error" });
//   }
// });

// router.post("/getProxyByParam", async (req, res, next) => {
//   var dcodeInfo = req.userData;
//   try {
//     var apps = dcodeInfo.apps[0];
//     // {"start_date":"2021-09-24","end_date":"2021-09-27","branch":"B023","channel":"C002","trxtype":"PRX01"}
//     const { start_date, end_date, branch, channel, prxtype } = req.body;

//     const resp = await conn("m_proxyalias")
//       .join("m_branch", "m_proxyalias.branch", "=", "m_branch.branchcode")
//       .join(
//         "m_channeltype",
//         "m_proxyalias.channel",
//         "=",
//         "m_channeltype.channelcode"
//       )
//       .join(
//         "m_proxytype",
//         "m_proxyalias.proxytype",
//         "=",
//         "m_proxytype.proxycode"
//       )
//       .join("m_resident", "m_proxyalias.custresident", "=", "m_resident.code")
//       .select(
//         "m_proxyalias.id",
//         "m_proxyalias.name",
//         "m_proxyalias.accnum",
//         "m_proxyalias.prxaddress",
//         "m_proxyalias.branch",
//         "m_channeltype.channeltype as channel",
//         "m_proxytype.proxyname as proxytype",
//         "m_proxyalias.secondid",
//         "m_resident.name as custresident",
//         "m_proxyalias.proxystat",
//         "m_proxyalias.created_date",
//         "m_proxyalias.change_who",
//         "m_proxyalias.last_update_date",
//         "m_proxyalias.idtenant",
//         "m_proxyalias.stsprxadd"
//       )
//       .modify(function (queryBuilder) {
//         if (branch) {
//           queryBuilder.where("m_proxyalias.proxytype", prxtype);
//         }
//         if (channel) {
//           queryBuilder.where("m_proxyalias.branch", branch);
//         }
//         if (prxtype) {
//           queryBuilder.where("m_proxyalias.channel", channel);
//         }
//         if (start_date && end_date) {
//           queryBuilder.whereBetween("m_proxyalias.created_date", [
//             start_date,
//             end_date,
//           ]);
//         }
//       });

//     if (resp.length > 0) {
//       res.status(200).json({ status: 200, data: resp });
//     } else {
//       res.status(200).json({
//         status: 202,
//         data: [],
//       });
//     }
//   } catch (err) {
//     console.log(err);
//     res.status(500).json({ status: 500, data: "Error insert User" });
//   }
// });

module.exports = router;
