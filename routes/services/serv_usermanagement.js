const express = require("express"),
  app = express();
const router = express.Router();
const pool = require("../../connection/db");
const jwttools = require("../../routes/services/utils/encryptdecryptjwt");
const logger = require("../../routes/services/utils/logservices");
const moment = require("moment");
const format = require("pg-format");
const checkAuth = require("../../middleware/check-auth");
router.use(checkAuth);
/*POST END*/

router.get("/", async (req, res, next) => {
  try {
    const resp = await pool.query(
      "Select * from public.cdid_owner order by id",
      []
    );
    res.status(200).json({ status: 200, data: resp.rows });
  } catch (err) {
    res.status(500).json({ status: 500, data: "No data tenants found" });
  }
});

router.get("/retriveusers", async (req, res, next) => {
  // const authHeader = req.headers.authorization;
    var dcodeInfo = req.userData;
    var apps = dcodeInfo.apps[0];
    // res.status(200).json({ status: 200, data: apps.id_application });
    try {
      var leveluser = parseInt(dcodeInfo.leveltenant);
      console.log(">>>>>>>>>> LEVEL USER PAGE USER " + leveluser);
      var query = "";
      switch (leveluser) {
        case 0:
          // query = "SELECT tu.id, tu.fullname, tu.userid, tu.pwd, tu.creator_stat, tu.creator_byid, tu.created_at, tu.updated_at,tu.idtenant, tu.idsubtenant, tu.leveltenant, tu.active FROM public.cdid_tenant_user tu WHERE tu.idtenant=$1 and tu.leveltenant = $2";
          query = "SELECT tu.id, tu.fullname, tu.userid, tu.pwd, tu.creator_stat, tu.creator_byid, tu.created_at, tu.updated_at,tu.idtenant, tu.idsubtenant, tu.leveltenant, tu.active, ou.orgid, mo.orgname FROM public.cdid_tenant_user tu INNER JOIN public.cdid_orguserassign ou ON tu.id = ou.userid INNER JOIN public.mst_organizations mo ON ou.orgid=mo.id WHERE tu.idtenant=$1 and tu.leveltenant = $2";


          break;
        case 1:
          console.log(">>>>>>>>>> CASE PAGE USER " + leveluser);
          console.log(dcodeInfo.idtenant, leveluser, apps.idapp);
          query =
            "SELECT DISTINCT ON (tu.id) tu.id,tu.leveltenant, tu.fullname, tu.userid, tu.pwd, tu.creator_stat, tu.creator_byid, tu.created_at, tu.updated_at, tu.idtenant, tu.idsubtenant,tu.leveltenant, tu.active, ct.tnname tenantname,'' subtenantname,tu2.fullname creatorname FROM public.cdid_tenant_user tu INNER JOIN public.cdid_tenant ct ON tu.idtenant = ct.id INNER JOIN public.cdid_tenant_user tu2 ON tu.creator_byid = tu2.id INNER JOIN public.cdid_tenant_user_groupacl tug ON tu.id = tug.iduser WHERE   tu.leveltenant >= $1 and tug.idapplication = $2";
          break;

        default:
          query =
            "SELECT tu.id, tu.fullname, tu.userid, tu.pwd, tu.creator_stat, tu.creator_byid, tu.created_at, tu.updated_at, tu.idtenant, tu.idsubtenant, tu.leveltenant, tu.active, ct.tnname tenantname,'' subtenantname,tu2.fullname creatorname, tug.idgroupuseracl idgroupacl, mug.groupname groupaclname FROM public.cdid_tenant_user tu INNER JOIN public.cdid_tenant_user_groupacl tug ON tu.id = tug.iduser INNER JOIN public.cdid_tenant ct ON tu.idtenant = ct.id INNER JOIN mst_usergroup mug ON tug.idgroupuseracl = mug.id INNER JOIN public.cdid_tenant_user tu2 ON tu.creator_byid = tu2.id WHERE tu.idtenant=$1 and tu.leveltenant > $2 and tug.idapplication=$3";
          break;
      }
      const resp = await pool.query(
        query,
        leveluser === 0
          ? [dcodeInfo.idtenant, leveluser + 1]
          : [leveluser, apps.idapp]
      );
      if (resp.rows.length > 0) {

        // res.status(200).json({ status: 200, data: resp.rows });
        setTimeout(function() {
          res.status(200).json({ status: 200, data: resp.rows });
        }, 500);
      
      } else {
        res.status(500).json({ status: 500, data: "Error retrive Users" });
      }
    } catch (err) {
      console.log(err);
      res.status(500).json({ status: 500, data: "Error retrive Users" });
    }
 
});

router.get("/retriveusersbyid/:id", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
    var apps = dcodeInfo.apps[0];
    let userId = req.params.id;
    try {
      var leveluser = parseInt(dcodeInfo.leveltenant);
      var query = "";
      let preparedStatement = [];
      let queryOrg = false;
      let preparedStatementOrg = [];
      let queryGroup = false;
      let preparedStatementGroup = [];
      /*oldquery= SELECT tu.id, tu.fullname, tu.userid, tu.pwd, tu.creator_stat, tu.creator_byid, tu.created_at, tu.updated_at, tu.idtenant, tu.idsubtenant, tu.leveltenant, tu.active, ct.tnname tenantname,'' subtenantname,tu2.fullname creatorname, tug.idgroupuseracl idgroupacl, mug.groupname groupaclname FROM public.cdid_tenant_user tu INNER JOIN public.cdid_tenant_user_groupacl tug ON tu.id = tug.iduser INNER JOIN public.cdid_tenant ct ON tu.idtenant = ct.id INNER JOIN mst_usergroup mug ON tug.idgroupuseracl = mug.id INNER JOIN public.cdid_tenant_user tu2 ON tu.creator_byid = tu2.id WHERE tu.idtenant=$1 and tu.leveltenant > 0 and tug.idapplication=$2 and tu.id = $3*/
      switch (leveluser) {
        case 0:
          /* query =
            "SELECT tu.id tenantid, tu.fullname, tu.userid, tu.pwd, tu.creator_stat, tu.creator_byid, tu.created_at, tu.updated_at, tu.idtenant,tu.idsubtenant, tu.leveltenant, tu.active, oua.orgid  id, mo.orgname FROM public.cdid_tenant_user tu INNER JOIN public.cdid_orguserassign oua ON tu.id = oua.userid INNER JOIN public.mst_organizations mo ON oua.orgid = mo.id WHERE tu.idtenant = $1 and tu.leveltenant > $2  and tu.id = $3";*/
          query =
            "SELECT tu.id, tu.fullname, tu.userid, tu.pwd, tu.creator_stat, tu.creator_byid, tu.created_at, tu.updated_at,tu.idtenant, tu.idsubtenant, tu.leveltenant, tu.active FROM public.cdid_tenant_user tu WHERE tu.idtenant=$1 and tu.leveltenant = $2 and tu.id = $3";
          preparedStatement = [dcodeInfo.idtenant, leveluser + 1, userId];
          queryOrg =
            "SELECT  oua.orgid  id, mo.orgname AS name FROM  public.cdid_orguserassign oua INNER JOIN public.mst_organizations mo ON oua.orgid = mo.id WHERE userid = $1 ";
          preparedStatementOrg = [userId];
          break;
        case 1:
          /*
          query =
            "SELECT tu.id, tu.fullname, tu.userid, tu.pwd, tu.creator_stat, tu.creator_byid, tu.created_at, tu.updated_at, tu.idtenant, tu.idsubtenant, tu.leveltenant, tu.active, ct.tnname tenantname,'' subtenantname,tu2.fullname creatorname, tug.idgroupuseracl idgroupacl, mug.groupname groupaclname FROM public.cdid_tenant_user tu INNER JOIN public.cdid_tenant_user_groupacl tug ON tu.id = tug.iduser INNER JOIN public.cdid_tenant ct ON tu.idtenant = ct.id INNER JOIN mst_usergroup mug ON tug.idgroupuseracl = mug.id INNER JOIN public.cdid_tenant_user tu2 ON tu.creator_byid = tu2.id WHERE tu.idtenant = $1 and tu.leveltenant > $2 and tug.idapplication = $3 and tu.id = $4";*/
          query =
            "SELECT DISTINCT ON (tu.id) tu.id, tu.fullname, tu.userid, tu.pwd, tu.creator_stat, tu.creator_byid, tu.created_at, tu.updated_at, tu.idtenant, tu.idsubtenant,tu.leveltenant, tu.active, ct.tnname tenantname,'' subtenantname,tu2.fullname creatorname FROM public.cdid_tenant_user tu INNER JOIN public.cdid_tenant ct ON tu.idtenant = ct.id INNER JOIN public.cdid_tenant_user tu2 ON tu.creator_byid = tu2.id INNER JOIN public.cdid_tenant_user_groupacl tug ON tu.id = tug.iduser WHERE tu.idtenant=$1 and tu.leveltenant = $2 and tug.idapplication = $3 and tu.id =$4";
          preparedStatement = [
            dcodeInfo.idtenant,
            userId == dcodeInfo.id ? leveluser + 1 : leveluser,
            apps.idapp,
            userId,
          ];
          queryGroup =
            "SELECT mug.id ,mug.groupname as name FROM public.cdid_tenant_user tu INNER JOIN public.cdid_tenant_user_groupacl tug ON tu.id = tug.iduser INNER JOIN public.cdid_tenant ct ON tu.idtenant = ct.id INNER JOIN mst_usergroup mug ON tug.idgroupuseracl = mug.id WHERE tu.idtenant = $1 and tu.leveltenant = $2 and tug.idapplication = $3 and tu.id = $4";
          preparedStatementGroup = [
            dcodeInfo.idtenant,
            userId == dcodeInfo.id ? leveluser + 1 : leveluser,
            apps.idapp,
            userId,
          ];
          break;
        default:
          query =
            "SELECT tu.id, tu.fullname, tu.userid, tu.pwd, tu.creator_stat, tu.creator_byid, tu.created_at, tu.updated_at, tu.idtenant, tu.idsubtenant, tu.leveltenant, tu.active, ct.tnname tenantname,'' subtenantname,tu2.fullname creatorname, tug.idgroupuseracl idgroupacl, mug.groupname groupaclname FROM public.cdid_tenant_user tu INNER JOIN public.cdid_tenant_user_groupacl tug ON tu.id = tug.iduser INNER JOIN public.cdid_tenant ct ON tu.idtenant = ct.id INNER JOIN mst_usergroup mug ON tug.idgroupuseracl = mug.id INNER JOIN public.cdid_tenant_user tu2 ON tu.creator_byid = tu2.id WHERE tu.idtenant=$1 and tu.leveltenant > 0 and tug.idapplication=$2 and tu.id = $3 tu.leveltenant > $4 ";
          break;
      }
      console.log("param: " + dcodeInfo.idtenant, apps.id, userId, leveluser);
      console.log(query);
      //let preparedStatement =
      //  leveluser === 0 ? [dcodeInfo.idtenant, leveluser, userId] : [];
      console.log(preparedStatement);
      const resp = await pool.query(query, preparedStatement);
      if (queryOrg) {
        const orgData = await pool.query(queryOrg, preparedStatementOrg);
        if (orgData.rows.length > 0) {
          resp.rows[0].org = orgData.rows;
        }
      }
      if (queryGroup) {
        const groupData = await pool.query(queryGroup, preparedStatementGroup);
        if (groupData.rows.length > 0) {
          resp.rows[0].group = groupData.rows;
        }
      }
      console.log("line 109");
      console.log(resp.rows);
      if (resp.rows.length > 0) {
        res.status(200).json({ status: 200, data: resp.rows[0] });
      } else {
        res.status(500).json({ status: 500, data: "Error retrive Users" });
      }
    } catch (err) {
      console.log(err);
      res.status(500).json({ status: 500, data: "Error retrive Users" });
    }
 
});

router.post("/insertbysuper", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
    try {
      var apps = dcodeInfo.apps[0];
      const { fullname, userid, password, isactive, org, group } = req.body;
      var levetid = parseInt(dcodeInfo.leveltenant);
      if (levetid == 0) {
        levetid = levetid + 1;
      }

      console.log(
        ">>>>>>>>>>>>>>>>>>>>>>>>>> APP INSERT ID " + JSON.stringify(apps)
      );
      const resp = await pool.query(
        "INSERT INTO public.cdid_tenant_user(fullname, userid, pwd, creator_stat, creator_byid, idtenant,  leveltenant, active) VALUES ($1, $2, crypt($3, gen_salt('bf', 4)), $4, $5, $6, $7, $8) RETURNING *",
        [
          fullname,
          userid,
          password,
          levetid > 0 ? 1 : levetid,
          dcodeInfo.id,
          dcodeInfo.idtenant,
          levetid,
          isactive,
        ]
      );
      if (resp.rows.length > 0) {
        var userObj = resp.rows[0];
        // console.log(">>>>>>>>>>>>>>>>>>>>>>>. ID APP " + apps.id_application);
        const cdidbio = await pool.query(
          "INSERT INTO public.mst_biodata_corell(bioname, bioemailactive, bioidcorel, biocorelobjid) VALUES ($1, $2, $3, $4) RETURNING *",
          [userObj.fullname, userObj.userid, 3, userObj.id]
        );
        if (cdidbio.rows.length > 0) {
          let groupDatas = [];
          let orgAssign = [];
          let query;
          let query2;
          switch (parseInt(dcodeInfo.leveltenant)) {
            case 0:
              await org.map((grp) => {
                console.log(grp);
                let groups = [];
                groups.push(userObj.id);
                groups.push(grp.id);
                groups.push(0);
                groupDatas.push(groups);
              });

              query =
                "INSERT INTO public.cdid_orguserassign(userid, orgid, grpid)  VALUES %L returning *";
              break;
            case 1:
              query2 =
                "INSERT INTO public.cdid_orguserassign(userid, orgid, grpid)  VALUES %L returning *";
              await dcodeInfo.apps.map(async (app) => {
                let groups = [];
                groups.push(userObj.id);
                groups.push(app.orgid);
                groups.push(0);
                orgAssign.push(groups);
              });

              await group.map((grp) => {
                console.log(grp);
                let groups = [];
                groups.push(userObj.id);
                groups.push(grp.id);
                groups.push(dcodeInfo.idtenant);
                groups.push(apps.idapp);
                groups.push(dcodeInfo.id);
                groups.push(levetid);
                groupDatas.push(groups);
              });
              query =
                "INSERT INTO public.cdid_tenant_user_groupacl(iduser, idgroupuseracl, idtenant, idapplication, created_byid, grouptype) VALUES %L returning *";

              break;
            default:
              break;
          }
          console.log(groupDatas);
          let resp;
          if (query2) {
            let queryFormatAssign = format(query2, orgAssign);
            resp = await pool.query(queryFormatAssign);
          }
          let queryFormat = format(query, groupDatas);
          resp = await pool.query(queryFormat);
          if (resp.rows.length > 0) {
            res.status(200).json({ status: 200, data: userObj });
          } else {
            res.status(500).json({
              status: 500,
              data: "Error insert cdid_orguserassign or cdid_tenant_user_groupacl ",
            });
          }
        }
      } else {
        res.status(500).json({ status: 500, data: "Error insert User" });
      }
    } catch (err) {
      console.log(err);
      res.status(500).json({ status: 500, data: "Error insert User" });
    }
  
});

router.post("/updatebysupperuser", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
    var apps = dcodeInfo.apps[0];
    // var levetid = parseInt(dcodeInfo.leveltenant) + 1;
    var levetid = parseInt(dcodeInfo.leveltenant);
    if (levetid == 0) {
      levetid = levetid + 1;
    }
    // res.status(200).json({ status: 200, data: apps.id_application });
    try {
      const { fullname, userid, isactive, org, group } = req.body;

      // res.status(200).json({ status: 200, data: req.body});

      const resp = await pool.query(
        "UPDATE public.cdid_tenant_user SET fullname=$1, updated_at = to_timestamp($2 / 1000.0), active=$3 WHERE id = $4 RETURNING *",
        [fullname, Date.now(), isactive, userid]
      );
      if (resp.rows.length > 0) {
        var userObj = resp.rows[0];
        //console.log(">>>>>>>>>>>>>>>>>>>>>>>. ID APP " + apps.id_application);
        const cdidbio = await pool.query(
          "update  public.mst_biodata_corell set bioname =$1 ,updated_at = to_timestamp($2 / 1000.0) where  biocorelobjid =$3  RETURNING *",
          [userObj.fullname, Date.now(), userid]
        );
        var leveluser = parseInt(dcodeInfo.leveltenant);
        let queryDelete;
        let psDelete;
        let queryUpdate;
        let queryDeleteAssign;
        let psDeleteAssign;
        let psUpdateAssign;
        let queryUpdateAssign;
        let assignData = [];
        let groupDatas = [];
        switch (leveluser) {
          case 0:
            queryDelete =
              "delete From public.cdid_orguserassign  WHERE userid = $1 RETURNING *";
            psDelete = [userid];
            await org.map((grp) => {
              let groups = [];
              groups.push(userid);
              groups.push(grp.id);
              groups.push(0);
              groupDatas.push(groups);
            });
            queryUpdate =
              "INSERT INTO public.cdid_orguserassign(userid, orgid, grpid)  VALUES %L returning *";
            break;
          case 1:
            queryDeleteAssign =
              "delete From public.cdid_orguserassign  WHERE userid = $1 RETURNING *";
            psDeleteAssign = [userid];
            await dcodeInfo.apps.map((app) => {
              let groups = [];
              groups.push(userObj.id);
              groups.push(app.orgid);
              groups.push(0);
              assignData.push(groups);
            });
            queryUpdateAssign =
              "INSERT INTO public.cdid_orguserassign(userid, orgid, grpid)  VALUES %L returning *";

            queryDelete =
              "delete From public.cdid_tenant_user_groupacl  WHERE iduser = $1 RETURNING *";
            psDelete = [userid];
            await group.map((grp) => {
              let groups = [];

              groups.push(userObj.id);
              groups.push(grp.id);
              groups.push(dcodeInfo.idtenant);
              groups.push(apps.idapp);
              groups.push(dcodeInfo.id);
              groups.push(levetid);
              groupDatas.push(groups);
            });
            queryUpdate =
              "INSERT INTO public.cdid_tenant_user_groupacl(iduser, idgroupuseracl, idtenant, idapplication, created_byid, grouptype) VALUES %L returning *";

            break;
          default:
            break;
        }
        console.log(groupDatas);
        pool
          .query(queryDelete, psDelete)
          .then(async (dt) => {
            if (queryDeleteAssign && queryUpdateAssign) {
              let dltAssign = await pool.query(
                queryDeleteAssign,
                psDeleteAssign
              );
              let queryFormatAssign = format(queryUpdateAssign, assignData);
              let updateAssign = await pool.query(queryFormatAssign);
            }
            let queryFormat = format(queryUpdate, groupDatas);
            const resp = await pool.query(queryFormat);
            if (resp.rows.length > 0) {
              res.status(200).json({ status: 200, data: userObj });
            } else {
              res.status(500).json({
                status: 500,
                data: "Error update cdid_orguserassign or cdid_tenant_user_groupacl ",
              });
            }
          })
          .catch((err) => {
            console.log(err);
            res.status(500).json({
              status: 500,
              data: "Error update cdid_orguserassign or cdid_tenant_user_groupacl ",
            });
          });
      } else {
        res.status(500).json({ status: 500, data: "Error update User" });
      }
    } catch (err) {
      console.log(err);
      res.status(500).json({ status: 500, data: "Error update User" });
    }
 
});

router.get("/deletebysuper/:id", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData
    var apps = dcodeInfo.apps[0];
    let userId = req.params.id;
    try {
      let resp = await pool.query(
        "DELETE FROM public.cdid_tenant_user where id = $1",
        [userId]
      );
      resp = await pool.query(
        "DELETE FROM public.mst_biodata_corell where biocorelobjid = $1",
        [userId]
      );
      var leveluser = parseInt(dcodeInfo.leveltenant);
      if (leveluser == 0) {
        resp = await pool.query(
          "DELETE FROM public.cdid_orguserassign where userid = $1",
          [userId]
        );
      } else {
        resp = await pool.query(
          "DELETE FROM public.cdid_orguserassign where userid = $1",
          [userId]
        );
        resp = await pool.query(
          "DELETE FROM public.cdid_tenant_user_groupacl where iduser = $1",
          [userId]
        );
      }

      res.status(200).json({ status: 200, data: "Success" });
    } catch (err) {
      console.log(err);
      res.status(500).json({ status: 500, data: "Error retrive Users" });
    }
  
});

module.exports = router;

// router.get("/retriveusers", async (req, res, next) => {
//   const authHeader = req.headers.authorization;
//   var dcodeInfo = null;
//   if (authHeader) {
//     TokenArray = authHeader.split(" ");
//     dcodeInfo = await jwttools.decryptdata(TokenArray[1]);
//     var apps = dcodeInfo.apps[0];
//     // res.status(200).json({ status: 200, data: apps.id_application });
//     try {
//       var leveluser = parseInt(dcodeInfo.leveltenant);
//       console.log(">>>>>>>>>> LEVEL USER PAGE USER " + leveluser);
//       var query = "";
//       switch (leveluser) {
//         case 0:
//           /* user multi org
//           query =
//             "SELECT tu.id, tu.fullname, tu.userid, tu.pwd, tu.creator_stat, tu.creator_byid, tu.created_at, tu.updated_at, tu.idtenant, tu.idsubtenant, tu.leveltenant, tu.active, oua.id idorg, mo.orgname FROM public.cdid_tenant_user tu INNER JOIN public.cdid_orguserassign oua ON tu.id = oua.userid INNER JOIN public.mst_organizations mo ON oua.orgid = mo.id WHERE tu.idtenant=$1 and tu.leveltenant > $2";*/
//           query =
//             "SELECT tu.id, tu.fullname, tu.userid, tu.pwd, tu.creator_stat, tu.creator_byid, tu.created_at, tu.updated_at,tu.idtenant, tu.idsubtenant, tu.leveltenant, tu.active FROM public.cdid_tenant_user tu WHERE tu.idtenant=$1 and tu.leveltenant = $2";
//           break;
//         case 1:
//           /*query =
//             "SELECT tu.id, tu.fullname, tu.userid, tu.pwd, tu.creator_stat, tu.creator_byid, tu.created_at, tu.updated_at, tu.idtenant, tu.idsubtenant,tu.leveltenant, tu.active, ct.tnname tenantname,'' subtenantname,tu2.fullname creatorname FROM public.cdid_tenant_user tu INNER JOIN public.cdid_tenant ct ON tu.idtenant = ct.id INNER JOIN public.cdid_tenant_user tu2 ON tu.creator_byid = tu2.id WHERE tu.idtenant=$1 and tu.leveltenant > $2";*/
//           /* 
          
//           query =
//             "SELECT tu.id, tu.fullname, tu.userid, tu.pwd, tu.creator_stat, tu.creator_byid, tu.created_at, tu.updated_at, tu.idtenant, tu.idsubtenant,tu.leveltenant, tu.active, ct.tnname tenantname,'' subtenantname,tu2.fullname creatorname,ug.id idgroup, ug.groupname FROM public.cdid_tenant_user tu INNER JOIN public.cdid_tenant ct ON tu.idtenant = ct.id INNER JOIN public.cdid_tenant_user tu2 ON tu.creator_byid = tu2.id inner join  public.cdid_tenant_user_groupacl tug on tug.iduser = tu.id inner join public.mst_usergroup ug on ug.id= tug.idgroupuseracl WHERE tu.idtenant=$1 and tu.leveltenant > $2";
//             */
//           console.log(">>>>>>>>>> CASE PAGE USER " + leveluser);
//           console.log(dcodeInfo.idtenant, leveluser, apps.idapp);
//           /*query =
//             "SELECT DISTINCT ON (tu.id) tu.id, tu.fullname, tu.userid, tu.pwd, tu.creator_stat, tu.creator_byid, tu.created_at, tu.updated_at, tu.idtenant, tu.idsubtenant,tu.leveltenant, tu.active, ct.tnname tenantname,'' subtenantname,tu2.fullname creatorname FROM public.cdid_tenant_user tu INNER JOIN public.cdid_tenant ct ON tu.idtenant = ct.id INNER JOIN public.cdid_tenant_user tu2 ON tu.creator_byid = tu2.id INNER JOIN public.cdid_tenant_user_groupacl tug ON tu.id = tug.iduser WHERE tu.idtenant=$1 and tu.leveltenant = $2 and tug.idapplication = $3";
//             */
//           query =
//             "SELECT DISTINCT ON (tu.id) tu.id,tu.leveltenant, tu.fullname, tu.userid, tu.pwd, tu.creator_stat, tu.creator_byid, tu.created_at, tu.updated_at, tu.idtenant, tu.idsubtenant,tu.leveltenant, tu.active, ct.tnname tenantname,'' subtenantname,tu2.fullname creatorname FROM public.cdid_tenant_user tu INNER JOIN public.cdid_tenant ct ON tu.idtenant = ct.id INNER JOIN public.cdid_tenant_user tu2 ON tu.creator_byid = tu2.id INNER JOIN public.cdid_tenant_user_groupacl tug ON tu.id = tug.iduser WHERE   tu.leveltenant >= $1 and tug.idapplication = $2";
//           break;

//         default:
//           query =
//             "SELECT tu.id, tu.fullname, tu.userid, tu.pwd, tu.creator_stat, tu.creator_byid, tu.created_at, tu.updated_at, tu.idtenant, tu.idsubtenant, tu.leveltenant, tu.active, ct.tnname tenantname,'' subtenantname,tu2.fullname creatorname, tug.idgroupuseracl idgroupacl, mug.groupname groupaclname FROM public.cdid_tenant_user tu INNER JOIN public.cdid_tenant_user_groupacl tug ON tu.id = tug.iduser INNER JOIN public.cdid_tenant ct ON tu.idtenant = ct.id INNER JOIN mst_usergroup mug ON tug.idgroupuseracl = mug.id INNER JOIN public.cdid_tenant_user tu2 ON tu.creator_byid = tu2.id WHERE tu.idtenant=$1 and tu.leveltenant > $2 and tug.idapplication=$3";
//           break;
//       }

//       // const resp = await pool.query(
//       //   "SELECT tu.id, tu.fullname, tu.userid, tu.pwd, tu.creator_stat, tu.creator_byid, tu.created_at, tu.updated_at, tu.idtenant, tu.idsubtenant, tu.leveltenant, tu.active, ct.tnname tenantname,'' subtenantname,tu2.fullname creatorname, tug.idgroupuseracl idgroupacl, mug.groupname groupaclname FROM public.cdid_tenant_user tu INNER JOIN public.cdid_tenant_user_groupacl tug ON tu.id = tug.iduser INNER JOIN public.cdid_tenant ct ON tu.idtenant = ct.id INNER JOIN mst_usergroup mug ON tug.idgroupuseracl = mug.id INNER JOIN public.cdid_tenant_user tu2 ON tu.creator_byid = tu2.id WHERE tu.idtenant=$1 and tu.leveltenant > $2 and tug.idapplication=$3",
//       //   [dcodeInfo.idtenant,leveluser, apps.id]
//       // );
//       // console.log(dcodeInfo.idtenant, leveluser + 1, apps.idapp);
//       // console.log(">>>>>>>>>> QUERYNYA "+query);
//       const resp = await pool.query(
//         query,
//         leveluser === 0
//           ? [dcodeInfo.idtenant, leveluser + 1]
//           : [leveluser, apps.idapp]

//         //[dcodeInfo.idtenant, leveluser, apps.idapp]
//         // leveluser == 0
//         //   ? [dcodeInfo.idtenant, leveluser + 1]
//         //   : leveluser == 1
//         //   ? [dcodeInfo.idtenant, leveluser + 1, apps.idapp]
//         //   : [dcodeInfo.idtenant, leveluser, apps.id]
//       );
//       if (resp.rows.length > 0) {
//         res.status(200).json({ status: 200, data: resp.rows });
//       } else {
//         res.status(500).json({ status: 500, data: "Error retrive Users" });
//       }
//     } catch (err) {
//       console.log(err);
//       res.status(500).json({ status: 500, data: "Error retrive Users" });
//     }
//   } else {
//     res.status(500).json({ status: 500, data: "Not authorized" });
//   }
// });


// router.post("/insertbysuper", async (req, res, next) => {
//   const authHeader = req.headers.authorization;
//   var dcodeInfo = null;
//   if (authHeader) {
//     TokenArray = authHeader.split(" ");
//     dcodeInfo = await jwttools.decryptdata(TokenArray[1]);

//     // res.status(200).json({ status: 200, data: apps.id_application });
//     try {
//       var apps = dcodeInfo.apps[0];
//       const { fullname, userid, password, isactive, org, group } = req.body;

//       // res.status(200).json({ status: 200, data: req.body});
//       // var levetid = parseInt(dcodeInfo.leveltenant) + 1;
//       var levetid = parseInt(dcodeInfo.leveltenant);
//       if (levetid == 0) {
//         levetid = levetid + 1;
//       }

//       console.log(
//         ">>>>>>>>>>>>>>>>>>>>>>>>>> APP INSERT ID " + JSON.stringify(apps)
//       );
//       const resp = await pool.query(
//         "INSERT INTO public.cdid_tenant_user(fullname, userid, pwd, creator_stat, creator_byid, idtenant,  leveltenant, active) VALUES ($1, $2, crypt($3, gen_salt('bf', 4)), $4, $5, $6, $7, $8) RETURNING *",
//         [
//           fullname,
//           userid,
//           password,
//           levetid > 0 ? 1 : levetid,
//           dcodeInfo.id,
//           dcodeInfo.idtenant,
//           levetid,
//           isactive,
//         ]
//       );
//       if (resp.rows.length > 0) {
//         var userObj = resp.rows[0];
//         // console.log(">>>>>>>>>>>>>>>>>>>>>>>. ID APP " + apps.id_application);
//         const cdidbio = await pool.query(
//           "INSERT INTO public.mst_biodata_corell(bioname, bioemailactive, bioidcorel, biocorelobjid) VALUES ($1, $2, $3, $4) RETURNING *",
//           [userObj.fullname, userObj.userid, 3, userObj.id]
//         );
//         if (cdidbio.rows.length > 0) {
//           let groupDatas = [];
//           let orgAssign = [];
//           let query;
//           let query2;
//           switch (parseInt(dcodeInfo.leveltenant)) {
//             case 0:
//               await org.map((grp) => {
//                 console.log(grp);
//                 let groups = [];
//                 groups.push(userObj.id);
//                 groups.push(grp.id);
//                 groups.push(0);
//                 groupDatas.push(groups);
//               });

//               query =
//                 "INSERT INTO public.cdid_orguserassign(userid, orgid, grpid)  VALUES %L returning *";
//               break;
//             case 1:
//               query2 =
//                 "INSERT INTO public.cdid_orguserassign(userid, orgid, grpid)  VALUES %L returning *";
//               await dcodeInfo.apps.map(async (app) => {
//                 let groups = [];
//                 groups.push(userObj.id);
//                 groups.push(app.orgid);
//                 groups.push(0);
//                 orgAssign.push(groups);
//               });

//               await group.map((grp) => {
//                 console.log(grp);
//                 let groups = [];
//                 groups.push(userObj.id);
//                 groups.push(grp.id);
//                 groups.push(dcodeInfo.idtenant);
//                 groups.push(apps.idapp);
//                 groups.push(dcodeInfo.id);
//                 groups.push(levetid);
//                 groupDatas.push(groups);
//               });
//               query =
//                 "INSERT INTO public.cdid_tenant_user_groupacl(iduser, idgroupuseracl, idtenant, idapplication, created_byid, grouptype) VALUES %L returning *";

//               break;
//             default:
//               break;
//           }
//           console.log(groupDatas);
//           let resp;
//           if (query2) {
//             let queryFormatAssign = format(query2, orgAssign);
//             resp = await pool.query(queryFormatAssign);
//           }
//           let queryFormat = format(query, groupDatas);
//           resp = await pool.query(queryFormat);
//           if (resp.rows.length > 0) {
//             res.status(200).json({ status: 200, data: userObj });
//           } else {
//             res.status(500).json({
//               status: 500,
//               data: "Error insert cdid_orguserassign or cdid_tenant_user_groupacl ",
//             });
//           }
//         }
//       } else {
//         res.status(500).json({ status: 500, data: "Error insert User" });
//       }
//     } catch (err) {
//       console.log(err);
//       res.status(500).json({ status: 500, data: "Error insert User" });
//     }
//   } else {
//     res.status(500).json({ status: 500, data: "Not authorized" });
//   }
// });


// router.get("/retriveusersbyid/:id", async (req, res, next) => {
//   const authHeader = req.headers.authorization;
//   var dcodeInfo = null;
//   if (authHeader) {
//     TokenArray = authHeader.split(" ");
//     dcodeInfo = await jwttools.decryptdata(TokenArray[1]);
//     var apps = dcodeInfo.apps[0];
//     let userId = req.params.id;
//     try {
//       var leveluser = parseInt(dcodeInfo.leveltenant);
//       var query = "";
//       let preparedStatement = [];
//       let queryOrg = false;
//       let preparedStatementOrg = [];
//       let queryGroup = false;
//       let preparedStatementGroup = [];
//       /*oldquery= SELECT tu.id, tu.fullname, tu.userid, tu.pwd, tu.creator_stat, tu.creator_byid, tu.created_at, tu.updated_at, tu.idtenant, tu.idsubtenant, tu.leveltenant, tu.active, ct.tnname tenantname,'' subtenantname,tu2.fullname creatorname, tug.idgroupuseracl idgroupacl, mug.groupname groupaclname FROM public.cdid_tenant_user tu INNER JOIN public.cdid_tenant_user_groupacl tug ON tu.id = tug.iduser INNER JOIN public.cdid_tenant ct ON tu.idtenant = ct.id INNER JOIN mst_usergroup mug ON tug.idgroupuseracl = mug.id INNER JOIN public.cdid_tenant_user tu2 ON tu.creator_byid = tu2.id WHERE tu.idtenant=$1 and tu.leveltenant > 0 and tug.idapplication=$2 and tu.id = $3*/
//       switch (leveluser) {
//         case 0:
//           /* query =
//             "SELECT tu.id tenantid, tu.fullname, tu.userid, tu.pwd, tu.creator_stat, tu.creator_byid, tu.created_at, tu.updated_at, tu.idtenant,tu.idsubtenant, tu.leveltenant, tu.active, oua.orgid  id, mo.orgname FROM public.cdid_tenant_user tu INNER JOIN public.cdid_orguserassign oua ON tu.id = oua.userid INNER JOIN public.mst_organizations mo ON oua.orgid = mo.id WHERE tu.idtenant = $1 and tu.leveltenant > $2  and tu.id = $3";*/
//           query =
//             "SELECT tu.id, tu.fullname, tu.userid, tu.pwd, tu.creator_stat, tu.creator_byid, tu.created_at, tu.updated_at,tu.idtenant, tu.idsubtenant, tu.leveltenant, tu.active FROM public.cdid_tenant_user tu WHERE tu.idtenant=$1 and tu.leveltenant = $2 and tu.id = $3";
//           preparedStatement = [dcodeInfo.idtenant, leveluser + 1, userId];
//           queryOrg =
//             "SELECT  oua.orgid  id, mo.orgname AS name FROM  public.cdid_orguserassign oua INNER JOIN public.mst_organizations mo ON oua.orgid = mo.id WHERE userid = $1 ";
//           preparedStatementOrg = [userId];
//           break;
//         case 1:
//           /*
//           query =
//             "SELECT tu.id, tu.fullname, tu.userid, tu.pwd, tu.creator_stat, tu.creator_byid, tu.created_at, tu.updated_at, tu.idtenant, tu.idsubtenant, tu.leveltenant, tu.active, ct.tnname tenantname,'' subtenantname,tu2.fullname creatorname, tug.idgroupuseracl idgroupacl, mug.groupname groupaclname FROM public.cdid_tenant_user tu INNER JOIN public.cdid_tenant_user_groupacl tug ON tu.id = tug.iduser INNER JOIN public.cdid_tenant ct ON tu.idtenant = ct.id INNER JOIN mst_usergroup mug ON tug.idgroupuseracl = mug.id INNER JOIN public.cdid_tenant_user tu2 ON tu.creator_byid = tu2.id WHERE tu.idtenant = $1 and tu.leveltenant > $2 and tug.idapplication = $3 and tu.id = $4";*/
//           query =
//             "SELECT DISTINCT ON (tu.id) tu.id, tu.fullname, tu.userid, tu.pwd, tu.creator_stat, tu.creator_byid, tu.created_at, tu.updated_at, tu.idtenant, tu.idsubtenant,tu.leveltenant, tu.active, ct.tnname tenantname,'' subtenantname,tu2.fullname creatorname FROM public.cdid_tenant_user tu INNER JOIN public.cdid_tenant ct ON tu.idtenant = ct.id INNER JOIN public.cdid_tenant_user tu2 ON tu.creator_byid = tu2.id INNER JOIN public.cdid_tenant_user_groupacl tug ON tu.id = tug.iduser WHERE tu.idtenant=$1 and tu.leveltenant = $2 and tug.idapplication = $3 and tu.id =$4";
//           preparedStatement = [
//             dcodeInfo.idtenant,
//             userId == dcodeInfo.id ? leveluser + 1 : leveluser,
//             apps.idapp,
//             userId,
//           ];
//           queryGroup =
//             "SELECT mug.id ,mug.groupname as name FROM public.cdid_tenant_user tu INNER JOIN public.cdid_tenant_user_groupacl tug ON tu.id = tug.iduser INNER JOIN public.cdid_tenant ct ON tu.idtenant = ct.id INNER JOIN mst_usergroup mug ON tug.idgroupuseracl = mug.id WHERE tu.idtenant = $1 and tu.leveltenant = $2 and tug.idapplication = $3 and tu.id = $4";
//           preparedStatementGroup = [
//             dcodeInfo.idtenant,
//             userId == dcodeInfo.id ? leveluser + 1 : leveluser,
//             apps.idapp,
//             userId,
//           ];
//           break;
//         default:
//           query =
//             "SELECT tu.id, tu.fullname, tu.userid, tu.pwd, tu.creator_stat, tu.creator_byid, tu.created_at, tu.updated_at, tu.idtenant, tu.idsubtenant, tu.leveltenant, tu.active, ct.tnname tenantname,'' subtenantname,tu2.fullname creatorname, tug.idgroupuseracl idgroupacl, mug.groupname groupaclname FROM public.cdid_tenant_user tu INNER JOIN public.cdid_tenant_user_groupacl tug ON tu.id = tug.iduser INNER JOIN public.cdid_tenant ct ON tu.idtenant = ct.id INNER JOIN mst_usergroup mug ON tug.idgroupuseracl = mug.id INNER JOIN public.cdid_tenant_user tu2 ON tu.creator_byid = tu2.id WHERE tu.idtenant=$1 and tu.leveltenant > 0 and tug.idapplication=$2 and tu.id = $3 tu.leveltenant > $4 ";
//           break;
//       }
//       console.log("param: " + dcodeInfo.idtenant, apps.id, userId, leveluser);
//       console.log(query);
//       //let preparedStatement =
//       //  leveluser === 0 ? [dcodeInfo.idtenant, leveluser, userId] : [];
//       console.log(preparedStatement);
//       const resp = await pool.query(query, preparedStatement);
//       if (queryOrg) {
//         const orgData = await pool.query(queryOrg, preparedStatementOrg);
//         if (orgData.rows.length > 0) {
//           resp.rows[0].org = orgData.rows;
//         }
//       }
//       if (queryGroup) {
//         const groupData = await pool.query(queryGroup, preparedStatementGroup);
//         if (groupData.rows.length > 0) {
//           resp.rows[0].group = groupData.rows;
//         }
//       }
//       console.log("line 109");
//       console.log(resp.rows);
//       if (resp.rows.length > 0) {
//         res.status(200).json({ status: 200, data: resp.rows[0] });
//       } else {
//         res.status(500).json({ status: 500, data: "Error retrive Users" });
//       }
//     } catch (err) {
//       console.log(err);
//       res.status(500).json({ status: 500, data: "Error retrive Users" });
//     }
//   } else {
//     res.status(500).json({ status: 500, data: "Not authorized" });
//   }
// });

// router.post("/updatebysupperuser", async (req, res, next) => {
//   const authHeader = req.headers.authorization;
//   var dcodeInfo = null;
//   if (authHeader) {
//     TokenArray = authHeader.split(" ");
//     dcodeInfo = await jwttools.decryptdata(TokenArray[1]);
//     var apps = dcodeInfo.apps[0];
//     // var levetid = parseInt(dcodeInfo.leveltenant) + 1;
//     var levetid = parseInt(dcodeInfo.leveltenant);
//     if (levetid == 0) {
//       levetid = levetid + 1;
//     }
//     // res.status(200).json({ status: 200, data: apps.id_application });
//     try {
//       const { fullname, userid, isactive, org, group } = req.body;

//       // res.status(200).json({ status: 200, data: req.body});

//       const resp = await pool.query(
//         "UPDATE public.cdid_tenant_user SET fullname=$1, updated_at = to_timestamp($2 / 1000.0), active=$3 WHERE id = $4 RETURNING *",
//         [fullname, Date.now(), isactive, userid]
//       );
//       if (resp.rows.length > 0) {
//         var userObj = resp.rows[0];
//         //console.log(">>>>>>>>>>>>>>>>>>>>>>>. ID APP " + apps.id_application);
//         const cdidbio = await pool.query(
//           "update  public.mst_biodata_corell set bioname =$1 ,updated_at = to_timestamp($2 / 1000.0) where  biocorelobjid =$3  RETURNING *",
//           [userObj.fullname, Date.now(), userid]
//         );
//         var leveluser = parseInt(dcodeInfo.leveltenant);
//         let queryDelete;
//         let psDelete;
//         let queryUpdate;
//         let queryDeleteAssign;
//         let psDeleteAssign;
//         let psUpdateAssign;
//         let queryUpdateAssign;
//         let assignData = [];
//         let groupDatas = [];
//         switch (leveluser) {
//           case 0:
//             queryDelete =
//               "delete From public.cdid_orguserassign  WHERE userid = $1 RETURNING *";
//             psDelete = [userid];
//             await org.map((grp) => {
//               let groups = [];
//               groups.push(userid);
//               groups.push(grp.id);
//               groups.push(0);
//               groupDatas.push(groups);
//             });
//             queryUpdate =
//               "INSERT INTO public.cdid_orguserassign(userid, orgid, grpid)  VALUES %L returning *";
//             break;
//           case 1:
//             queryDeleteAssign =
//               "delete From public.cdid_orguserassign  WHERE userid = $1 RETURNING *";
//             psDeleteAssign = [userid];
//             await dcodeInfo.apps.map((app) => {
//               let groups = [];
//               groups.push(userObj.id);
//               groups.push(app.orgid);
//               groups.push(0);
//               assignData.push(groups);
//             });
//             queryUpdateAssign =
//               "INSERT INTO public.cdid_orguserassign(userid, orgid, grpid)  VALUES %L returning *";

//             queryDelete =
//               "delete From public.cdid_tenant_user_groupacl  WHERE iduser = $1 RETURNING *";
//             psDelete = [userid];
//             await group.map((grp) => {
//               let groups = [];

//               groups.push(userObj.id);
//               groups.push(grp.id);
//               groups.push(dcodeInfo.idtenant);
//               groups.push(apps.idapp);
//               groups.push(dcodeInfo.id);
//               groups.push(levetid);
//               groupDatas.push(groups);
//             });
//             queryUpdate =
//               "INSERT INTO public.cdid_tenant_user_groupacl(iduser, idgroupuseracl, idtenant, idapplication, created_byid, grouptype) VALUES %L returning *";

//             break;
//           default:
//             break;
//         }
//         console.log(groupDatas);
//         pool
//           .query(queryDelete, psDelete)
//           .then(async (dt) => {
//             if (queryDeleteAssign && queryUpdateAssign) {
//               let dltAssign = await pool.query(
//                 queryDeleteAssign,
//                 psDeleteAssign
//               );
//               let queryFormatAssign = format(queryUpdateAssign, assignData);
//               let updateAssign = await pool.query(queryFormatAssign);
//             }
//             let queryFormat = format(queryUpdate, groupDatas);
//             const resp = await pool.query(queryFormat);
//             if (resp.rows.length > 0) {
//               res.status(200).json({ status: 200, data: userObj });
//             } else {
//               res.status(500).json({
//                 status: 500,
//                 data: "Error update cdid_orguserassign or cdid_tenant_user_groupacl ",
//               });
//             }
//           })
//           .catch((err) => {
//             console.log(err);
//             res.status(500).json({
//               status: 500,
//               data: "Error update cdid_orguserassign or cdid_tenant_user_groupacl ",
//             });
//           });
//       } else {
//         res.status(500).json({ status: 500, data: "Error update User" });
//       }
//     } catch (err) {
//       console.log(err);
//       res.status(500).json({ status: 500, data: "Error update User" });
//     }
//   } else {
//     res.status(500).json({ status: 500, data: "Not authorized" });
//   }
// });

// router.get("/deletebysuper/:id", async (req, res, next) => {
//   const authHeader = req.headers.authorization;
//   var dcodeInfo = null;
//   if (authHeader) {
//     TokenArray = authHeader.split(" ");
//     dcodeInfo = await jwttools.decryptdata(TokenArray[1]);
//     var apps = dcodeInfo.apps[0];
//     let userId = req.params.id;
//     try {
//       let resp = await pool.query(
//         "DELETE FROM public.cdid_tenant_user where id = $1",
//         [userId]
//       );
//       resp = await pool.query(
//         "DELETE FROM public.mst_biodata_corell where biocorelobjid = $1",
//         [userId]
//       );
//       var leveluser = parseInt(dcodeInfo.leveltenant);
//       if (leveluser == 0) {
//         resp = await pool.query(
//           "DELETE FROM public.cdid_orguserassign where userid = $1",
//           [userId]
//         );
//       } else {
//         resp = await pool.query(
//           "DELETE FROM public.cdid_orguserassign where userid = $1",
//           [userId]
//         );
//         resp = await pool.query(
//           "DELETE FROM public.cdid_tenant_user_groupacl where iduser = $1",
//           [userId]
//         );
//       }

//       res.status(200).json({ status: 200, data: "Success" });
//     } catch (err) {
//       console.log(err);
//       res.status(500).json({ status: 500, data: "Error retrive Users" });
//     }
//   } else {
//     res.status(500).json({ status: 500, data: "Not authorized" });
//   }
// });