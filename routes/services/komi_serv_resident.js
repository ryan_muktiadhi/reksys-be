const express = require("express"),
  app = express();
const router = express.Router();
const checkAuth = require("../../middleware/check-auth");
router.use(checkAuth);
const knex = require("../../connection/dborm");
const { body, validationResult } = require("express-validator");
const conn = knex.conn();

//getAllGroup
router.get("/getAll", async (req, res, next) => {
  var dcodeInfo = req.userData;

  try {
    const residData = await conn
      .select(
        "id",
        "bank_resident_status",
        "bi_resident_status",
        "description",
        "status",
        conn
          .raw(`TO_CHAR("created_date", 'dd MON yyyy')`)
          .wrap("(", ") created_date"),
        "created_by",
        conn
          .raw(`TO_CHAR("updated_date", 'dd MON yyyy')`)
          .wrap("(", ") updated_date"),
        "updated_by"
      )
      .from("m_resident");

    if (residData.length > 0) {
      setTimeout(function () {
        res.status(200).json({
          status: 200,
          data: residData,
        });
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({
          status: 202,
          data: "Data not found",
        });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({
      status: 500,
      data: "Internal Server Error",
    });
  }
});

router.post(
  "/insert",
  body(
    "bankResidentStatus",
    "Bank Resident Status maximum 5 character"
  ).isLength({
    max: 5,
  }),
  body("biResidentStatus", "BI Account Type maximum 15 character").isLength({
    max: 15,
  }),
  body("description", "description 30 character").isLength({
    max: 30,
  }),
  async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(400).json({
        success: false,
        errors: errors.array(),
      });
    }
    var dcodeInfo = req.userData;
    const userId = dcodeInfo.id;

    try {
      const { bankResidentStatus, biResidentStatus, description, status } =
        req.body;

      // Check Already exsist
      const residExsist = await conn
        .select("*")
        .from("m_resident")
        .where("bank_resident_status", bankResidentStatus)
        .andWhere("bi_resident_status", biResidentStatus);

      if (residExsist.length > 0) {
        res.status(200).json({
          status: 409,
          data: "Data Already Exist",
        });
      } else {
        const resp = await conn("m_resident")
          .insert({
            bank_resident_status: bankResidentStatus,
            bi_resident_status: biResidentStatus,
            description: description,
            status: status,
            created_date: conn.fn.now(),
            created_by: userId,
          })
          .returning(["id"]);

        if (resp.length > 0) {
          console.log("Success insert Resident Status data");
          res.status(200).json({
            status: 200,
            data: "Success insert data",
          });
        } else {
          console.log("Failed insert Resident Status data");
          res.status(200).json({
            status: 500,
            data: "Failed insert data",
          });
        }
      }
    } catch (err) {
      console.log("Failed insert Resident Status data");
      console.log(err);
      res.status(500).json({
        status: 500,
        data: "Internal Server Error",
      });
    }
  }
);

router.post(
  "/update",
  body(
    "bankResidentStatus",
    "Bank Resident Status maximum 5 character"
  ).isLength({
    max: 5,
  }),
  body("biResidentStatus", "BI Account Type maximum 15 character").isLength({
    max: 15,
  }),
  body("description", "description 30 character").isLength({
    max: 30,
  }),
  async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(400).json({
        success: false,
        errors: errors.array(),
      });
    }

    var dcodeInfo = req.userData;
    const userId = dcodeInfo.id;

    try {
      const { id, bankResidentStatus, biResidentStatus, description, status } =
        req.body;

      // Check Already exsist
      const residExsist = await conn
        .select("*")
        .from("m_resident")
        .where("bank_resident_status", bankResidentStatus)
        .andWhere("bi_resident_status", biResidentStatus)
        .andWhere("description", description)
        .andWhere("status", status);

      if (residExsist.length > 0) {
        res.status(200).json({
          status: 204,
          data: "Data already updated",
        });
      } else {
        const resp = await conn("m_resident")
          .update({
            bank_resident_status: bankResidentStatus,
            bi_resident_status: biResidentStatus,
            description: description,
            status: status,
            updated_date: conn.fn.now(),
            updated_by: userId,
          })
          .where("id", id);

        if (resp > 0) {
          console.log("Success update Resident Status data");
          res.status(200).json({
            status: 200,
            data: resp,
          });
        } else {
          console.log("Failed update Resident Status data");
          res.status(200).json({
            status: 500,
            data: "Failed update data",
          });
        }
      }
    } catch (err) {
      console.log(err);
      res.status(500).json({
        status: 500,
        data: "Internal server error",
      });
    }
  }
);

router.get("/delete/:id", async (req, res, next) => {
  var dcodeInfo = req.userData;
  let id = req.params.id;

  try {
    let resp = await conn("m_resident").where("id", id).del();
    res.status(200).json({
      status: 200,
      data: "Success delete data",
    });
  } catch (err) {
    res.status(500).json({
      status: 500,
      data: "Error delete data",
    });
  }
});

router.get("/getById/:id", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;

  try {
    let id = req.params.id;
    const idTypeData = await conn
      .select(
        "id",
        "bank_resident_status",
        "bi_resident_status",
        "description",
        "status",
        "created_date",
        "created_by",
        "updated_date",
        "updated_by"
      )
      .from("m_resident")
      .where("id", id);

    if (idTypeData.length > 0) {
      setTimeout(function () {
        console.log("Success get Resident Status data by id");
        res.status(200).json({
          status: 200,
          data: {
            resident: idTypeData[0],
          },
        });
      }, 500);
    } else {
      console.log("No data Resident Status by id");
      setTimeout(function () {
        res.status(200).json({
          status: 202,
          data: "Data tidak ditemukan",
        });
      }, 500);
    }
  } catch (err) {
    console.log("Failed get Resident Status data");
    console.log(err);
    res.status(500).json({
      status: 500,
      data: "internal error",
    });
  }
});

module.exports = router;
