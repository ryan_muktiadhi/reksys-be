const express = require("express"),
  app = express();
const router = express.Router();
const jwttools = require("./utils/encryptdecryptjwt");
const groupUtils = require("./utils/groupUtils");
const pool = require("../../connection/db");
const checkAuth = require("../../middleware/check-auth");
const moment = require("moment");
const { fork } = require("child_process");
const knex = require("../../connection/dborm");
const httpUtil = require("./utils/httpUtils");
const jsonUtil = require("./utils/jsonUtils");
const trxIdUtil = require("./utils/transactionIdUtils");
var conf = require("../../config.json");
const conn = knex.conn();
const kktconn = knex.connKKT();

router.use(checkAuth);

router.post("/", async (req, res, next) => {
  var dcodeInfo = req.userData;
  var appid = dcodeInfo.apps[0].idapp;
  var iduser = dcodeInfo.id;

  try {
    const { cif, proxyAlias, accountNumber } = req.body;

    // Generate Proxy List transactionId
    const trxSeq = await conn.raw(
      "select channel_trxid_proxylist_seq.nextval from dual"
    );
    const transactionId = await trxIdUtil.seq(trxSeq[0].NEXTVAL);

    // Proxy list request
    const reqBody = JSON.stringify({
      proxyListRequest: {
        transactionId: transactionId,
        transactionCode: "230000",
        cid: "KOM-666",
        channelType: "KOM",
        cifNumber: cif,
        proxyAlias: proxyAlias,
        accountNumber: accountNumber,
      },
    });

    // Request Option
    const options = {
      url: conf.komicore.url + "/invoke/bjb.bifast.provider.services:proxyList",
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization:
          "Basic " +
          Buffer.from(
            conf.komicore.username + ":" + conf.komicore.password
          ).toString("base64"),
      },
    };

    // Send Request
    const request = await httpUtil.request(options, reqBody);

    // Get response
    const response = request.data.proxyListResponse;

    // Event log
    await kktconn("trx_eventlog").insert({
      created_by: iduser,
      value: "Channel Proxylist",
      idapp: appid,
      idmenu: 0,
      description:
        "Channel Proxylist Request with transaction id " + transactionId,
      cdaction: 0,
      refevent: req.tokenID ? req.tokenID : 0,
      valuejson: req.body ? JSON.stringify(req.body) : null,
    });

    if (response) {
      setTimeout(function () {
        res.status(200).json({ status: 200, data: response.data.list });
      }, 1500);
    } else {
      setTimeout(function () {
        res.status(200).json({ status: 202, data: {} });
      }, 1500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

module.exports = router;
