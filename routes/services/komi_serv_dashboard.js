const express = require("express");
const router = express.Router();
const checkAuth = require("../../middleware/check-auth");

const knex = require("../../connection/dborm");
var conf = require("../../config.json");
const conn = knex.conn();

//router.use(checkAuth);

//getTPSData
router.get("/getTPS", async (req, res, next) => {
  try {
    const { startDate, endDate } = req.query;
    let start = new Date(startDate);
    let end = new Date(endDate);
    console.log(start, end);
    if (start > end) {
      res.send(422).json({
        status: 422,
        message: "StartDate Cannot Greater than EndDate",
      });
      return;
    }
    const data = await conn("t_ct_transaction")
      .select(
        "id",
        "trx_initiation_date",
        "trx_response_code",
        "trx_status_code",
        "trx_SLA_flag"
      )
      .where("trx_initiation_date", ">=", start)
      .where("trx_initiation_date", "<=", end);

    var rsArr = [];
    await data.forEach(async (item) => {
      let obj = {
        tranTotal: 0,
        avgTran: 0,
        maxTran: 0,
        hour: "",
        date: "",
        dataHour: [],
      };
      let minObj = {
        tranTotal: 0,
        avgTran: 0,
        tranSuccess: 0,
        tranFailed: 0,
        tranTimeOut: 0,
        minutes: "",
        dataMinutes: [],
      };
      var hour = new Date(item.trx_initiation_date).getHours();
      hour = ("0" + hour).slice(-2);
      obj.hour = hour;

      var minutes = new Date(item.trx_initiation_date).getMinutes();
      minutes = ("0" + minutes).slice(-2);
      minObj.minutes = minutes;
      const existHour = rsArr.findIndex((dt) => {
        return dt.hour == hour;
      });

      if (existHour > -1) {
        let existMin = rsArr[existHour].dataHour.findIndex((dt) => {
          return dt.minutes == minutes;
        });

        if (existMin > -1) {
          obj.date = item.trx_initiation_date;
          rsArr[existHour].dataHour[existMin].dataMinutes.push(item);
        } else {
          obj.date = item.trx_initiation_date;
          minObj.dataMinutes.push(item);
          rsArr[existHour].dataHour.push(minObj);
        }
      } else {
        obj.date = item.trx_initiation_date;
        minObj.dataMinutes.push(item);
        obj.dataHour.push(minObj);
        rsArr.push(obj);
      }
    });
    rsArr.sort(function (a, b) {
      return a.hour.localeCompare(b.hour);
    });
    rsArr.map((hData) => {
      hData.dataHour.map((mData) => {
        let totalMData = mData.dataMinutes.length;
        mData.tranTotal = totalMData;
        hData.tranTotal = hData.tranTotal + mData.tranTotal;
        hData.avgTran = hData.tranTotal / 24;
        hData.maxTran =
          hData.maxTran > mData.tranTotal ? hData.maxTran : mData.tranTotal;
        mData.dataMinutes.map((dt) => {
          mData.tranSuccess =
            dt.trx_response_code == "ACTC"
              ? mData.tranSuccess + 1
              : mData.tranSuccess;
          mData.tranFailed =
            dt.trx_response_code == "RJCT"
              ? mData.tranFailed + 1
              : mData.tranFailed;
          mData.tranTimeOut =
            dt.trx_response_code == "KSTS"
              ? mData.tranTimeOut + 1
              : mData.tranTimeOut;
        });
      });
    });

    res.status(200).json({ status: 200, data: rsArr });
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

//getDaily
router.get("/getDaily", async (req, res, next) => {
  try {
    const { startDate, endDate } = req.query;
    let start = new Date(startDate);
    let end = new Date(endDate);
    console.log("daily");
    console.log(start, end);
    if (start > end) {
      res.send(422).json({
        status: 422,
        message: "StartDate Cannot Greater than EndDate",
      });
      return;
    }
    const data = await conn("t_ct_transaction")
      .select(
        "id",
        "trx_initiation_date",
        "trx_status_code",
        "trx_SLA_flag",
        "trx_response_code",
        "trx_amount"
      )
      .where("trx_initiation_date", ">=", start)
      .where("trx_initiation_date", "<=", end);
    let response = [
      { hour: "00.00", data: {} },
      { hour: "01.00", data: {} },
      { hour: "02.00", data: {} },
      { hour: "03.00", data: {} },
      { hour: "04.00", data: {} },
      { hour: "05.00", data: {} },
      { hour: "06.00", data: {} },
      { hour: "07.00", data: {} },
      { hour: "08.00", data: {} },
      { hour: "09.00", data: {} },
      { hour: "10.00", data: {} },
      { hour: "11.00", data: {} },
      { hour: "12.00", data: {} },
      { hour: "13.00", data: {} },
      { hour: "14.00", data: {} },
      { hour: "15.00", data: {} },
      { hour: "16.00", data: {} },
      { hour: "17.00", data: {} },
      { hour: "18.00", data: {} },
      { hour: "19.00", data: {} },
      { hour: "20.00", data: {} },
      { hour: "21.00", data: {} },
      { hour: "22.00", data: {} },
      { hour: "23.00", data: {} },
    ];
    var rsArr = [];
    await data.forEach(async (item) => {
      let obj = {
        tranTotal: 0,
        avgTran: 0,
        maxTran: 0,
        hour: "",
        dataHour: [],
      };
      let minObj = {
        tranTotal: 0,
        avgTran: 0,
        minutes: "",
        tranSuccess: 0,
        tranSuccessAmount: 0,
        tranSuccessNotSLA: 0,
        tranSuccessNotSLAAmount: 0,
        tranFailed: 0,
        tranFailedAmount: 0,
        tranFailedNotSLA: 0,
        tranFailedNotSLAAmount: 0,
        tranTimeOut: 0,
        tranTimeOutAmount: 0,
        totalTranAmount: 0,
        dataMinutes: [],
      };
      var hour = new Date(item.trx_initiation_date).getHours();
      hour = ("0" + hour).slice(-2);
      obj.hour = hour;

      var minutes = new Date(item.trx_initiation_date).getMinutes();

      minutes = minutes > 0 && minutes < 29 ? "00" : "30";

      minObj.minutes = minutes;
      const existHour = rsArr.findIndex((dt) => {
        return dt.hour == hour;
      });

      if (existHour > -1) {
        let existMin = rsArr[existHour].dataHour.findIndex((dt) => {
          return dt.minutes == minutes;
        });
        if (existMin > -1) {
          rsArr[existHour].dataHour[existMin].dataMinutes.push(item);
        } else {
          minObj.dataMinutes.push(item);
          rsArr[existHour].dataHour.push(minObj);
        }
      } else {
        minObj.dataMinutes.push(item);
        obj.dataHour.push(minObj);
        rsArr.push(obj);
      }
    });
    await rsArr.sort(function (a, b) {
      return a.hour.localeCompare(b.hour);
    });
    await rsArr.map(async (hData) => {
      hData.dataHour.map((mData) => {
        let totalMData = mData.dataMinutes.length;
        mData.tranTotal = totalMData;
        hData.tranTotal = hData.tranTotal + mData.tranTotal;
        hData.avgTran = hData.tranTotal / 24;
        hData.maxTran =
          hData.maxTran > mData.tranTotal ? hData.maxTran : mData.tranTotal;
        mData.dataMinutes.map((dt) => {
          // if (dt.trx_response_code == "ACTC") {
          //   dt.trx_SLA_flag == "1"
          //     ? (mData.tranSuccess = mData.tranSuccess + 1)
          //     : (mData.tranSuccessNotSLA = mData.tranSuccessNotSLA + 1);
          // }
          // if (dt.trx_response_code == "RJCT") {
          //   dt.trx_SLA_flag == "1"
          //     ? (mData.tranFailed = mData.tranFailed + 1)
          //     : (mData.tranFailedNotSLA = mData.tranFailedNotSLA + 1);
          // }
          // if (dt.trx_response_code == "KSTS") {
          //   dt.trx_response_code == "KSTS"
          //     ? (mData.tranTimeOut = mData.tranTimeOut + 1)
          //     : (mData.tranTimeOut = mData.tranTimeOut);
          // }
          if (dt.trx_response_code == "ACTC") {
            if (dt.trx_SLA_flag == "1") {
              mData.tranSuccess = mData.tranSuccess + 1;
              mData.tranSuccessAmount =
                parseInt(mData.tranSuccessAmount) + parseInt(dt.trx_amount);
            } else {
              mData.tranSuccessNotSLA = mData.tranSuccessNotSLA + 1;
              mData.tranSuccessNotSLAAmount =
                parseInt(mData.tranSuccessNotSLAAmount) +
                parseInt(dt.trx_amount);
            }
          }
          if (dt.trx_response_code == "RJCT") {
            if (dt.trx_SLA_flag == "1") {
              mData.tranFailed = mData.tranFailed + 1;
              mData.tranFailedAmount =
                parseInt(mData.tranFailedAmount) + parseInt(dt.trx_amount);
            } else {
              mData.tranFailedNotSLA = mData.tranFailedNotSLA + 1;
              mData.tranFailedNotSLAAmount =
                parseInt(mData.tranFailedNotSLAAmount) +
                parseInt(dt.trx_amount);
            }
          }
          if (dt.trx_response_code == "KSTS") {
            mData.tranTimeOut = mData.tranTimeOut + 1;
            mData.tranTimeOutAmount =
              parseInt(mData.tranTimeOutAmount) + parseInt(dt.trx_amount);
          }
        });
      });
    });
    await rsArr.map(async (data) => {
      let indexRes = await response.findIndex((el) => {
        let hour = el.hour.substring(0, 2);
        return hour == data.hour;
      });
      console.log(data);
      let respData = {
        tranSuccess:
          data.dataHour[0].tranSuccess + data.dataHour[1]?.tranSuccess ||
          data.dataHour[0].tranSuccess,
        tranSuccessAmount:
          data.dataHour[0].tranSuccessAmount +
            data.dataHour[1]?.tranSuccessAmount ||
          data.dataHour[0].tranSuccessAmount,
        tranSuccessNotSLA:
          data.dataHour[0].tranSuccessNotSLA +
            data.dataHour[1]?.tranSuccessNotSLA ||
          data.dataHour[0].tranSuccessNotSLA,
        tranSuccessNotSLAAmount:
          data.dataHour[0].tranSuccessNotSLAAmount +
            data.dataHour[1]?.tranSuccessNotSLAAmount ||
          data.dataHour[0].tranSuccessNotSLAAmount,
        tranFailed:
          data.dataHour[0].tranFailed + data.dataHour[1]?.tranFailed ||
          data.dataHour[0].tranFailed,
        tranFailedAmount:
          data.dataHour[0].tranFailedAmount +
            data.dataHour[1]?.tranFailedAmount ||
          data.dataHour[0].tranFailedAmount,
        tranFailedNotSLA:
          data.dataHour[0].tranFailedNotSLA +
            data.dataHour[1]?.tranFailedNotSLA ||
          data.dataHour[0].tranFailedNotSLA,
        tranFailedNotSLAAmount:
          data.dataHour[0].tranFailedNotSLAAmount +
            data.dataHour[1]?.tranFailedNotSLAAmount ||
          data.dataHour[0].tranFailedNotSLAAmount,
        tranTimeOut:
          data.dataHour[0].tranTimeOut + data.dataHour[1]?.tranTimeOut ||
          data.dataHour[0].tranTimeOut,
        tranTimeOutAmount:
          data.dataHour[0].tranTimeOutAmount +
            data.dataHour[1]?.tranTimeOutAmount ||
          data.dataHour[0].tranTimeOutAmount,
      };
      response[indexRes].data = respData;
    });
    res.status(200).json({ status: 200, data: response, detail: rsArr });
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

//getWeekly
router.get("/getWeekly", async (req, res, next) => {
  try {
    const dayName = [
      "Sunday",
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday",
    ];

    let response = [
      { day: "Sunday", data: {} },
      { day: "Monday", data: {} },
      { day: "Tuesday", data: {} },
      { day: "Wednesday", data: {} },
      { day: "Thursday", data: {} },
      { day: "Friday", data: {} },
      { day: "Saturday", data: {} },
    ];
    const { startDate, endDate } = req.query;
    let start = new Date(startDate);
    let end = new Date(endDate);
    console.log(start, end);
    if (start > end) {
      res.send(422).json({
        status: 422,
        message: "StartDate Cannot Greater than EndDate",
      });
      return;
    }
    const data = await conn("t_ct_transaction")
      .select(
        "id",
        "trx_initiation_date",
        "trx_status_code",
        "trx_SLA_flag",
        "trx_response_code",
        "trx_amount"
      )
      .where("trx_initiation_date", ">=", start)
      .where("trx_initiation_date", "<=", end);

    var rsArr = [];
    await data.forEach(async (item) => {
      let obj = {
        tranTotal: 0,
        avgTran: 0,
        maxTran: 0,
        day: "",
        tranSuccess: 0,
        tranSuccessAmount: 0,
        tranSuccessNotSLA: 0,
        tranSuccessNotSLAAmount: 0,
        tranFailed: 0,
        tranFailedAmount: 0,
        tranFailedNotSLA: 0,
        tranFailedNotSLAAmount: 0,
        tranTimeOut: 0,
        tranTimeOutAmount: 0,
        totalTranAmount: 0,
        dataDay: [],
      };
      var day = new Date(item.trx_initiation_date).getDay();
      day = ("0" + day).slice(-2);
      obj.day = day;
      const existHour = rsArr.findIndex((dt) => {
        return dt.day == day;
      });
      if (existHour > -1) {
        rsArr[existHour].dataDay.push(item);
      } else {
        obj.dataDay.push(item);
        rsArr.push(obj);
      }
    });

    await rsArr.map((hData) => {
      let day = hData.day.slice();
      hData.day = dayName[day[1]];
      hData.tranTotal = hData.tranTotal + hData.dataDay.length;
      hData.maxTran =
        hData.maxTran > hData.tranTotal ? hData.maxTran : hData.tranTotal;
      hData.dataDay.map((dt, index) => {
        // if (dt.trx_response_code == "ACTC") {
        //   dt.trx_SLA_flag == "1"
        //     ? (hData.tranSuccess = hData.tranSuccess + 1)
        //     : (hData.tranSuccessNotSLA = hData.tranSuccessNotSLA + 1);
        // }
        // if (dt.trx_response_code == "RJCT") {
        //   dt.trx_SLA_flag == "1"
        //     ? (hData.tranFailed = hData.tranFailed + 1)
        //     : (hData.tranFailedNotSLA = hData.tranFailedNotSLA + 1);
        // }
        // if (dt.trx_response_code == "KSTS") {
        //   dt.trx_response_code == "KSTS"
        //     ? (hData.tranTimeOut = hData.tranTimeOut + 1)
        //     : (hData.tranTimeOut = hData.tranTimeOut);
        // }
        if (dt.trx_response_code == "ACTC") {
          if (dt.trx_SLA_flag == "1") {
            hData.tranSuccess = hData.tranSuccess + 1;
            hData.tranSuccessAmount =
              parseInt(hData.tranSuccessAmount) + parseInt(dt.trx_amount);
          } else {
            hData.tranSuccessNotSLA = hData.tranSuccessNotSLA + 1;
            hData.tranSuccessNotSLAAmount =
              parseInt(hData.tranSuccessNotSLAAmount) + parseInt(dt.trx_amount);
          }
        }
        if (dt.trx_response_code == "RJCT") {
          if (dt.trx_SLA_flag == "1") {
            hData.tranFailed = hData.tranFailed + 1;
            hData.tranFailedAmount =
              parseInt(hData.tranFailedAmount) + parseInt(dt.trx_amount);
          } else {
            hData.tranFailedNotSLA = hData.tranFailedNotSLA + 1;
            hData.tranFailedNotSLAAmount =
              parseInt(hData.tranFailedNotSLAAmount) + parseInt(dt.trx_amount);
          }
        }
        if (dt.trx_response_code == "KSTS") {
          hData.tranTimeOut = hData.tranTimeOut + 1;
          hData.tranTimeOutAmount =
            parseInt(hData.tranTimeOutAmount) + parseInt(dt.trx_amount);
        }
      });
    });

    await rsArr.map(async (data) => {
      let indexRes = await response.findIndex((el) => {
        return el.day == data.day;
      });

      let respData = {
        tranSuccess: data.tranSuccess,
        tranSuccessAmount: data.tranSuccessAmount,
        tranSuccessNotSLA: data.tranSuccessNotSLA,
        tranSuccessNotSLAAmount: data.tranSuccessNotSLAAmount,
        tranFailed: data.tranFailed,
        tranFailedAmount: data.tranFailedAmount,
        tranFailedNotSLA: data.tranFailedNotSLA,
        tranFailedNotSLAAmount: data.tranFailedNotSLAAmount,
        tranTimeOut: data.tranTimeOut,
        tranTimeOutAmount: data.tranTimeOutAmount,
      };
      response[indexRes].data = respData;
    });

    res.status(200).json({ status: 200, data: response, detail: rsArr });
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

//getMonthly
router.get("/getMonthly", async (req, res, next) => {
  try {
    const today = new Date();
    const year = today.getFullYear();
    const month = today.getMonth();
    var lastday = function (year, month) {
      return new Date(year, month + 1, 0).getDate();
    };
    const lastDate = await lastday(year, month);
    let response = [];
    for (let date = 1; date <= lastDate; date++) {
      let data = {
        date: date < 10 ? `0${date}` : date,
        data: {},
      };
      response.push(data);
    }
    const { startDate, endDate } = req.query;
    let start = new Date(startDate);
    let end = new Date(endDate);
    if (start > end) {
      res.send(422).json({
        status: 422,
        message: "StartDate Cannot Greater than EndDate",
      });
      return;
    }
    const data = await conn("t_ct_transaction")
      .select(
        "id",
        "trx_initiation_date",
        "trx_status_code",
        "trx_SLA_flag",
        "trx_response_code",
        "trx_amount"
      )
      .where("trx_initiation_date", ">=", start)
      .where("trx_initiation_date", "<=", end);

    var rsArr = [];
    await data.forEach(async (item) => {
      let obj = {
        tranTotal: 0,
        avgTran: 0,
        maxTran: 0,
        month: "",
        tranSuccess: 0,
        tranSuccessAmount: 0,
        tranSuccessNotSLA: 0,
        tranSuccessNotSLAAmount: 0,
        tranFailed: 0,
        tranFailedAmount: 0,
        tranFailedNotSLA: 0,
        tranFailedNotSLAAmount: 0,
        tranTimeOut: 0,
        tranTimeOutAmount: 0,
        totalTranAmount: 0,
        dataMonth: [],
      };

      //var month = new Date(item.trx_initiation_date).getMonth();
      // month = ("0" + month).slice(-2);
      // obj.month = month;

      var date = new Date(item.trx_initiation_date).getDate();
      obj.date = date;
      const existHour = rsArr.findIndex((dt) => {
        return dt.date == date;
      });
      if (existHour > -1) {
        rsArr[existHour].dataMonth.push(item);
      } else {
        obj.dataMonth.push(item);
        rsArr.push(obj);
      }
    });

    await rsArr.map(async (hData, index) => {
      hData.tranTotal = hData.tranTotal + hData.dataMonth.length;
      await hData.dataMonth.map((dt) => {
        if (dt.trx_response_code == "ACTC") {
          if (dt.trx_SLA_flag == "1") {
            hData.tranSuccess = hData.tranSuccess + 1;
            hData.tranSuccessAmount =
              parseInt(hData.tranSuccessAmount) +
              parseInt(hData.dataMonth[index]?.trx_amount);
          } else {
            hData.tranSuccessNotSLA = hData.tranSuccessNotSLA + 1;

            hData.tranSuccessNotSLAAmount =
              parseInt(hData.tranSuccessNotSLAAmount) +
              parseInt(hData.dataMonth[index]?.trx_amount);
          }
        }
        if (dt.trx_response_code == "RJCT") {
          if (dt.trx_SLA_flag == "1") {
            hData.tranFailed = hData.tranFailed + 1;
            hData.tranFailedAmount =
              parseInt(hData.tranFailedAmount) +
              parseInt(hData.dataMonth[index]?.trx_amount);
          } else {
            hData.tranFailedNotSLA = hData.tranFailedNotSLA + 1;
            hData.tranFailedNotSLAAmount =
              parseInt(hData.tranFailedNotSLAAmount) +
              parseInt(hData.dataMonth[index]?.trx_amount);
          }
        }
        if (dt.trx_response_code == "KSTS") {
          hData.tranTimeOut = hData.tranTimeOut + 1;
          hData.tranTimeOutAmount =
            parseInt(hData.tranTimeOutAmount) +
            parseInt(hData.dataMonth[index]?.trx_amount);
        }
      });
      hData.maxTran = Math.max(
        hData.maxTran,
        hData.tranSuccess,
        hData.tranSuccessNotSLA,
        hData.tranFailed,
        hData.tranFailedNotSLA,
        hData.tranTimeOut
      );
      hData.totalTranAmount =
        parseInt(hData.totalTranAmount) +
        parseInt(hData.dataMonth[index]?.trx_amount);
    });
    await rsArr.map(async (data) => {
      let indexRes = await response.findIndex((el) => {
        return el.date == data.date;
      });

      let respData = {
        tranSuccess: data.tranSuccess,
        tranSuccessAmount: data.tranSuccessAmount,
        tranSuccessNotSLA: data.tranSuccessNotSLA,
        tranSuccessNotSLAAmount: data.tranSuccessNotSLAAmount,
        tranFailed: data.tranFailed,
        tranFailedAmount: data.tranFailedAmount,
        tranFailedNotSLA: data.tranFailedNotSLA,
        tranFailedNotSLAAmount: data.tranFailedNotSLAAmount,
        tranTimeOut: data.tranTimeOut,
        tranTimeOutAmount: data.tranTimeOutAmount,
      };
      response[indexRes].data = respData;
    });
    res.status(200).json({ status: 200, data: response, detail: rsArr });
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

//getDialyBusiness
router.get("/getDailyBusiness", async (req, res, next) => {
  try {
    const { startDate, endDate } = req.query;
    let start = new Date(startDate);
    let end = new Date(endDate);
    console.log(start, end);
    if (start > end) {
      res.send(422).json({
        status: 422,
        message: "StartDate Cannot Greater than EndDate",
      });
      return;
    }
    let response = [
      { hour: "00.00", data: {} },
      { hour: "01.00", data: {} },
      { hour: "02.00", data: {} },
      { hour: "03.00", data: {} },
      { hour: "04.00", data: {} },
      { hour: "05.00", data: {} },
      { hour: "06.00", data: {} },
      { hour: "07.00", data: {} },
      { hour: "08.00", data: {} },
      { hour: "09.00", data: {} },
      { hour: "10.00", data: {} },
      { hour: "11.00", data: {} },
      { hour: "12.00", data: {} },
      { hour: "13.00", data: {} },
      { hour: "14.00", data: {} },
      { hour: "15.00", data: {} },
      { hour: "16.00", data: {} },
      { hour: "17.00", data: {} },
      { hour: "18.00", data: {} },
      { hour: "19.00", data: {} },
      { hour: "20.00", data: {} },
      { hour: "21.00", data: {} },
      { hour: "22.00", data: {} },
      { hour: "23.00", data: {} },
    ];
    const data = await conn("t_ct_transaction")
      .select(
        "id",
        "trx_initiation_date",
        "trx_response_code",
        "trx_status_code",
        "trx_SLA_flag"
      )
      .where("trx_initiation_date", ">=", start)
      .where("trx_initiation_date", "<=", end);

    var rsArr = [];
    await data.forEach(async (item) => {
      let obj = {
        tranTotal: 0,
        avgTran: 0,
        maxTran: 0,
        hour: "",
        dataHour: [],
      };
      let minObj = {
        tranTotal: 0,
        avgTran: 0,
        minutes: "",
        tranSuccess: 0,
        tranSuccessNotSLA: 0,
        tranFailed: 0,
        tranFailedNotSLA: 0,
        tranTimeOut: 0,
        dataMinutes: [],
      };
      var hour = new Date(item.trx_initiation_date).getHours();
      hour = ("0" + hour).slice(-2);
      obj.hour = hour;

      var minutes = new Date(item.trx_initiation_date).getMinutes();

      minutes = minutes > 0 && minutes < 29 ? "00" : "30";

      minObj.minutes = minutes;
      const existHour = rsArr.findIndex((dt) => {
        return dt.hour == hour;
      });

      if (existHour > -1) {
        let existMin = rsArr[existHour].dataHour.findIndex((dt) => {
          return dt.minutes == minutes;
        });
        if (existMin > -1) {
          rsArr[existHour].dataHour[existMin].dataMinutes.push(item);
        } else {
          minObj.dataMinutes.push(item);
          rsArr[existHour].dataHour.push(minObj);
        }
      } else {
        minObj.dataMinutes.push(item);
        obj.dataHour.push(minObj);
        rsArr.push(obj);
      }
    });
    await rsArr.sort(function (a, b) {
      return a.hour.localeCompare(b.hour);
    });
    await rsArr.map((hData) => {
      hData.dataHour.map((mData) => {
        let totalMData = mData.dataMinutes.length;
        mData.tranTotal = totalMData;
        hData.tranTotal = hData.tranTotal + mData.tranTotal;
        hData.avgTran = hData.tranTotal / 24;
        hData.maxTran =
          hData.maxTran > mData.tranTotal ? hData.maxTran : mData.tranTotal;
        mData.dataMinutes.map((dt) => {
          if (dt.trx_status_code == "S") {
            dt.trx_SLA_flag == "1"
              ? (mData.tranSuccess = mData.tranSuccess + 1)
              : (mData.tranSuccessNotSLA = mData.tranSuccessNotSLA + 1);
          }
          if (dt.trx_status_code == "E") {
            dt.trx_SLA_flag == "1"
              ? (mData.tranFailed = mData.tranFailed + 1)
              : (mData.tranFailedNotSLA = mData.tranFailedNotSLA + 1);
          }
          if (dt.trx_status_code == "T") {
            dt.trx_status_code == "T"
              ? (mData.tranTimeOut = mData.tranTimeOut + 1)
              : (mData.tranTimeOut = mData.tranTimeOut);
          }
        });
      });
    });
    await rsArr.map(async (data) => {
      let indexRes = await response.findIndex((el) => {
        let hour = el.hour.substring(0, 2);
        return hour == data.hour;
      });

      let respData = {
        tranSuccess:
          parseInt(data.dataHour[0].tranSuccess) +
            parseInt(data.dataHour[1]?.tranSuccess) ||
          data.dataHour[0].tranSuccess,
        tranSuccessNotSLA:
          parseInt(data.dataHour[0].tranSuccessNotSLA) +
            parseInt(data.dataHour[1]?.tranSuccessNotSLA) ||
          data.dataHour[0].tranSuccessNotSLA,
        tranFailed:
          parseInt(data.dataHour[0].tranFailed) +
            parseInt(data.dataHour[1]?.tranFailed) ||
          data.dataHour[0].tranFailed,
        tranFailedNotSLA:
          parseInt(data.dataHour[0].tranFailedNotSLA) +
            parseInt(data.dataHour[1]?.tranFailedNotSLA) ||
          data.dataHour[0].tranFailedNotSLA,
        tranTimeOut:
          parseInt(data.dataHour[0].tranTimeOut) +
            parseInt(data.dataHour[1]?.tranTimeOut) ||
          data.dataHour[0].tranTimeOut,
      };
      response[indexRes].data = respData;
    });
    res.status(200).json({ status: 200, data: response, detail: rsArr });
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

module.exports = router;
