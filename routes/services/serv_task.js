const express = require("express"),
  app = express();
const router = express.Router();
const pool = require("../../connection/db");
const jwttools = require("../../routes/services/utils/encryptdecryptjwt");
const logger = require("../../routes/services/utils/logservices");
const moment = require("moment");

/*POST END*/

router.get("/byappid/:id", async (req, res, next) => {
  try {
    let systemdate = new Date();
    const authHeader = req.headers.authorization;
    var dcodeInfo = null;
    if (authHeader) {
      TokenArray = authHeader.split(" ");
      dcodeInfo = await jwttools.decryptdata(TokenArray[1]);
      var query =
        "SELECT mtask.id, mtask.subject, mtask.tasks, mtask.name, mtask.relatedto, mtask.accountid, mtask.accountstr, mtask.completed, mtask.created_byid, mtask.created_at, mtask.updated_at, mtask.duedate_at, mtask.idtenant, mtask.idapp, mtask.datetask FROM public.cdid_mytask mtask where mtask.idapp=$1 and mtask.created_byid=$2 order by mtask.duedate_at";
      const resp = await pool.query(query, [req.params.id, dcodeInfo.id]);
      var jsonbody = resp.rows;

      res.status(200).json({ status: 200, data: jsonbody });
    } else {
      res.status(500).json({ status: 500, data: "Not authorized" });
    }

    res.status(200).json({ status: 200, data: dcodeInfo });
  } catch (err) {
    res.status(500).json({ status: 500, data: "Error insert log Catch" });
  }
});
router.post("/", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = null;
  if (authHeader) {
    TokenArray = authHeader.split(" ");
    dcodeInfo = await jwttools.decryptdata(TokenArray[1]);

    var apps = dcodeInfo.apps[0];

    // res.status(200).json({ status: 200, data: apps });
    try {
      const {
        subject,
        tasks,
        name,
        relatedto,
        accountid,
        accountstr,
        completed,
        duedate_at,
        datetask,
      } = req.body;
      // INSERT INTO public.cdid_mytask(subject, tasks, name, relatedto, accountid, accountstr, completed, created_byid,  duedate_at, idtenant, idapp, datetask)
      //   VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);
      const resp = await pool.query(
        "INSERT INTO public.cdid_mytask(subject, tasks, name, relatedto, accountid, accountstr, completed, created_byid,  duedate_at, idtenant, idapp, datetask) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12) RETURNING *",
        [
          subject,
          tasks,
          name,
          relatedto,
          accountid,
          accountstr,
          completed,
          dcodeInfo.id,
          duedate_at,
          dcodeInfo.idtenant,
          apps.id,
          datetask,
        ]
      );
      res.status(200).json({ status: 200, data: resp.rows });
    } catch (err) {
      res.status(500).json({ status: 500, data: "Error insert Tenant" });
    }
  } else {
    res.status(500).json({ status: 500, data: "Not authorized" });
  }
});

router.put("/:id", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = null;
  if (authHeader) {
    var updateDate = new Date();
    TokenArray = authHeader.split(" ");
    dcodeInfo = await jwttools.decryptdata(TokenArray[1]);
    var apps = dcodeInfo.apps[0];
    // res.status(200).json({ status: 200, data: apps });
    try {
      const {
        subject,
        tasks,
        name,
        relatedto,
        accountid,
        accountstr,
        completed,
        duedate_at,
        datetask,
      } = req.body;
      const resp = await pool.query(
        "UPDATE public.cdid_mytask SET subject=$1, tasks=$2, relatedto=$3, accountstr=$4, completed=$5, updated_at=$6, duedate_at=$7, datetask=$8 WHERE id=$9 RETURNING *",
        [
          subject,
          tasks,
          relatedto,
          accountstr,
          completed,
          updateDate,
          duedate_at,
          datetask,
          req.params.id,
        ]
      );
      res.status(200).json({ status: 200, data: resp.rows });
    } catch (err) {
      res.status(500).json({ status: 500, data: "Error update Task" });
    }
  } else {
    res.status(500).json({ status: 500, data: "Not authorized" });
  }
});

async function asyncForEach(array, callback) {
  // for (let index = 0; index < array.length; index++) {
  //   await callback(array[index], index, array);
  // }
}

module.exports = router;
