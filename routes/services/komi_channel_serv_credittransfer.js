const express = require("express"),
  app = express();
const router = express.Router();
const jwttools = require("./utils/encryptdecryptjwt");
const groupUtils = require("./utils/groupUtils");
const pool = require("../../connection/db");
const checkAuth = require("../../middleware/check-auth");
const moment = require("moment");
const { fork } = require("child_process");
const knex = require("../../connection/dborm");
const httpUtil = require("./utils/httpUtils");
const jsonUtil = require("./utils/jsonUtils");
const trxIdUtil = require("./utils/transactionIdUtils");
var conf = require("../../config.json");
const conn = knex.conn();
const kktconn = knex.connKKT();

router.use(checkAuth);

router.post("/", async (req, res, next) => {
  var dcodeInfo = req.userData;
  var appid = dcodeInfo.apps[0].idapp;
  var iduser = dcodeInfo.id;

  try {
    const {
      branchCode,
      branchInput,
      debitedAccountName,
      debitedAccountNumber,
      debitedAccountType,
      debitedCityCode,
      debitedId,
      debitedResidentStatus,
      debitedType,
      creditedAccountName,
      creditedAccountNumber,
      creditedAccountType,
      creditedBIC,
      creditedCityCode,
      creditedId,
      creditedProxyAlias,
      creditedProxyType,
      creditedResidentStatus,
      creditedType,
      trxAmount,
      feeAmount,
      amountCurrency,
      categoryPurpose,
      chargeBearerCode,
      paymentInformation,
    } = req.body;

    // Generate Acc Enquiry transactionId
    const trxSeq = await conn.raw(
      "select channel_trxid_accenquiry_seq.nextval from dual"
    );
    const transactionId = await trxIdUtil.seq(trxSeq[0].NEXTVAL);

    // Request
    const reqBody = JSON.stringify({
      creditTransferRequest: {
        transactionId: transactionId,
        transactionCode: "310000",
        cid: "IBC",
        channelType: "MBL",
        chargeType: "D",
        userInput: "ME01",
        channelRef: "MBL-M001",
        branchCode: branchCode,
        branchInput: "0000",
        branchFee: "0000",
        debitedAccountName: debitedAccountName,
        debitedAccountNumber: debitedAccountNumber,
        debitedAccountType: debitedAccountType,
        debitedCityCode: debitedCityCode,
        debitedId: debitedId,
        debitedResidentStatus: debitedResidentStatus,
        debitedType: debitedType,
        creditedAccountName: creditedAccountName,
        creditedAccountNumber: creditedAccountNumber,
        creditedAccountType: creditedAccountType,
        creditedBIC: creditedBIC,
        creditedCityCode: creditedCityCode,
        creditedId: creditedId,
        creditedProxyAlias: creditedProxyAlias,
        creditedProxyType: creditedProxyType,
        creditedResidentStatus: creditedResidentStatus,
        creditedType: creditedType,
        categoryPurpose: categoryPurpose,
        trxAmount: trxAmount,
        feeAmount: feeAmount,
        amountCurrency: amountCurrency,
        chargeBearerCode: chargeBearerCode,
        paymentInformation: paymentInformation,
      },
    });

    // Request Option
    const options = {
      url:
        conf.komicore.url +
        "/invoke/bjb.bifast.provider.services:creditTransfer",
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization:
          "Basic " +
          Buffer.from(
            conf.komicore.username + ":" + conf.komicore.password
          ).toString("base64"),
      },
    };

    // Event log
    await kktconn("trx_eventlog").insert({
      created_by: iduser,
      value: "Channel Credit Transfer",
      idapp: appid,
      idmenu: 0,
      description:
        "Channel Credit Transfer Request with transaction id " + transactionId,
      cdaction: 0,
      refevent: req.tokenID ? req.tokenID : 0,
      valuejson: req.body ? JSON.stringify(req.body) : null,
    });

    // Send Request
    const request = await httpUtil.request(options, reqBody);
    // Get response
    const response = request.data.creditTransferResponse;

    if (response.data) {
      setTimeout(function () {
        res.status(200).json({ status: 200, data: response });
      }, 1500);
    } else {
      setTimeout(function () {
        res.status(200).json({ status: 202, data: response });
      }, 1500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "internal error" });
  }
});

module.exports = router;
