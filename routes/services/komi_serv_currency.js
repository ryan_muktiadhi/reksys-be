const express = require("express"),
  app = express();
const router = express.Router();
const jwttools = require("./utils/encryptdecryptjwt");
const groupUtils = require("./utils/groupUtils");
const pool = require("../../connection/db");
const checkAuth = require("../../middleware/check-auth");
const moment = require("moment");
const knex = require("../../connection/dborm");
const msg = require("./utils/responseMessage");
router.use(checkAuth);

var conf = require("../../config.json");
// const conn
const conn = knex.conn();

//getAllGroup
router.get("/getAll", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  var apps = dcodeInfo.apps[0];
  try {
    console.log(
      ">>>>>>>>>>>> Tenanr, Level " + JSON.stringify(dcodeInfo.leveltenant)
    );

    const proxyType = await conn
      .select(
        "id",
        "currency_code",
        "currency_name",
        "status",
        "created_date",
        "created_by",
        "updated_date",
        "updated_by"
      )
      .from("m_currency");

    if (proxyType.length > 0) {
      setTimeout(function () {
        res.status(200).json({ status: 200, data: proxyType });
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({ status: 202, data: {} });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Internal server error" });
  }
});

router.get("/getById/:id", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = req.userData;
  // var apps = dcodeInfo.apps[0];

  try {
    let id = req.params.id;

    const proxyType = await conn
      .select(
        "id",
        "currency_code",
        "currency_name",
        "status",
        "created_date",
        "created_by",
        "updated_date",
        "updated_by"
      )
      .from("m_currency")
      .where("id", id);

    if (proxyType.length > 0) {
      setTimeout(function () {
        res.status(200).json({ status: 200, data: proxyType[0] });
      }, 500);
    } else {
      setTimeout(function () {
        res.status(200).json({ status: 202, data: {} });
      }, 500);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Internal server error" });
  }
});

router.post("/insert", async (req, res, next) => {
  var dcodeInfo = req.userData;
  var apps = dcodeInfo.apps[0];

  try {
    const { currencyCode, currencyName, status } = req.body;

    // Check Already exsist
    const proxyTypeExsist = await conn
      .select("currency_code")
      .where("currency_code", currencyCode.trim())
      .from("m_currency");

    if (proxyTypeExsist.length > 0) {
      res.status(200).json({ status: 409, data: msg.ADD_DATA_ALREADY_EXSIST });
    }

    const resp = await conn("m_currency").insert({
      currency_code: currencyCode,
      currency_code: currencyName,
      status: status,
      created_by: dcodeInfo.userid,
      created_date: conn.fn.now(),
    });

    if (resp.length > 0) {
      console.log(resp);
      res.status(200).json({ status: 200, data: msg.ADD_DATA_SUCCESS });
    } else {
      res.status(500).json({
        status: 500,
        data: msg.ADD_DATA_FAILED,
      });
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Internal server error" });
  }
});

router.post("/update", async (req, res, next) => {
  var dcodeInfo = req.userData;
  var apps = dcodeInfo.apps[0];

  try {
    const { id, currencyCode, currencyName, status } = req.body;
    const today = moment().format("YYYY-MM-DD HH:mm:ss");

    // Check eksist data
    const currencyExsist = await conn
      .select("*")
      .where("id", id)
      .from("m_currency");

    if (currencyExsist.length < 0) {
      res
        .status(200)
        .json({ status: 204, data: msg.UPDATE_DATA_DOSENT_EXSIST });
    }

    const resp = await conn("m_currency")
      .update({
        currency_code: currencyCode,
        currency_name: currencyName,
        status: status,
        created_by: currencyExsist.CREATED_BY,
        created_date: currencyExsist.CREATED_DATE,
        updated_by: dcodeInfo.id,
        updated_date: conn.fn.now(),
      })
      .where("id", id);

    if (resp.length > 0) {
      res.status(200).json({ status: 200, data: msg.UPDATA_DATA_SUCCESS });
    } else {
      res.status(500).json({
        status: 500,
        data: msg.UPDATE_DATA_FAILED,
      });
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Internal server error" });
  }
});

router.get("/delete/:id", async (req, res, next) => {
  var dcodeInfo = req.userData;
  var apps = dcodeInfo.apps[0];
  let id = req.params.id;

  try {
    let resp = await conn("m_currency").where("id", id).del();
    res.status(200).json({ status: 200, data: msg.DELETE_DATA_SUCCESS });
  } catch (err) {
    console.log(err);
    res.status(500).json({ status: 500, data: "Error retrive Users" });
  }
});

module.exports = router;
